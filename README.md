# Frettio.
This is a Mobile Application developed for devices running under Android operative system.

## Development Team.
* Luis Burgos
* André Chay
* Julio Heredia
* Javier Serrano
* Melissa González 

## How to collaborate?.

In order to collaborate on this project is necessary to review all the guidelines and steps defined on the [**Contributing Guide**](https://gitlab.com/frettio/frettio-handbook/blob/master/templates/A_CONTRIBUTING.md).

## Styleguides.

Inside the [**Contributing Guide**](https://gitlab.com/frettio/frettio-handbook/blob/master/templates/A_CONTRIBUTING.md) you can find more information about all the styleguides used in this project.

## API Definition.

In this project the API documentation is hosted on [Apiary](http://apiary.io/) to see the API specification you must review the [Frettio - Apiary](http://docs.frettio.apiary.io/#)

In addition, you also could some of the API Rest request using Postman by clicking the next button.

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/ab2b5c75010ccbf7d87d)

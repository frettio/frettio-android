package com.frettio.frettio.app;

import android.util.Log;

import com.frettio.frettio.app.common.AppConstants;
import com.frettio.frettio.data.remote.services.OrdersService;
import com.frettio.frettio.domain.interactor.AbstractOrderInteractor;
import com.frettio.frettio.domain.model.NewOrder;
import com.frettio.frettio.domain.model.Order;
import com.frettio.frettio.presentation.mvp.Repository;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.frettio.frettio.app.common.AppConstants.TAG;

/**
 * Created by luisburgos on 4/15/17.
 */

public class OrdersInteractor extends AbstractOrderInteractor {


    public OrdersInteractor(OrdersService apiService) {
        super(apiService);
    }

    @Override
    public void getAllOrders(final String email, final Repository.ListAllCallback<Order> callback) {
        Call<List<Order>> call = apiService.getOrdersHistory(email);
        Log.d(TAG, "REQUEST: " + call.request().toString());
        call.enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {
                Log.d(TAG, "RESPONSE: " + response.raw().toString());
                final List<Order> orders = response.body();
                Log.d(TAG, "RESPONSE ELEMENTS: " + new Gson().toJson(orders));

                if (response.isSuccessful()){
                    callback.onItemsLoaded(orders);
                } else if(response.code() == 401) {
                    callback.onServerError("Información incorrecta");
                } else {
                    callback.onServerError("Ha ocurrido un error");
                }
            }

            @Override
            public void onFailure(Call<List<Order>> call, Throwable t) {
                t.printStackTrace();
                callback.onServerError(t.getMessage());
            }
        });
    }

    @Override
    public void createOrder(final NewOrder order, final Repository.SingleItemInformationCallback<NewOrder> callback) {
        Log.d(
                AppConstants.TAG,
                "Creating order from restaurant " + order.getRestaurantName() + "/" + order.getRestaurantEmail() +
                        " from consumer " + order.getConsumerEmail() +
                        " a total of  " + order.getDishes().size() + " dishes and the cost is " + order.getTotal()
        );
        Call<Void> call = apiService.createOrder(order);
        Log.d(TAG, "REQUEST: " + call.request().toString());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(TAG, "RESPONSE: " + response.raw().toString());

                if (response.isSuccessful()){
                    callback.onItemInformationLoaded(order);
                } else if(response.code() == 401) {
                    callback.onServerError("Información incorrecta");
                } else {
                    callback.onServerError("Ha ocurrido un error");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                t.printStackTrace();
                callback.onServerError(t.getMessage());
            }
        });
    }
}

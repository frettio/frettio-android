package com.frettio.frettio.app;

import android.content.Context;
import android.support.annotation.NonNull;

import com.frettio.frettio.app.common.utils.DispatchHelper;
import com.frettio.frettio.data.ServiceGenerator;
import com.frettio.frettio.data.local.FoodDishesLocalRepository;
import com.frettio.frettio.data.local.OrdersLocalRepository;
import com.frettio.frettio.data.local.RestaurantsLocalRepository;
import com.frettio.frettio.data.local.managers.CurrentOrderManager;
import com.frettio.frettio.data.local.managers.TokenManager;
import com.frettio.frettio.data.local.managers.UserSessionManager;
import com.frettio.frettio.data.local.realm.mappers.RealmFoodDishToFoodDishMapper;
import com.frettio.frettio.data.local.realm.mappers.RealmOrderToOrderMapper;
import com.frettio.frettio.data.local.realm.mappers.RealmRestaurantToRestaurantMapper;
import com.frettio.frettio.data.remote.FoodDishesRemoteRepository;
import com.frettio.frettio.data.remote.OrdersRemoteRepository;
import com.frettio.frettio.data.remote.RestaurantsRemoteRepository;
import com.frettio.frettio.data.remote.services.AuthenticationService;
import com.frettio.frettio.data.remote.services.ConsumersService;
import com.frettio.frettio.data.remote.services.FoodDishesService;
import com.frettio.frettio.data.remote.services.OrdersService;
import com.frettio.frettio.data.remote.services.RestaurantsService;
import com.frettio.frettio.data.remote.services.SignUpService;
import com.frettio.frettio.data.repositories.FoodDishesRepository;
import com.frettio.frettio.data.repositories.OrdersRepository;
import com.frettio.frettio.data.repositories.RestaurantsRepository;
import com.frettio.frettio.domain.interactor.AbstractFoodDishInteractor;
import com.frettio.frettio.domain.interactor.AbstractOrderInteractor;
import com.frettio.frettio.domain.interactor.AbstractProfileInteractor;
import com.frettio.frettio.domain.interactor.AbstractRestaurantInteractor;
import com.frettio.frettio.domain.interactor.AbstractSignInInteractor;
import com.frettio.frettio.domain.interactor.AbstractSignUpInteractor;
import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.domain.model.Order;
import com.frettio.frettio.domain.model.Restaurant;
import com.frettio.frettio.presentation.dispatch.RestaurantsDispatchHelper;
import com.frettio.frettio.presentation.mvp.Repository;
import com.frettio.frettio.presentation.signin.SignInActivity;

import java.util.List;

import io.realm.RealmConfiguration;

/**
 * Created by luisburgos on 1/27/17.
 */
public class Injection {

    //INTERACTORS

    @NonNull
    public static AbstractSignInInteractor provideSignInInteractor() {
        return new SignInInteractor(
                provideAuthenticationService()
        );
    }

    @NonNull
    public static AbstractSignUpInteractor provideSignUpInteractor() {
        return new SignUpInteractor(
                provideSignUpService()
        );
    }

    @NonNull
    public static AbstractRestaurantInteractor provideRestaurantsInteractor(String token){
        return new RestaurantsInteractor(
                provideRestaurantsAPIService(token)
        );
    }

    @NonNull
    public static AbstractFoodDishInteractor provideFoodDishesInteractor(String token, String restaurantId){
        return new FoodDishesInteractor(
                provideFoodDishesAPIService(token),
                restaurantId
        );
    }

    @NonNull
    public static AbstractOrderInteractor provideOrderInteractor(String token) {
        return new OrdersInteractor(
                provideOrdersService(token)
        );
    }

    @NonNull
    public static AbstractProfileInteractor provideProfileInteractor(){
        return new ProfileInteractor(providesConsumerService());
    }

    //MANAGERS AND HELPERS

    @NonNull
    public static UserSessionManager provideUserSessionManager(Context context) {
        return new UserSessionManager(context, provideSessionDateTransformer());
    }

    @NonNull
    public static TokenManager provideTokenManager(Context context) {
        return new TokenManager(context);
    }

    public static CurrentOrderManager provideCurrentOrderInteractor(Context context) {
        return new CurrentOrderManager(context);
    }

    @NonNull
    public static DispatchHelper<List<Restaurant>> provideRestaurantsDispatchHelper(Context context) {
        return new RestaurantsDispatchHelper(
                provideRestaurantsRepository(context)
        );
    }

    private static SessionExpirationDateTransformer provideSessionDateTransformer() {
        return new SessionExpirationDateTransformer();
    }

    //SERVICES

    public static ConsumersService providesConsumerService() {
        return ServiceGenerator.createService(ConsumersService.class);
    }

    private static AuthenticationService provideAuthenticationService() {
        return ServiceGenerator.createService(AuthenticationService.class);
    }

    private static SignUpService provideSignUpService() {
        return ServiceGenerator.createService(SignUpService.class);
    }

    private static OrdersService provideOrdersService(String token) {
        return ServiceGenerator.createService(OrdersService.class, token);
    }

    private static RestaurantsService provideRestaurantsAPIService(String token) {
        return ServiceGenerator.createService(RestaurantsService.class, token);
    }

    private static FoodDishesService provideFoodDishesAPIService(String token) {
        return ServiceGenerator.createService(FoodDishesService.class, token);
    }

    //DATA MANAGE

    /**
     * This method uses the Authentication token stored when it were done.
     *
     * @param context where the request comes.
     * @return a Repository data manager for the list of available Restaurants.
     */
    public static Repository<Restaurant> provideRestaurantsRepository(Context context) {
        return RestaurantsRepository.getInstance(
                new RestaurantsRemoteRepository(
                        provideRestaurantsInteractor(provideTokenManager(context).getSession())
                ),
                RestaurantsLocalRepository.getInstance(
                        getRealmConfig(context),
                        new RealmRestaurantToRestaurantMapper()
                )
        );
    }

    /**
     * This method uses the Authentication token stored when it were done.
     *
     * @param context where the request comes.
     * @return a Repository data manager for the Food Dishes of a restaurant.
     */
    public static Repository<FoodDish> provideFoodDishRepository(Context context, String restaurantId) {
        return FoodDishesRepository.getInstance(
                new FoodDishesRemoteRepository(
                        provideFoodDishesInteractor(provideTokenManager(context).getSession(), restaurantId)
                ),
                FoodDishesLocalRepository.getInstance(
                        getRealmConfig(context),
                        new RealmFoodDishToFoodDishMapper()
                )
        );
    }

    /**
     * This method uses the Authentication token stored when it were done.
     *
     * @param context where the request comes.
     * @return a Repository data manager for the user order's history.
     */
    public static Repository<Order> provideOrdersInteractor(Context context) {
        return OrdersRepository.getInstance(
                new OrdersRemoteRepository(
                        provideOrderInteractor(provideTokenManager(context).getSession()),
                        Injection.provideUserSessionManager(context).getSession().getEmail()
                ),
                OrdersLocalRepository.getInstance(
                        getRealmConfig(context),
                        new RealmOrderToOrderMapper()
                )
        );
    }

    private static RealmConfiguration getRealmConfig(Context context) {
        //TODO: Change when finish develop
        return new RealmConfiguration.Builder(context).deleteRealmIfMigrationNeeded().build();
    }
}

package com.frettio.frettio.app;

import android.util.Log;

import com.frettio.frettio.data.remote.services.FoodDishesService;
import com.frettio.frettio.domain.interactor.AbstractFoodDishInteractor;
import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.presentation.mvp.Repository;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.frettio.frettio.app.common.AppConstants.TAG;
import static com.frettio.frettio.app.common.utils.ActivityHelper.checkNotNull;

/**
 * Created by luisburgos on 4/15/17.
 */

public class FoodDishesInteractor extends AbstractFoodDishInteractor {

    private String mCurrentRestaurantId;

    public FoodDishesInteractor(FoodDishesService apiService, String currentRestaurantIdentifier) {
        super(apiService);
        Log.d(TAG, "Setting API service email: " + currentRestaurantIdentifier);
        setCurrentRestaurantId(currentRestaurantIdentifier);
    }

    public void setCurrentRestaurantId(String currentRestaurantIdentifier) {
        mCurrentRestaurantId = checkNotNull(currentRestaurantIdentifier);
    }

    @Override
    public void getAllFoodDishes(final Repository.ListAllCallback<FoodDish> callback) {
        Call<List<FoodDish>> call = apiService.listAllDishes(mCurrentRestaurantId);
        Log.d(TAG, "REQUEST: " + call.request().toString());

        call.enqueue(new Callback<List<FoodDish>>() {
            @Override
            public void onResponse(Call<List<FoodDish>> call, Response<List<FoodDish>> response) {
                Log.d(TAG, "RESPONSE: " + response.raw().toString());
                final List<FoodDish> dishes = response.body();
                Log.d(TAG, "RESPONSE ELEMENTS: " + new Gson().toJson(dishes));
                for(FoodDish dish : dishes){
                    dish.setRestaurantEmail(mCurrentRestaurantId);
                }

                if (response.isSuccessful()){
                    callback.onItemsLoaded(dishes);
                } else if(response.code() == 401) {
                    callback.onServerError("Información incorrecta");
                } else {
                    callback.onServerError("Ha ocurrido un error");
                }
            }

            @Override
            public void onFailure(Call<List<FoodDish>> call, Throwable t) {
                t.printStackTrace();
                callback.onServerError(t.getMessage());
            }
        });
    }
}


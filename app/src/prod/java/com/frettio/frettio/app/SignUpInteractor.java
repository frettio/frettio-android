package com.frettio.frettio.app;

import android.util.Log;

import com.frettio.frettio.app.common.exceptions.NullSessionToken;
import com.frettio.frettio.app.common.exceptions.NullUserSession;
import com.frettio.frettio.data.remote.responses.SignInResponse;
import com.frettio.frettio.data.remote.services.SignUpService;
import com.frettio.frettio.domain.interactor.AbstractInteractor;
import com.frettio.frettio.domain.interactor.AbstractSignUpInteractor;
import com.frettio.frettio.domain.model.Consumer;
import com.frettio.frettio.domain.model.User;
import com.frettio.frettio.presentation.mvp.Repository;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.frettio.frettio.app.common.AppConstants.TAG;

/**
 * Created by luisburgos on 2/13/17.
 */

public class SignUpInteractor extends AbstractSignUpInteractor {

    public SignUpInteractor(SignUpService apiService) {
        super(apiService);
    }

    /**
     * Creates a application session on server in order to obtain a token for authentication on
     * future requests to the server.
     *
     * @param callback listener for server response.
     */
    public void signUp(final Consumer user, final Repository.SingleItemInformationCallback<User> callback) {
        Call<User> call = apiService.signUp(user);
        Log.d(TAG, "REQUEST: " + call.request().toString());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d(TAG, "RESPONSE: " + response.raw().toString());
                if (response.isSuccessful()){
                    User responseBody = response.body();
                    Log.d(TAG, "USER is " + new Gson().toJson(responseBody));
                    callback.onItemInformationLoaded(responseBody);
                } else if(response.code() == 401) {
                    callback.onServerError("Información incorrecta");
                } else {
                    callback.onServerError("Ha ocurrido un error");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();
                callback.onServerError(t.getMessage());
            }
        });
    }

}


package com.frettio.frettio.app;

import android.util.Log;

import com.frettio.frettio.data.local.managers.UserSessionManager;
import com.frettio.frettio.data.remote.responses.SignInResponse;
import com.frettio.frettio.data.remote.services.AuthenticationService;
import com.frettio.frettio.domain.interactor.AbstractSignInInteractor;
import com.frettio.frettio.domain.model.User;
import com.frettio.frettio.presentation.mvp.Repository;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.frettio.frettio.app.common.AppConstants.TAG;
import static com.frettio.frettio.app.common.utils.ActivityHelper.checkNotNull;

public class SignInInteractor extends AbstractSignInInteractor {

    public SignInInteractor(AuthenticationService authAPIService) {
        super(authAPIService);
    }

    /**
     * Creates a application session on server in order to obtain a token for authentication on
     * future requests to the server.
     *
     * @param callback listener for server response.
     * @param email a user email.
     * @param password a user password.
     */
    @Override
    public void signIn(
            final String email,
            final String password,
            final Repository.CompositeItemInformationCallback<User, String> callback
    ) {
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);

        Call<SignInResponse> call = apiService.signIn(user);
        call.enqueue(new Callback<SignInResponse>() {
            @Override
            public void onResponse(Call<SignInResponse> call, Response<SignInResponse> response) {
                Log.d(TAG, "RESPONSE: " + response.raw().toString());
                final SignInResponse signInResponse = response.body();

                if (response.isSuccessful()){
                    User responseBody = signInResponse.getUser();
                    String token = signInResponse.getToken();
                    Log.d(TAG, "USER is " + new Gson().toJson(responseBody));
                    Log.d(TAG, "TOKEN is " + token);
                    callback.onItemInformationLoaded(responseBody, token);
                } else if(response.code() == 401) {
                    callback.onServerError("Información incorrecta");
                } else {
                    callback.onServerError("Ha ocurrido un error");
                }
            }

            @Override
            public void onFailure(Call<SignInResponse> call, Throwable t) {
                t.printStackTrace();
                callback.onServerError(t.getMessage());
            }
        });
    }

}

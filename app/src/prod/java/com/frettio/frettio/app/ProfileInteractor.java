package com.frettio.frettio.app;

import android.util.Log;

import com.frettio.frettio.data.remote.responses.ConsumerResponse;
import com.frettio.frettio.data.remote.services.ConsumersService;
import com.frettio.frettio.domain.interactor.AbstractProfileInteractor;
import com.frettio.frettio.domain.model.Consumer;
import com.frettio.frettio.domain.model.User;
import com.frettio.frettio.presentation.mvp.Repository;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.frettio.frettio.app.common.AppConstants.TAG;

/**
 * Created by Melissa on 25/02/2017.
 */

public class ProfileInteractor extends AbstractProfileInteractor {

    public ProfileInteractor(ConsumersService sessionsAPIService) {
        super(sessionsAPIService);
    }

    @Override
    public void getConsumer(String email, final Repository.SingleItemInformationCallback<User> callback) {
        Call<User> call = apiService.getConsumer(email);
        Log.d(TAG, "REQUEST: " + call.request().toString());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d(TAG, "RESPONSE: " + response.raw().toString());
                if (response.isSuccessful()){
                    User responseBody = response.body();
                    Log.d(TAG, "RETRIEVED USER is " + new Gson().toJson(responseBody));
                    callback.onItemInformationLoaded(responseBody);
                } else if(response.code() == 401) {
                    callback.onServerError("Información incorrecta");
                } else {
                    callback.onServerError("Ha ocurrido un error");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();
                callback.onServerError(t.getMessage());
            }
        });
    }

    @Override
    public void updateConsumer(final Consumer user, final Repository.SingleItemInformationCallback<User> callback) {
        Call<User> call = apiService.updateConsumer(user.getEmail(), user);
        Log.d(TAG, "REQUEST: " + call.request().toString());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d(TAG, "RESPONSE: " + response.raw().toString());
                if (response.isSuccessful()){
                    User responseBody = response.body();
                    Log.d(TAG, "UPDATED USER is " + new Gson().toJson(responseBody));
                    callback.onItemInformationLoaded(responseBody);
                } else if(response.code() == 401) {
                    callback.onServerError("Información incorrecta");
                } else {
                    callback.onServerError("Ha ocurrido un error");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();
                callback.onServerError(t.getMessage());
            }
        });
    }

}

package com.frettio.frettio.app;

import android.util.Log;

import com.frettio.frettio.app.common.AppConstants;
import com.frettio.frettio.data.remote.responses.SignInResponse;
import com.frettio.frettio.data.remote.services.RestaurantsService;
import com.frettio.frettio.domain.interactor.AbstractRestaurantInteractor;
import com.frettio.frettio.domain.model.Restaurant;
import com.frettio.frettio.domain.model.User;
import com.frettio.frettio.presentation.mvp.Repository;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.frettio.frettio.app.common.AppConstants.TAG;

/**
 * Created by luisburgos on 4/15/17.
 */

public class RestaurantsInteractor extends AbstractRestaurantInteractor {

    public RestaurantsInteractor(RestaurantsService apiService) {
        super(apiService);
    }

    @Override
    public void getAllRestaurants(final Repository.ListAllCallback<Restaurant> callback) {
        Call<List<Restaurant>> call = apiService.listAllRestaurants(AppConstants.Restaurants.ACCEPTED);
        Log.d(TAG, "REQUEST: " + call.request().toString());
        call.enqueue(new Callback<List<Restaurant>>() {
            @Override
            public void onResponse(Call<List<Restaurant>> call, Response<List<Restaurant>> response) {
                Log.d(TAG, "RESPONSE: " + response.raw().toString());
                final List<Restaurant> restaurants = response.body();

                if (response.isSuccessful()){
                    callback.onItemsLoaded(restaurants);
                } else if(response.code() == 401) {
                    callback.onServerError("Información incorrecta");
                } else {
                    callback.onServerError("Ha ocurrido un error");
                }
            }

            @Override
            public void onFailure(Call<List<Restaurant>> call, Throwable t) {
                t.printStackTrace();
                callback.onServerError(t.getMessage());
            }
        });
    }

    @Override
    public void getRestaurant(int id, Repository.SingleItemInformationCallback<Restaurant> callback) {
        //Do nothing!
    }
}

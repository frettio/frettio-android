package com.frettio.frettio.app;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class SessionExpirationDateTransformer {

    private Calendar calendar;
    private Date signInDate;
    private Date expirationDate;

    public SessionExpirationDateTransformer() {
        calendar = new GregorianCalendar();
        signInDate = calendar.getTime();
        addExpirationTimeToCalendar();
        expirationDate = calendar.getTime();
    }

    public Date getSignInDate() {
        return signInDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    private void addExpirationTimeToCalendar(){
        calendar.add(Calendar.DATE, 30);
    }

}
package com.frettio.frettio.app;

import com.frettio.frettio.data.remote.services.SignUpService;
import com.frettio.frettio.domain.interactor.AbstractSignUpInteractor;
import com.frettio.frettio.domain.model.User;
import com.frettio.frettio.presentation.mvp.Repository;

/**
 * Created by luisburgos on 2/19/17.
 */

public class FakeSignUpInteractor extends AbstractSignUpInteractor {

    public FakeSignUpInteractor(SignUpService apiService) {
        super(apiService);
    }

    /**
     * Creates a application session on server in order to obtain a token for authentication on
     * future requests to the server.
     *
     * @param callback listener for server response.
     */
    public void signUp(User user, final Repository.SingleItemInformationCallback<User> callback) {
        //TODO: Finish when service is ready.
        callback.onItemInformationLoaded(user);
    }

}

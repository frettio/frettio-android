package com.frettio.frettio.app;

import com.frettio.frettio.data.local.managers.UserSessionManager;
import com.frettio.frettio.data.remote.services.AuthenticationService;
import com.frettio.frettio.domain.interactor.AbstractInteractor;
import com.frettio.frettio.domain.interactor.AbstractSignInInteractor;
import com.frettio.frettio.domain.model.HomeAddress;
import com.frettio.frettio.domain.model.User;
import com.frettio.frettio.presentation.mvp.Repository;

import static com.frettio.frettio.app.common.utils.ActivityHelper.checkNotNull;

/**
 * Created by luisburgos on 2/19/17.
 */

public class FakeSignInInteractor extends AbstractSignInInteractor {

    private UserSessionManager sessionManager;

    public FakeSignInInteractor(
            AuthenticationService authAPIService,
            UserSessionManager sessionManager
    ) {
        super(authAPIService);
        this.sessionManager = checkNotNull(sessionManager);
    }

    /**
     * Creates a application session on server in order to obtain a token for authentication on
     * future requests to the server.
     *
     * @param callback listener for server response.
     * @param email    a user email.
     * @param password a user password.
     */
    public void signIn(String email, String password, final Repository.SingleItemInformationCallback<User> callback) {
        User user = new User();
        user.setFirstName("Luis");
        user.setLastNames("Burgos");
        user.setEmail(email);
        user.setPassword(password);
        user.setPhone("99918998230");
        user.setHomeAddress(new HomeAddress("118", "Mérida", "11", "12 y 16", "Mulsay I"));
        user.setImageURL("http://lorempixel.com/200/200/people/");

        if(user.getEmail().equals("demo@demo.com") && user.getPassword().equals("demo")){
            callback.onItemInformationLoaded(user);
        } else {
            callback.onServerError("Error");
        }
    }


}
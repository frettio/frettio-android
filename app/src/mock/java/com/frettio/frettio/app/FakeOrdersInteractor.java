package com.frettio.frettio.app;

import android.util.Log;

import com.frettio.frettio.app.common.AppConstants;
import com.frettio.frettio.data.remote.services.OrdersService;
import com.frettio.frettio.domain.interactor.AbstractOrderInteractor;
import com.frettio.frettio.domain.model.Order;
import com.frettio.frettio.presentation.mvp.Repository;

import java.util.ArrayList;

/**
 * Created by luisburgos on 3/5/17.
 */

public class FakeOrdersInteractor extends AbstractOrderInteractor {

    private final ArrayList<Order> loadedList;

    public FakeOrdersInteractor(OrdersService apiService) {
        super(apiService);

        loadedList = new ArrayList<>();
        loadedList.add(new Order(1, "Restaurante 1", "res1@res.com", "demo@demo.com", 200));
        loadedList.add(new Order(2, "Establecimiento 2", "res3@res.com", "demo@demo.com", 1000));
        loadedList.add(new Order(3,  "Establecimiento 3", "res2@res.com", "demo@demo.com", 1500));
    }

    @Override
    public void getAllOrders(String serverToken, Repository.ListAllCallback<Order> callback) {
        Log.d(AppConstants.TAG, "Loading from fake interactor total of " + loadedList.size());
        callback.onItemsLoaded(loadedList);
    }

    @Override
    public void createOrder(Order order, Repository.SingleItemInformationCallback<Order> callback) {
        Log.d(
                AppConstants.TAG,
                "Creating order from restaurant " + order.getRestaurantName() +
                " from consumer " + order.getConsumerEmail() +
                " a total of  " + order.getDishes().size() + " dishes and the cost is " + order.getTotal()
        );
        callback.onItemInformationLoaded(order);
    }
}


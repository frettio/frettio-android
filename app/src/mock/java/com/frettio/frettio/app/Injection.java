package com.frettio.frettio.app;

import android.content.Context;
import android.support.annotation.NonNull;

import com.frettio.frettio.app.common.utils.DispatchHelper;
import com.frettio.frettio.data.ServiceGenerator;
import com.frettio.frettio.data.local.FoodDishesLocalRepository;
import com.frettio.frettio.data.local.OrdersLocalRepository;
import com.frettio.frettio.data.local.RestaurantsLocalRepository;
import com.frettio.frettio.data.local.managers.CurrentOrderManager;
import com.frettio.frettio.data.local.managers.UserSessionManager;
import com.frettio.frettio.data.local.realm.mappers.RealmFoodDishToFoodDishMapper;
import com.frettio.frettio.data.local.realm.mappers.RealmOrderToOrderMapper;
import com.frettio.frettio.data.local.realm.mappers.RealmRestaurantToRestaurantMapper;
import com.frettio.frettio.data.remote.FoodDishesRemoteRepository;
import com.frettio.frettio.data.remote.OrdersRemoteRepository;
import com.frettio.frettio.data.remote.RestaurantsRemoteRepository;
import com.frettio.frettio.data.remote.services.AuthenticationService;
import com.frettio.frettio.data.remote.services.ConsumersService;
import com.frettio.frettio.data.remote.services.FoodDishesService;
import com.frettio.frettio.data.remote.services.OrdersService;
import com.frettio.frettio.data.remote.services.RestaurantsService;
import com.frettio.frettio.data.remote.services.SignUpService;
import com.frettio.frettio.data.repositories.FoodDishesRepository;
import com.frettio.frettio.data.repositories.OrdersRepository;
import com.frettio.frettio.data.repositories.RestaurantsRepository;
import com.frettio.frettio.domain.interactor.AbstractFoodDishInteractor;
import com.frettio.frettio.domain.interactor.AbstractOrderInteractor;
import com.frettio.frettio.domain.interactor.AbstractProfileInteractor;
import com.frettio.frettio.domain.interactor.AbstractRestaurantInteractor;
import com.frettio.frettio.domain.interactor.AbstractSignInInteractor;
import com.frettio.frettio.domain.interactor.AbstractSignUpInteractor;
import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.domain.model.Order;
import com.frettio.frettio.domain.model.Restaurant;
import com.frettio.frettio.presentation.dispatch.RestaurantsDispatchHelper;
import com.frettio.frettio.presentation.mvp.Repository;

import java.util.List;

import io.realm.RealmConfiguration;

/**
 * Created by luisburgos on 1/27/17.
 */
public class Injection {

    @NonNull
    public static AbstractSignInInteractor provideSignInInteractor(Context context) {
        return new FakeSignInInteractor(
                provideAuthenticationService(),
                provideUserSessionManager(context)
        );
    }

    @NonNull
    public static AbstractSignUpInteractor provideSignUpInteractor() {
        return new FakeSignUpInteractor(
                provideSignUpService()
        );
    }

    @NonNull
    public static AbstractRestaurantInteractor provideRestaurantsInteractor(){
        return new FakeRestaurantsInteractor(
                provideRestaurantsAPIService()
        );
    }

    @NonNull
    public static AbstractFoodDishInteractor provideFoodDishesInteractor(){
        return new FakeFoodDishesInteractor(
                provideFoodDishesAPIService()
        );
    }

    @NonNull
    public static AbstractProfileInteractor provideProfileInteractor(Context context){
        return new FakeProfileInteractor(
                providesConsumerService()
        );
    }

    @NonNull
    public static AbstractOrderInteractor provideOrderInteractor() {
        return new FakeOrdersInteractor(
                provideOrdersService()
        );
    }

    @NonNull
    public static UserSessionManager provideUserSessionManager(Context context) {
        return new UserSessionManager(context);
    }

    @NonNull
    public static DispatchHelper<List<Restaurant>> provideRestaurantsDispatchHelper(Context context) {
        return new RestaurantsDispatchHelper(
                provideRestaurantsRepository(context)
        );
    }

    public static Repository<Restaurant> provideRestaurantsRepository(Context context) {
        return RestaurantsRepository.getInstance(
                new RestaurantsRemoteRepository(
                        provideRestaurantsInteractor(),
                        "" //TODO: Change
                ),
                RestaurantsLocalRepository.getInstance(
                        getRealmConfig(context),
                        new RealmRestaurantToRestaurantMapper()
                )
        );
    }

    private static OrdersService provideOrdersService() {
        return ServiceGenerator.createService(OrdersService.class);
    }

    private static RestaurantsService provideRestaurantsAPIService() {
        return ServiceGenerator.createService(RestaurantsService.class);
    }

    private static AuthenticationService provideAuthenticationService() {
        return ServiceGenerator.createService(AuthenticationService.class);
    }

    private static SignUpService provideSignUpService() {
        return ServiceGenerator.createService(SignUpService.class);
    }

    private static ConsumersService providesConsumerService() {
        return ServiceGenerator.createService(ConsumersService.class);
    }

    private static FoodDishesService provideFoodDishesAPIService() {
        return ServiceGenerator.createService(FoodDishesService.class);
    }

    public static Repository<FoodDish> provideFoodDishRepository(Context context) {
        return FoodDishesRepository.getInstance(
                new FoodDishesRemoteRepository(
                        provideFoodDishesInteractor(),
                        "" //TODO: Change
                ),
                FoodDishesLocalRepository.getInstance(
                        getRealmConfig(context),
                        new RealmFoodDishToFoodDishMapper()
                )
        );
    }

    private static RealmConfiguration getRealmConfig(Context context) {
        //TODO: Change when finish develop
        return new RealmConfiguration.Builder(context).deleteRealmIfMigrationNeeded().build();
    }

    public static CurrentOrderManager provideCurrentOrderInteractor(Context context) {
        return new CurrentOrderManager(context);
    }

    public static Repository<Order> provideOrdersInteractor(Context context) {
        return OrdersRepository.getInstance(
                new OrdersRemoteRepository(
                        provideOrderInteractor(),
                        "" //TODO: Change
                ),
                OrdersLocalRepository.getInstance(
                        getRealmConfig(context),
                        new RealmOrderToOrderMapper()
                )
        );
    }
}

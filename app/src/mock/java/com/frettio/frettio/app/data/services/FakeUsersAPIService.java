package com.frettio.frettio.app.data.services;

import android.support.annotation.VisibleForTesting;
import android.support.v4.util.ArrayMap;

import com.frettio.frettio.data.remote.responses.SignInResponse;
import com.frettio.frettio.data.remote.services.AuthenticationService;
import com.frettio.frettio.domain.model.User;
import com.frettio.frettio.presentation.mvp.Repository;

import retrofit2.Call;
import retrofit2.http.Body;

public class FakeUsersAPIService implements AuthenticationService {

    // TODO replace this with a new test specific data set.
    private static final ArrayMap<String, User> SERVICE_DATA = new ArrayMap();

    public void getUser(String userId, Repository.SingleItemInformationCallback<User> callback) {
        User user = SERVICE_DATA.get(userId);
        callback.onItemInformationLoaded(user);
    }

    public void saveUser(User user) {
        SERVICE_DATA.put(user.getEmail(), user);
    }

    @VisibleForTesting
    public static void addUser(User... users) {
        for (User user : users) {
            SERVICE_DATA.put(user.getEmail(), user);
        }
    }

    @Override
    public Call<SignInResponse> signIn(@Body User user) {
        //WARNING: This function does not needs to be implemented.
        return null;
    }
}
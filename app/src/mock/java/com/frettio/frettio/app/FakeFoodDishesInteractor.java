package com.frettio.frettio.app;

import android.util.Log;

import com.frettio.frettio.data.remote.services.FoodDishesService;
import com.frettio.frettio.domain.interactor.AbstractFoodDishInteractor;
import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.presentation.mvp.Repository;

import java.util.ArrayList;

import static com.frettio.frettio.app.common.AppConstants.TAG;

/**
 * Created by luisburgos on 3/3/17.
 */

public class FakeFoodDishesInteractor extends AbstractFoodDishInteractor {

    private final ArrayList<FoodDish> loadedList;

    public FakeFoodDishesInteractor(FoodDishesService apiService) {
        super(apiService);

        loadedList = new ArrayList<>();
        loadedList.add(new FoodDish(1, "res1@res.com", "Platillo 1", "Un platillo de restaurante 1", 100, "http://lorempixel.com/200/200/food/"));
        loadedList.add(new FoodDish(2, "res1@res.com", "Platillo 2", "Un platillo de restaurante 2", 190, "http://lorempixel.com/200/200/food/"));
        loadedList.add(new FoodDish(3, "res1@res.com", "Platillo 3", "Un platillo de restaurante 3", 200, "http://lorempixel.com/200/200/food/"));
        loadedList.add(new FoodDish(4, "res1@res.com", "Platillo 4", "Un platillo de restaurante 4", 150, "http://lorempixel.com/200/200/food/"));
        loadedList.add(new FoodDish(5, "res1@res.com", "Platillo 5", "Un platillo de restaurante 5", 300, "http://lorempixel.com/200/200/food/"));
        //loadedList.add(new FoodDish(6, "res2@res.com", "Platillo 6", "Un platillo de restaurante 6", 500, "http://lorempixel.com/200/200/food/"));
        //loadedList.add(new FoodDish(7, "res2@res.com", "Platillo 7", "Un platillo de restaurante 7", 650, "http://lorempixel.com/200/200/food/"));
        //loadedList.add(new FoodDish(8, "res2@res.com", "Platillo 8", "Un platillo de restaurante 8", 650, "http://lorempixel.com/200/200/food/"));
    }

    @Override
    public void getAllFoodDishes(String serverToken, Repository.ListAllCallback<FoodDish> callback) {
        Log.d(TAG, "Loading from fake interactor total of " + loadedList.size());

        callback.onItemsLoaded(loadedList);
    }
}

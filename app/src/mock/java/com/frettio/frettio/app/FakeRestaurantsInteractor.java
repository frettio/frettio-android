package com.frettio.frettio.app;

import android.util.Log;

import com.frettio.frettio.data.remote.services.RestaurantsService;
import com.frettio.frettio.domain.interactor.AbstractRestaurantInteractor;
import com.frettio.frettio.domain.model.Restaurant;
import com.frettio.frettio.presentation.mvp.Repository;

import java.util.ArrayList;

import static com.frettio.frettio.app.common.AppConstants.TAG;

/**
 * Created by luisburgos on 2/25/17.
 */

public class FakeRestaurantsInteractor extends AbstractRestaurantInteractor {

    private final ArrayList<Restaurant> loadedList;

    public FakeRestaurantsInteractor(RestaurantsService apiService) {
        super(apiService);

        loadedList = new ArrayList<>();
        loadedList.add(new Restaurant("res1@res.com", "Restaurante 1", "Sucursal Montejo", "Un mal restaurante 1", "http://lorempixel.com/200/200/sports/"));
        loadedList.add(new Restaurant("res2@res.com", "Establecimiento 2", "Sucursal Centro", "Un mal restaurante 2", "http://lorempixel.com/200/200/sports/"));
        loadedList.add(new Restaurant("res3@res.com", "Restaurante 3", "Sucursal Centro", "Un buen restaurante 3", "http://lorempixel.com/200/200/sports/"));
        loadedList.add(new Restaurant("res4@res.com", "Establecimiento 4", "Sucursal Gran Plaza", "Un buen restaurante 4", "http://lorempixel.com/200/200/sports/"));
        loadedList.add(new Restaurant("res5@res.com", "Restaurante 5", "Sucursal Montejo", "Un buen establecimiento 5", "http://lorempixel.com/200/200/sports/"));
        loadedList.add(new Restaurant("res6@res.com", "Restaurante 6", "Sucursal Montejo", "Un buen restaurante 6", "http://lorempixel.com/200/200/sports/"));
    }

    @Override
    public void getAllRestaurants(String serverToken, Repository.ListAllCallback<Restaurant> callback) {
        Log.d(TAG, "Loading from fake interactor total of " + loadedList.size());

        callback.onItemsLoaded(loadedList);
    }

    @Override
    public void getRestaurant(String serverToken, Repository.SingleItemInformationCallback<Restaurant> callback) {

    }
}

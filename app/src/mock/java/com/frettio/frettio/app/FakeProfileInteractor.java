package com.frettio.frettio.app;

import android.util.Log;

import com.frettio.frettio.data.remote.services.ConsumersService;
import com.frettio.frettio.domain.interactor.AbstractProfileInteractor;
import com.frettio.frettio.domain.model.User;
import com.frettio.frettio.presentation.mvp.Repository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.frettio.frettio.app.common.AppConstants.TAG;

/**
 * Created by Melissa on 25/02/2017.
 */

public class FakeProfileInteractor extends AbstractProfileInteractor {

    public FakeProfileInteractor(ConsumersService sessionsAPIService) {
        super(sessionsAPIService);
    }

    @Override
    public void getConsumer(String email, final Repository.SingleItemInformationCallback<User> callback) {
        Log.d(TAG, "PRODUCTION GET CONSUMER");

        /*Call<User> call = apiService.getConsumer(email);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d(TAG, "RESPONSE: " + response.raw().toString());
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();
                callback.onServerError(t.getMessage());
            }
        });*/
    }

    @Override
    public void updateConsumer(User user, final Repository.SingleItemInformationCallback<User> callback) {
        Log.d(TAG, "PRODUCTION UPDATE CONSUMER");

        callback.onItemInformationLoaded(user);
        /*Call<User> call = apiService.updateConsumer(user.getEmail(), user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d(TAG, "RESPONSE: " + response.raw().toString());
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();
                callback.onServerError(t.getMessage());
            }
        });*/
    }

}
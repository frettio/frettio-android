package com.frettio.frettio.app.common.exceptions;

import java.io.FileNotFoundException;

/**
 * Created by Roberto on 12/23/16.
 */

public class NoneExistingPathFromFile extends FileNotFoundException {

    public NoneExistingPathFromFile() {
        super("File from Uri is null");
    }
}

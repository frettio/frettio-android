package com.frettio.frettio.app.common.exceptions;

import java.util.concurrent.ExecutionException;

/**
 * Created by luisburgos on 8/10/16.
 */
public class NullSessionToken extends ExecutionException {

    public NullSessionToken() {
        super("Session token is null");
    }

}

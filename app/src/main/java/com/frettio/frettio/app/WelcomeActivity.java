package com.frettio.frettio.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.frettio.frettio.R;
import com.frettio.frettio.app.common.utils.ActivityHelper;
import com.frettio.frettio.app.common.utils.TypefaceUtils;
import com.frettio.frettio.presentation.signin.SignInActivity;
import com.frettio.frettio.presentation.signup.SignUpActivity;

public class WelcomeActivity extends AppCompatActivity {

    TextView appLogoTitle;
    Button signInButton;
    Button signUpButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        bindViews();
        setupAppLogoTitle();
        setupActionsListeners();
    }

    private void bindViews() {
        appLogoTitle = (TextView) findViewById(R.id.appLogoTitle);
        signInButton = (Button) findViewById(R.id.signInButton);
        signUpButton = (Button) findViewById(R.id.signUpButton);

        final TextView appLogoDescription = (TextView) findViewById(R.id.appLogoDescription);
        appLogoDescription.setVisibility(View.VISIBLE);
    }

    private void setupAppLogoTitle() {
        TypefaceUtils.setLobsterTypeface(this, appLogoTitle);
    }

    private void setupActionsListeners() {
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityHelper.getInstance().begin(WelcomeActivity.this, SignInActivity.class);
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityHelper.getInstance().begin(WelcomeActivity.this, SignUpActivity.class);
            }
        });
    }

}

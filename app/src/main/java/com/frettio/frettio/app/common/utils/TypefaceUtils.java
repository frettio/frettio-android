package com.frettio.frettio.app.common.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.Button;
import android.widget.TextView;

/**
 * Facilitates typeface manipulation on UI components.
 *
 * Created by luisburgos on 25/03/16.
 */
public class TypefaceUtils {

    private static final String FONTS_DIR = "fonts/";
    static final String LOBSTER_TYPEFACE = "lobster.otf";

    /**
     * Setups a LOBSTER typeface on a Button.
     *
     * @param context context of caller activity/fragment.
     * @param view a {@link Button} where the typeface is going to be performed.
     */
    public static void setLobsterTypeface(Context context, final Button view){
        setTypeface(context, view, LOBSTER_TYPEFACE);
    }

    /**
     * Setups a LOBSTER typeface on a TextView.
     *
     * @param context context of caller activity/fragment.
     * @param view a {@link TextView} where the typeface is going to be performed.
     */
    public static void setLobsterTypeface(Context context, final TextView view){
        setTypeface(context, view, LOBSTER_TYPEFACE);
    }

    /**
     * Setups a specified typeface on a Button.
     *
     * @param context context of caller activity/fragment.
     * @param button a {@link Button} where the typeface is going to be performed.
     * @param typeface name of the typeface required.
     */
    static void setTypeface(Context context, final Button button, String typeface){
        Typeface mTypeface = Typeface.createFromAsset(context.getAssets(), FONTS_DIR + typeface);
        if(button != null){
            button.setTypeface(mTypeface);
        }
    }

    /**
     * Setups a specified typeface on a TextView.
     *
     * @param context context of caller activity/fragment
     * @param textView a {@link TextView} where the typeface is going to be performed.
     * @param typeface name of the typeface required.
     */
    static void setTypeface(Context context, final TextView textView, String typeface){
        Typeface mTypeface = Typeface.createFromAsset(context.getAssets(), FONTS_DIR + typeface);
        if(textView != null){
            textView.setTypeface(mTypeface);
        }
    }

}
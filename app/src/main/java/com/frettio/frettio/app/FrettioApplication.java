package com.frettio.frettio.app;

import android.app.Application;

import com.frettio.frettio.app.common.utils.ActivityHelper;
import com.frettio.frettio.presentation.main.MainActivity;
import com.squareup.picasso.Picasso;

/**
 * Created by luisburgos on 1/27/17.
 */

public class FrettioApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ActivityHelper.getInstance().setMainClass(MainActivity.class);

        Picasso.with(getApplicationContext()).setLoggingEnabled(true);
    }
}

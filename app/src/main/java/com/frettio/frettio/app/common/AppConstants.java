package com.frettio.frettio.app.common;

/**
 * Created by luisburgos on 2/19/17.
 */

public class AppConstants {

    public static final String TAG = "FRETTIO";

    public static class Messages {
        public static final String NOT_SAME_RESTAURANT = "El platillo no es el mismo restaurante que el resto de platillos de tu orden";
    }

    public static class Restaurants {
        public static final String PENDING = "pending";
        public static final String ACCEPTED = "accepted";
    }
}

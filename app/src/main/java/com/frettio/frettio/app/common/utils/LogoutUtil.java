package com.frettio.frettio.app.common.utils;

import android.app.Activity;
import android.content.Context;

import com.frettio.frettio.R;
import com.frettio.frettio.app.Injection;
import com.frettio.frettio.presentation.dispatch.DispatchActivity;
import com.frettio.frettio.presentation.widget.LogoutDialog;

/**
 * Created by espinzonv on 04/11/16.
 */

public class LogoutUtil {

    public static void requestLogoutConfirmation(final Activity activity) {
        LogoutDialog logoutDialog = new LogoutDialog(activity, R.layout.component_logout_dialog, R.id.btn_confirmation,
                R.id.btn_cancel, new LogoutDialog.OnConfirmationLogout() {
            @Override
            public void onConfirmation() {
                doLogout(activity);
            }

            @Override
            public void onCancel() {

            }
        });
        logoutDialog.show();
    }

    private static void doLogout(Activity activity){
        Context mContext = activity.getApplicationContext();
        Injection.provideUserSessionManager(mContext).clearSession();
        ActivityHelper.getInstance().sendAndFinish(activity, DispatchActivity.class);
    }
}

package com.frettio.frettio.app.common.utils;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import com.frettio.frettio.R;
import com.squareup.picasso.Picasso;

import static com.frettio.frettio.app.common.AppConstants.TAG;


/**
 * Created by luisburgos on 11/24/16.
 */
//TODO: Improve class API.
public class PicassoCustomLoader {

    public int sizeOfImageViewContainer = 40;

    //TODO: Refactor to add more custom parameters.
    public void load(Context context, ImageView imageView, String imageProfileURL) {
        if(imageProfileURL != null && !imageProfileURL.isEmpty()){
            Log.d(TAG, "Loading image on view " + imageProfileURL);
            Picasso.with(context)
                    .load(imageProfileURL)
                    .transform(new CircleTransformation())
                    .placeholder(R.drawable.adapter_image_load_placeholder)
                    .into(imageView);
        } else {
            loadDefaultImage(context, imageView);
        }
    }

    private void loadDefaultImage(Context context, ImageView imageView) {
        Log.d(TAG, "NO IMAGE, Loading default image on view ");
        loadThumb(context, imageView, R.drawable.ic_restaurant_menu_black_24dp);
    }

    public void loadThumb(Context context, ImageView imageView, int drawableId) {
        Picasso.with(context)
                .load(drawableId)
                .resize(sizeOfImageViewContainer, sizeOfImageViewContainer)
                .centerCrop()
                .transform(new CircleTransformation())
                .placeholder(R.drawable.adapter_image_load_placeholder)
                .error(R.drawable.adapter_image_load_error)
                .into(imageView);
    }

    public void loadThumb(Context context, ImageView imageView, String imageProfileURL) {
        if(imageProfileURL != null && !imageProfileURL.isEmpty()){
            Log.d(TAG, "Loading image on view " + imageProfileURL);
            Picasso.with(context)
                    .load(imageProfileURL)
                    .resize(sizeOfImageViewContainer, sizeOfImageViewContainer)
                    .centerCrop()
                    .transform(new CircleTransformation())
                    .placeholder(R.drawable.adapter_image_load_placeholder)
                    .error(R.drawable.adapter_image_load_error)
                    .into(imageView);
        } else {
            loadDefaultImage(context, imageView);
        }
    }
}
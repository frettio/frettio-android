package com.frettio.frettio.app.common.utils;

import android.Manifest;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.frettio.frettio.R;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Roberto on 11/24/16.
 */

public class PermissionHelper {

    public static final String TAG = "PERMISSION";
    private Activity activity;
    private Fragment fragment = null;

    private static final int PERMISSIONS_REQUEST_ALL = 0;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final int PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 2;
    private static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 3;


    public static boolean isRequest;

    public PermissionHelper(Activity activity) {
        this.activity = activity;
    }

    //TODO: Verify API exposure.
    public PermissionHelper(Activity activity, Fragment fragment) {
        this.activity = activity;
        this.fragment = fragment;
        this.isRequest = false;
    }

    @AfterPermissionGranted(PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
    public void checkAccessFineLocationPermission() {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};

        if (EasyPermissions.hasPermissions(activity, permissions)) {
            // Already have permission, so we can continue
            Log.d(TAG, "Already have permissions");
            if (activity instanceof ApplicationPermissionsCallbacks) {
                Log.d(TAG, "Notifying to activity");
                ((ApplicationPermissionsCallbacks) activity).onNoNeedToAskForAlreadyGrantedPermissions();
            } else if (fragment != null && fragment instanceof ApplicationPermissionsCallbacks){
                Log.d(TAG, "Notifying to fragment");
                ((ApplicationPermissionsCallbacks) fragment).onNoNeedToAskForAlreadyGrantedPermissions();
            } else {
                Log.d(TAG, "Cannot notify to listener");
            }
        } else {
            // Do not have permissions, request them now
            Log.d(TAG, "Not have permissions");
            if(!isRequest){
                isRequest=true;
                EasyPermissions.requestPermissions(
                        activity,
                        activity.getResources().getString(R.string.fine_location_permission),
                        PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION,
                        permissions
                );
            }

        }
    }

    @AfterPermissionGranted(PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE)
    public void checkReadExternalStorage() {
        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};

        if (EasyPermissions.hasPermissions(activity, permissions)) {
            // Already have permission, so we can continue
            Log.d(TAG, "Already have permissions");
            if (activity instanceof ApplicationPermissionsCallbacks) {
                ((ApplicationPermissionsCallbacks) activity).onNoNeedToAskForAlreadyGrantedPermissions();
            }
        } else {
            // Do not have permissions, request them now
            Log.d(TAG, "Not have permissions");
            EasyPermissions.requestPermissions(
                    activity,
                    activity.getResources().getString(R.string.read_external_storage_permission),
                    PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE,
                    permissions
            );
        }
    }

    @AfterPermissionGranted(PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE)
    public void checkWriteExternalStorage() {
        String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (EasyPermissions.hasPermissions(activity, permissions)) {
            // Already have permission, so we can continue
            Log.d("PERMISSION", "Already have permissions");
            if (activity instanceof ApplicationPermissionsCallbacks) {
                ((ApplicationPermissionsCallbacks) activity).onNoNeedToAskForAlreadyGrantedPermissions();
            }
        } else {
            // Do not have permissions, request them now
            Log.d("PERMISSION", "Not have permissions");
            EasyPermissions.requestPermissions(
                    activity,
                    activity.getResources().getString(R.string.read_external_storage_permission), //TODO: Change.
                    PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE,
                    permissions
            );
        }
    }

    @AfterPermissionGranted(PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE)
    public void checkAllPermissions(){
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

        if (EasyPermissions.hasPermissions(activity, permissions)) {
            // Already have permission, so we can continue
            Log.d(TAG, "Already have permissions");
            if (activity instanceof ApplicationPermissionsCallbacks) {
                ((ApplicationPermissionsCallbacks) activity).onNoNeedToAskForAlreadyGrantedPermissions();
            }
        } else {
            // Do not have permissions, request them now
            Log.d(TAG, "Not have permissions");
            EasyPermissions.requestPermissions(
                    activity,
                    activity.getResources().getString(R.string.all_permissions),
                    PERMISSIONS_REQUEST_ALL,
                    permissions
            );
        }
    }

    public interface ApplicationPermissionsCallbacks extends EasyPermissions.PermissionCallbacks {
        void onNoNeedToAskForAlreadyGrantedPermissions();
    }

}

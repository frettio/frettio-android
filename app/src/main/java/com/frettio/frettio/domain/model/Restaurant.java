package com.frettio.frettio.domain.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by luisburgos on 1/27/17.
 */

public class Restaurant extends BaseObject {

    @SerializedName("email")
    private String email;

    @SerializedName("name")
    private String name;

    @SerializedName("branchOffice")
    private String branchOffice;

    @SerializedName("description")
    private String description;

    @SerializedName("owner")
    private String owner;

    @SerializedName("logo")
    private String logo;

    @SerializedName("state")
    private String state;

    @SerializedName("addressId")
    private long addressId;

    @SerializedName("Address")
    private HomeAddress address;

    public Restaurant(){

    }

    public Restaurant(String email, String name, String branchOffice, String description, String logo) {
        this.email = email;
        this.name = name;
        this.branchOffice = branchOffice;
        this.description = description;
        this.logo = logo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBranchOffice() {
        return branchOffice;
    }

    public void setBranchOffice(String branchOffice) {
        this.branchOffice = branchOffice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getAddressId() {
        return addressId;
    }

    public void setAddressId(long addressId) {
        this.addressId = addressId;
    }

}

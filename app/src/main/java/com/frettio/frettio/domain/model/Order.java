package com.frettio.frettio.domain.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by luisburgos on 3/2/17.
 */

public class Order extends BaseObject {

    @SerializedName("id")
    private long id;

    @SerializedName("date")
    private String date;

    @SerializedName("total")
    private double total;

    @SerializedName("orderStatus")
    private String orderStatus;

    @SerializedName("OrderDishes")
    private ArrayList<FoodDish> dishes;

    @SerializedName("Consumer")
    private Consumer consumer;

    @SerializedName("Restaurant")
    private Restaurant restaurant;

    public Order() {
    }

    public Order(long id, double total) {
        this.id = id;
        this.total = total;
        this.dishes = new ArrayList<>(0);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public ArrayList<FoodDish> getDishes() {
        return dishes;
    }

    public void setDishes(ArrayList<FoodDish> dishes) {
        this.dishes = dishes;
    }

    public Consumer getConsumer() {
        return consumer;
    }

    public void setConsumer(Consumer consumer) {
        this.consumer = consumer;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
}

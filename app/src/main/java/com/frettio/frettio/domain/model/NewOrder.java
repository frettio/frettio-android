package com.frettio.frettio.domain.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by luisburgos on 4/16/17.
 */

public class NewOrder {

    @SerializedName("restaurantEmail")
    private String restaurantEmail;

    @SerializedName("restaurantName")
    private String restaurantName;

    @SerializedName("consumerEmail")
    private String consumerEmail;

    @SerializedName("total")
    private double total;

    @SerializedName("dishes")
    private ArrayList<FoodDish> dishes;

    public NewOrder() {
    }

    public NewOrder(String restaurantEmail, String restaurantName, String consumerEmail, double total) {
        this.restaurantEmail = restaurantEmail;
        this.restaurantName = restaurantName;
        this.consumerEmail = consumerEmail;
        this.total = total;
        this.dishes = new ArrayList<>(0);
    }

    public String getRestaurantEmail() {
        return restaurantEmail;
    }

    public void setRestaurantEmail(String restaurantEmail) {
        this.restaurantEmail = restaurantEmail;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getConsumerEmail() {
        return consumerEmail;
    }

    public void setConsumerEmail(String consumerEmail) {
        this.consumerEmail = consumerEmail;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public ArrayList<FoodDish> getDishes() {
        return dishes;
    }

    public void setDishes(ArrayList<FoodDish> dishes) {
        this.dishes = dishes;
    }
}

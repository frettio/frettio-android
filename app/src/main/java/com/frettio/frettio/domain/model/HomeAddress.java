package com.frettio.frettio.domain.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by luisburgos on 2/12/17.
 */

public class HomeAddress extends BaseObject {

    @SerializedName("id")
    private long id;

    @SerializedName("number")
    private String number;

    @SerializedName("city")
    private String city;

    @SerializedName("street")
    private String street;

    @SerializedName("intersection")
    private String crossStreets;

    @SerializedName("colony")
    private String houseBlock;

    public HomeAddress(long id, String number, String city, String street, String crossStreets, String houseBlock) {
        this(number, city, street, crossStreets, houseBlock);
        this.id = id;
    }

    public HomeAddress(String number, String city, String street, String crossStreets, String houseBlock) {
        this.number = number;
        this.city = city;
        this.street = street;
        this.crossStreets = crossStreets;
        this.houseBlock = houseBlock;
    }

    public HomeAddress() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCrossStreets() {
        return crossStreets;
    }

    public void setCrossStreets(String crossStreets) {
        this.crossStreets = crossStreets;
    }

    public String getHouseBlock() {
        return houseBlock;
    }

    public void setHouseBlock(String houseBlock) {
        this.houseBlock = houseBlock;
    }

}

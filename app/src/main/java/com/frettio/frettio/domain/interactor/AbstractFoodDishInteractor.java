package com.frettio.frettio.domain.interactor;

import com.frettio.frettio.data.remote.services.FoodDishesService;
import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.presentation.mvp.Repository;

/**
 * Created by luisburgos on 3/3/17.
 */

public abstract class AbstractFoodDishInteractor extends AbstractInteractor<FoodDishesService> {

    public AbstractFoodDishInteractor(FoodDishesService apiService) {
        super(apiService);
    }

    public abstract void getAllFoodDishes(Repository.ListAllCallback<FoodDish> callback);
}


package com.frettio.frettio.domain.model;

/**
 * Created by luisburgos on 2/25/17.
 */

public enum RestaurantState {
    PENDING("pending"),
    ACCEPTED("accepted");

    private final String text;

    /**
     * @param text
     */
    private RestaurantState(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}

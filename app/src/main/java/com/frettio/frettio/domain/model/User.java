package com.frettio.frettio.domain.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by luisburgos on 1/27/17.
 */

public class User extends BaseObject {

    @SerializedName("email")
    private String email;

    private String password;

    @SerializedName("name")
    private String firstName;

    @SerializedName("lastName")
    private String lastNames;

    @SerializedName("phone")
    private String phone;

    private String imageURL;

    @SerializedName("Address")
    private HomeAddress homeAddress;

    @SerializedName("role")
    private String role;

    public User(String email, String password, String firstName, String lastNames, String phone) {
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.lastNames = lastNames;
        this.phone = phone;
    }

    public User(){

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastNames() {
        return lastNames;
    }

    public void setLastNames(String lastNames) {
        this.lastNames = lastNames;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public HomeAddress getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(HomeAddress homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}

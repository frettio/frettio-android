package com.frettio.frettio.domain.interactor;

import com.frettio.frettio.data.remote.services.ConsumersService;
import com.frettio.frettio.domain.model.Consumer;
import com.frettio.frettio.domain.model.User;
import com.frettio.frettio.presentation.mvp.Repository;

/**
 * Created by Melissa on 25/02/2017.
 */

public abstract class AbstractProfileInteractor extends AbstractInteractor<ConsumersService> {
    public AbstractProfileInteractor(ConsumersService sessionsAPIService) {
        super(sessionsAPIService);
    }

    public abstract void getConsumer(String email, final Repository.SingleItemInformationCallback<User> callback);
    public abstract void updateConsumer(Consumer user, final Repository.SingleItemInformationCallback<User> callback);
}

package com.frettio.frettio.domain.interactor;

import com.frettio.frettio.data.remote.services.SignUpService;
import com.frettio.frettio.domain.model.Consumer;
import com.frettio.frettio.domain.model.User;
import com.frettio.frettio.presentation.mvp.Repository;

/**
 * Created by luisburgos on 2/19/17.
 */

public abstract class AbstractSignUpInteractor extends AbstractInteractor<SignUpService> {

    public AbstractSignUpInteractor(SignUpService apiService) {
        super(apiService);
    }

    public abstract void signUp(Consumer user, final Repository.SingleItemInformationCallback<User> callback);

}

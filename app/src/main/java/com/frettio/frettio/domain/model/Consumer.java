package com.frettio.frettio.domain.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by luisburgos on 4/15/17.
 */

public class Consumer extends BaseObject {

    @SerializedName("email")
    private String email;

    private String password;

    @SerializedName("name")
    private String firstName;

    @SerializedName("lastName")
    private String lastNames;

    @SerializedName("phone")
    private String phone;

    @SerializedName("number")
    private String number;

    @SerializedName("city")
    private String city;

    @SerializedName("street")
    private String street;

    @SerializedName("intersection")
    private String crossStreets;

    @SerializedName("colony")
    private String houseBlock;

    @SerializedName("addressId")
    private long addressId;

    @SerializedName("Address")
    private HomeAddress address;

    public Consumer(String email, String password, String firstName, String lastNames, String phone,
                    String number, String city, String street, String crossStreets, String houseBlock
    ) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastNames = lastNames;
        this.phone = phone;
        this.number = number;
        this.city = city;
        this.street = street;
        this.crossStreets = crossStreets;
        this.houseBlock = houseBlock;
    }

    public Consumer(){

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastNames() {
        return lastNames;
    }

    public void setLastNames(String lastNames) {
        this.lastNames = lastNames;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCrossStreets() {
        return crossStreets;
    }

    public void setCrossStreets(String crossStreets) {
        this.crossStreets = crossStreets;
    }

    public String getHouseBlock() {
        return houseBlock;
    }

    public void setHouseBlock(String houseBlock) {
        this.houseBlock = houseBlock;
    }

    public HomeAddress getAddress() {
        return address;
    }

    public void setAddress(HomeAddress address) {
        this.address = address;
    }
}

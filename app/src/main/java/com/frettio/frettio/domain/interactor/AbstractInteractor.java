package com.frettio.frettio.domain.interactor;

import com.frettio.frettio.data.remote.services.APIService;

import static com.frettio.frettio.app.common.utils.ActivityHelper.checkNotNull;

public abstract class AbstractInteractor<ConcreteService> implements Interactor {

    protected ConcreteService apiService;

    public AbstractInteractor(
            ConcreteService sessionsAPIService
    ) {
        this.apiService = checkNotNull(sessionsAPIService);
    }

}

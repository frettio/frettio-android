package com.frettio.frettio.domain.interactor;

import com.frettio.frettio.data.remote.services.RestaurantsService;
import com.frettio.frettio.domain.model.Restaurant;
import com.frettio.frettio.presentation.mvp.Repository;

/**
 * Created by luisburgos on 2/25/17.
 */

public abstract class AbstractRestaurantInteractor extends AbstractInteractor<RestaurantsService> {

    public AbstractRestaurantInteractor(RestaurantsService apiService) {
        super(apiService);
    }

    public abstract void getAllRestaurants(Repository.ListAllCallback<Restaurant> callback);

    public abstract void getRestaurant(int id, Repository.SingleItemInformationCallback<Restaurant> callback);
}

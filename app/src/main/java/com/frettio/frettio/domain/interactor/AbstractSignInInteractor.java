package com.frettio.frettio.domain.interactor;

import com.frettio.frettio.data.remote.services.AuthenticationService;
import com.frettio.frettio.domain.model.User;
import com.frettio.frettio.presentation.mvp.Repository;

/**
 * Created by luisburgos on 2/19/17.
 */

public abstract class AbstractSignInInteractor extends AbstractInteractor<AuthenticationService> {

    public AbstractSignInInteractor(AuthenticationService sessionsAPIService) {
        super(sessionsAPIService);
    }

    public abstract void signIn(
            final String email,
            final String password,
            final Repository.CompositeItemInformationCallback<User, String> callback
    );
}

package com.frettio.frettio.domain.interactor;

import com.frettio.frettio.data.remote.services.OrdersService;
import com.frettio.frettio.domain.model.NewOrder;
import com.frettio.frettio.domain.model.Order;
import com.frettio.frettio.presentation.mvp.Repository;

/**
 * Created by luisburgos on 3/5/17.
 */

public abstract class AbstractOrderInteractor extends AbstractInteractor<OrdersService> {

    public AbstractOrderInteractor(OrdersService apiService) {
        super(apiService);
    }

    public abstract void getAllOrders(
            String emailUser,
            Repository.ListAllCallback<Order> callback
    );

    public abstract void createOrder(
            NewOrder order,
            Repository.SingleItemInformationCallback<NewOrder> callback
    );

}


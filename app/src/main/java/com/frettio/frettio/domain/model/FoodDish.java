package com.frettio.frettio.domain.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by luisburgos on 1/27/17.
 */

public class FoodDish extends BaseObject {

    @SerializedName("id")
    private long id;

    private String restaurantEmail;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("cost")
    private double cost;

    @SerializedName("preparationTime")
    private String preparationTime;

    @SerializedName("picture")
    private String imageURL;

    //Only to some app context
    @SerializedName("quantity")
    private int quantity;
    private boolean isAddedToOrder;

    public FoodDish() {

    }

    public FoodDish(long id, String restaurantEmail, String name, String description, double cost, String imageURL) {
        this.id = id;
        this.restaurantEmail = restaurantEmail;
        this.name = name;
        this.description = description;
        this.cost = cost;
        this.imageURL = imageURL;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRestaurantEmail() {
        return restaurantEmail;
    }

    public void setRestaurantEmail(String restaurantEmail) {
        this.restaurantEmail = restaurantEmail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(String preparationTime) {
        this.preparationTime = preparationTime;
    }

    public void markAsAddedToOrder(boolean addedToOrder) {
        isAddedToOrder = addedToOrder;
    }

    public boolean isAlreadyInCurrentOrder() {
        return isAddedToOrder;
    }
}

package com.frettio.frettio.presentation.dispatch;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.frettio.frettio.R;
import com.frettio.frettio.app.Injection;
import com.frettio.frettio.app.WelcomeActivity;
import com.frettio.frettio.app.common.utils.ActivityHelper;
import com.frettio.frettio.app.common.utils.DispatchHelper;
import com.frettio.frettio.data.APIConstants;
import com.frettio.frettio.data.local.managers.UserSessionManager;
import com.frettio.frettio.domain.model.Restaurant;

import java.util.List;

/**
 * This class only decides the main flow for the application.
 * This class should not be visible for the user, it only validates if the user is logged in.
 *
 * This class use a helper to prefetch data from data source before starts. If the token
 * has expired, this means that the user session on the app has expired and
 * the app need to request the user to signIn on his account again. If there is
 * no problem with the token the app redirects the user to the main view depending on his role.
 *
 */
public class DispatchActivity extends AppCompatActivity {

    private View mainView;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainView = findViewById(android.R.id.content);
    }

    @Override
    protected void onResume() {
        super.onResume();

        hideFailedMessageIfRoom();

        UserSessionManager sessionManager = Injection.provideUserSessionManager(this);

        if (!sessionManager.isSessionSaved()) {
            ActivityHelper.getInstance().sendAndFinish(DispatchActivity.this, WelcomeActivity.class);
        } else {
            if(sessionManager.isValidSession()){
                prefetchInformation();
            } else {
                showNeedToLoginMessage();
            }
        }
    }

    private void prefetchInformation() {
        ActivityHelper.getInstance().sendToMainFrom(DispatchActivity.this);
        DispatchHelper<List<Restaurant>> mDispatchHelper = Injection.provideRestaurantsDispatchHelper(this);
        mDispatchHelper.begin(new DispatchHelper.DispatchListener<List<Restaurant>>() {
            @Override
            public void onFetchData(List<Restaurant> elders) {
                ActivityHelper.getInstance().sendToMainFrom(DispatchActivity.this);
            }

            @Override
            public void onError(String error) {
                if(error.equals(APIConstants.UNAUTHORIZED)){
                    showNeedToLoginMessage();
                } else {
                    showErrorMessage();
                }
            }
        });
    }

    private void hideFailedMessageIfRoom() {
        if(snackbar != null){
            snackbar.dismiss();
        }
    }

    private void showErrorMessage() {
        snackbar = Snackbar.make(mainView, R.string.error_has_ocurred, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.retry, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prefetchInformation();
            }
        });
        snackbar.show();
    }

    private void showNeedToLoginMessage() {
        snackbar = Snackbar.make(mainView, R.string.your_session_has_expired, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.sign_in, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Injection.provideUserSessionManager(DispatchActivity.this).clearSession();
                ActivityHelper.getInstance().sendAndFinish(DispatchActivity.this, WelcomeActivity.class);
            }
        });
        snackbar.show();
    }
}



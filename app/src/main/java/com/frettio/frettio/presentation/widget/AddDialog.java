package com.frettio.frettio.presentation.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Roberto on 12/21/16.
 */

public class AddDialog extends Dialog implements View.OnClickListener {

    private Context mContext;
    private OnAddition mListener;
    private Dialog mDialog;
    private Button btnConfirmation;
    private Button btnCancel;
    private EditText mInputText;
    private String mErrorMessage;
    private int mDialogLayoutID;
    private int mConfirmationButtonID;
    private int mCancelButtonID;
    private int mInputTextID;

    public AddDialog(
            Context context,
            int dialogLayoutID,
            int confirmationButtonID,
            int cancelButtonID,
            String errorMessage,
            OnAddition listener
    ) {
        super(context);
        this.mContext = context;
        this.mDialogLayoutID = dialogLayoutID;
        this.mConfirmationButtonID = confirmationButtonID;
        this.mCancelButtonID = cancelButtonID;
        this.mListener = listener;
        this.mErrorMessage = errorMessage;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(mDialogLayoutID);
        btnConfirmation = (Button) findViewById(mConfirmationButtonID);
        btnCancel = (Button) findViewById(mCancelButtonID);
        mInputText = (EditText) findViewById(mInputTextID);
        btnCancel.setOnClickListener(this);
        btnConfirmation.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(getWindow() != null){
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == mConfirmationButtonID){
            String textAdded = mInputText.getText().toString();

            if(textAdded.equals("")){
                mInputText.setError(mErrorMessage);
            }else {
                this.dismiss();
                mListener.onTextAdded(textAdded);
            }
        }else if (view.getId() == mCancelButtonID){
            this.dismiss();
            mListener.onCancel();
        }
    }

    public interface OnAddition {
        void onTextAdded(String addedText);
        void onCancel();
    }
}

package com.frettio.frettio.presentation.signup;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.frettio.frettio.R;
import com.frettio.frettio.app.Injection;
import com.frettio.frettio.app.common.utils.ActivityHelper;
import com.frettio.frettio.app.common.utils.Utils;
import com.frettio.frettio.presentation.mvp.activity.BaseActivityView;
import com.frettio.frettio.presentation.signin.SignInActivity;

public class SignUpActivity extends BaseActivityView implements SignUpContract.View {

    private EditText inputName;
    private EditText inputLastName;
    private EditText inputEmail;
    private EditText inputPhoneNumber;
    private EditText inputPassword;
    private EditText inputPasswordConfirm;
    private EditText inputNumber;
    private EditText inputCrossStreets;
    private EditText inputHouseBlock;
    private EditText inputStreet;
    private EditText inputCity;

    private ProgressBar progress;
    private RelativeLayout signUpContainer;

    private SignUpContract.UserActionsListener mActionListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView(R.layout.activity_sign_up);
        setupPresenter();
    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void setEmailErrorMessage() {
        inputEmail.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setPasswordErrorMessage() {
        inputPassword.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setFirstNameErrorMessage() {
        inputName.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setLastNamesErrorMessage() {
        inputLastName.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setPhoneErrorMessage() {
        inputPhoneNumber.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setStreetNumberErrorMessage() {
        inputNumber.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setCityErrorMessage() {
        inputCity.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setStreetNameErrorMessage() {
        inputStreet.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setCrossStreetsErrorMessage() {
        inputCrossStreets.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setHouseBlockErrorMessage() {
        inputHouseBlock.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void sendToLogin() {
        ActivityHelper.getInstance().sendAndFinish(this, SignInActivity.class);
    }

    @Override
    public void setPasswordConfirmationErrorMessage() {
        inputPasswordConfirm.setError(getString(R.string.error_password_confirmation_field));
    }

    @Override
    public void clearErrors() {
        inputEmail.setError(null);
        inputPassword.setError(null);
        inputPasswordConfirm.setError(null);
        inputName.setError(null);
        inputLastName.setError(null);
        inputPhoneNumber.setError(null);
        inputNumber.setError(null);
        inputCity.setError(null);
        inputStreet.setError(null);
        inputCrossStreets.setError(null);
        inputHouseBlock.setError(null);
    }

    @Override
    public void setProgressIndicator(boolean active) {
        if(active) {
            signUpContainer.setVisibility(View.INVISIBLE);
            progress.setVisibility(View.VISIBLE);
        } else {
            progress.setVisibility(View.INVISIBLE);
            signUpContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected ViewGroup getMainLayout() {
        return signUpContainer;
    }

    @Override
    protected void initViews() {
        inputName = (EditText) findViewById(R.id.inputName);
        inputLastName = (EditText) findViewById(R.id.inputLastName);
        inputEmail = (EditText) findViewById(R.id.inputEmail);
        inputPhoneNumber = (EditText) findViewById(R.id.inputPhoneNumber);
        inputPassword = (EditText) findViewById(R.id.inputPassword);
        inputPasswordConfirm = (EditText) findViewById(R.id.inputPasswordConfirm);
        inputNumber = (EditText) findViewById(R.id.inputNumber);
        inputCrossStreets = (EditText) findViewById(R.id.inputCrossStreets);
        inputHouseBlock = (EditText) findViewById(R.id.inputHouseBlock);
        inputStreet = (EditText) findViewById(R.id.inputStreet);
        inputCity = (EditText) findViewById(R.id.inputCity);
        progress = (ProgressBar) findViewById(R.id.progress);
        signUpContainer = (RelativeLayout) findViewById(R.id.signUpContainer);

        final Button signUpButton = (Button) findViewById(R.id.signUpButton);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyboard(SignUpActivity.this);
                mActionListener.doSignUp(
                        inputEmail.getText().toString(),
                        inputPassword.getText().toString(),
                        inputPasswordConfirm.getText().toString(),
                        inputName.getText().toString(),
                        inputLastName.getText().toString(),
                        inputPhoneNumber.getText().toString(),
                        inputNumber.getText().toString(),
                        inputCity.getText().toString(),
                        inputStreet.getText().toString(),
                        inputCrossStreets.getText().toString(),
                        inputHouseBlock.getText().toString()
                );
            }
        });

        final TextView appLogoTitle = (TextView) findViewById(R.id.appLogoTitle);
        appLogoTitle.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryText));

        final TextView alreadyHaveAnAccountAction = (TextView) findViewById(R.id.alreadyHaveAnAccountAction);
        Utils.makeTextViewClickable(alreadyHaveAnAccountAction, new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                mActionListener.alreadyHaveAnAccount();
            }
        });
    }

    @Override
    protected void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void setupPresenter(){
        mActionListener = new SignUpPresenter(this, Injection.provideSignUpInteractor());
    }
}

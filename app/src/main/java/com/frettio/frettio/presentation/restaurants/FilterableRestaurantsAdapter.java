package com.frettio.frettio.presentation.restaurants;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.frettio.frettio.R;
import com.frettio.frettio.app.common.utils.PicassoCustomLoader;
import com.frettio.frettio.domain.model.Restaurant;
import com.frettio.frettio.presentation.adapter.FilterableListAdapter;
import com.frettio.frettio.presentation.adapter.ItemListener;

import java.util.List;

/**
 * Created by luisburgos on 2/23/17.
 */

public class FilterableRestaurantsAdapter extends FilterableListAdapter<Restaurant, RestaurantViewHolder> {
    /**
     * Class constructor.
     *
     * @param restaurants  base list of items.
     * @param filterMethod filter criteria method.
     * @param itemListener listener that responds on item interaction.
     */
    public FilterableRestaurantsAdapter(
            @NonNull List<Restaurant> restaurants,
            @NonNull FilterMethod<Restaurant> filterMethod,
            @NonNull ItemListener<Restaurant> itemListener
    ) {
        super(restaurants, filterMethod, itemListener);
    }

    @Override
    public RestaurantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View noteView = inflater.inflate(R.layout.component_list_item_restaurant, parent, false);
        return new RestaurantViewHolder(noteView);
    }

    @Override
    public void onBindViewHolder(RestaurantViewHolder viewHolder, int position) {
        final Restaurant restaurant = mFilteredItemList.get(position);

        viewHolder.nameTextView.setText(restaurant.getName());
        viewHolder.branchOfficeTextView.setText(restaurant.getBranchOffice());
        viewHolder.descriptionTextView.setText(restaurant.getDescription());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemListener.onItemClick(restaurant);
            }
        });

        new PicassoCustomLoader().load(mContext.getApplicationContext(), viewHolder.logoImageView, restaurant.getLogo());
    }
}

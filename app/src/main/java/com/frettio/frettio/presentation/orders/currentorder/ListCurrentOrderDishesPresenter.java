package com.frettio.frettio.presentation.orders.currentorder;

import android.util.Log;

import com.frettio.frettio.app.common.AppConstants;
import com.frettio.frettio.data.local.managers.CurrentOrderManager;
import com.frettio.frettio.domain.interactor.Interactor;
import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.domain.model.NewOrder;
import com.frettio.frettio.domain.model.Order;
import com.frettio.frettio.presentation.mvp.AbstractPresenter;

import java.util.List;

/**
 * Created by luisburgos on 3/5/17.
 */

public class ListCurrentOrderDishesPresenter
        extends AbstractPresenter<ListCurrentOrderContract.View, Interactor<NewOrder>>
        implements ListCurrentOrderContract.UserActionsListener {

    public ListCurrentOrderDishesPresenter(
            ListCurrentOrderContract.View mView,
            Interactor<NewOrder> mInteractor
    ) {
        super(mView, mInteractor);
    }

    @Override
    public void loadElements(boolean forceUpdate) {
        List<FoodDish> models = ((CurrentOrderManager) mInteractor).getSession().getDishes();
        Log.d(AppConstants.TAG, "Retrieved total of " + models.size());
        mView.setProgressIndicator(false);
        if(models.isEmpty()){
            Log.d(AppConstants.TAG, "Setting empty state");
            mView.setEmptyListState(true);
        } else {
            Log.d(AppConstants.TAG, "Displaying data");
            mView.setEmptyListState(false);
            mView.showElements(models);
        }
    }

    @Override
    public void openElement(FoodDish element) {
        //DO NOTHING!!
    }

    @Override
    public boolean removeFromOrder(int position, FoodDish dish) {
        ((CurrentOrderManager) mInteractor).removeFromOrder(dish);
        if(((CurrentOrderManager) mInteractor).getListOfDishes().isEmpty()){
            mView.setEmptyListState(true);
        } else {
            mView.displayRemovedFromOrderMessage(position);
        }
        return true;
    }

    @Override
    public void increaseNumberOfSameDishes(int newNumber, int fromPosition, FoodDish ofDish) {
        ofDish.setQuantity(newNumber);
        ((CurrentOrderManager) mInteractor).updateDish(ofDish);
        mView.displayNewNumberOfDish(newNumber, fromPosition, ofDish);
    }

    @Override
    public void decreaseNumberOfSameDishes(int newNumber, int fromPosition, FoodDish ofDish) {
        ofDish.setQuantity(newNumber);
        ((CurrentOrderManager) mInteractor).updateDish(ofDish);
        mView.displayNewNumberOfDish(newNumber, fromPosition, ofDish);
    }

    public void removeAll() {
        ((CurrentOrderManager) mInteractor).clearSession();
        mView.setEmptyListState(true);
    }
}

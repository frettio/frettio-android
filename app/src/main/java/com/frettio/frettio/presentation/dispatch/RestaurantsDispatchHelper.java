package com.frettio.frettio.presentation.dispatch;

import com.android.annotations.NonNull;
import com.frettio.frettio.app.common.utils.DispatchHelper;
import com.frettio.frettio.domain.model.Restaurant;
import com.frettio.frettio.presentation.mvp.Repository;

import java.util.List;

import static com.frettio.frettio.app.common.utils.ActivityHelper.checkNotNull;

/**
 * Helps to fetch clients data from a remote server and
 * take action depending on the client session state.
 *
 * Created by luisburgos on 12/9/16.
 */

public class RestaurantsDispatchHelper extends DispatchHelper<List<Restaurant>> {

    protected Repository<Restaurant> mRepository;

    public RestaurantsDispatchHelper(Repository<Restaurant> repository) {
        mRepository = checkNotNull(repository);
    }

    /**
     * Starts the data source fetch operation.
     *
     * @param listener a responder of the request.
     */
    @Override
    public void begin(@NonNull final DispatchHelper.DispatchListener<List<Restaurant>> listener) {
        checkNotNull(listener);
        mRepository.refresh();
        mRepository.getAll(new Repository.ListAllCallback<Restaurant>() {
            @Override
            public void onItemsLoaded(List<Restaurant> elders) {
                listener.onFetchData(elders);
            }

            @Override
            public void onNetworkError(String networkErrorMessage) {
                listener.onError(networkErrorMessage);
            }

            @Override
            public void onServerError(String serverErrorMessage) {
                listener.onError(serverErrorMessage);
            }
        });
    }
}

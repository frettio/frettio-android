package com.frettio.frettio.presentation.mvp;

/**
 * Created by luisburgos on 8/9/16.
 */
public interface BaseView {

    void setProgressIndicator(boolean active);

    void displayFailedMessage(String message);

    void displayLargeFailedMessage(String largeMessage);

}

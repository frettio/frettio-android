package com.frettio.frettio.presentation.orders.currentorder;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.frettio.frettio.R;
import com.frettio.frettio.app.common.utils.PicassoCustomLoader;
import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.presentation.adapter.ItemListener;
import com.frettio.frettio.presentation.restaurants.fooddishes.FilterableDishesAdapter;
import com.frettio.frettio.presentation.restaurants.fooddishes.FoodDishViewHolder;

import java.util.List;

/**
 * Created by luisburgos on 3/5/17.
 */

public class CurrentOrderDishAdapter extends FilterableDishesAdapter {

    public CurrentOrderDishAdapter(
            @NonNull List<FoodDish> dishes,
            @NonNull FilterMethod<FoodDish> filterMethod,
            @NonNull ItemListener<FoodDish> itemListener,
            @NonNull OrderDishActionsListener dishActionsListener
    ) {
        super(dishes, filterMethod, itemListener, dishActionsListener);
    }

    @Override
    public OrderDishViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.component_list_item_order_dish, parent, false);
        return new OrderDishViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FoodDishViewHolder viewHolder, final int position) {
        final FoodDish dish = mFilteredItemList.get(position);

        viewHolder.nameTextView.setText(dish.getName());
        viewHolder.costTextView.setText(
                mContext.getString(R.string.text_holder_price, dish.getCost())
        );

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemListener.onItemClick(dish);
            }
        });

        viewHolder.removeFromOrderButton.setVisibility(View.VISIBLE);
        viewHolder.removeFromOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDishActionsListener.removeFromOrder(
                        viewHolder.addToOrderButton, viewHolder.removeFromOrderButton, position, dish)
                ;
            }
        });

        ((OrderDishViewHolder)viewHolder).setNumberPickerCounter(dish.getQuantity() == 0 ? 1 : dish.getQuantity());

        ((OrderDishViewHolder)viewHolder).setNumberPickerListener(new NumberPickerViewHolder.OnNumberPickerActions() {
            @Override
            public void onUp(int newCounter) {
                ((OrderDishActionsListener) mDishActionsListener).increaseDishQuantity(newCounter, position, dish);
            }

            @Override
            public void onDown(int newCounter) {
                ((OrderDishActionsListener) mDishActionsListener).decreaseDishQuantity(newCounter, position, dish);
            }
        });

        viewHolder.descriptionTextView.setVisibility(View.GONE);
        new PicassoCustomLoader().load(mContext.getApplicationContext(), viewHolder.logoImageView, dish.getImageURL());
    }

    public void removeItem(int position){
        mItemList.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    public interface OrderDishActionsListener extends DishActionsListener {

        void increaseDishQuantity(int newCounter, int position, FoodDish dish);

        void decreaseDishQuantity(int newCounter, int position, FoodDish dish);

    }

}

package com.frettio.frettio.presentation.orders.currentorder;

import android.view.View;

import com.frettio.frettio.presentation.restaurants.fooddishes.FoodDishViewHolder;

/**
 * Created by luisburgos on 3/6/17.
 */

public class OrderDishViewHolder extends FoodDishViewHolder {

    private NumberPickerViewHolder numberPickerViewHolder;

    public OrderDishViewHolder(View itemView) {
        super(itemView);
        numberPickerViewHolder = new NumberPickerViewHolder(itemView);
    }

    public void setNumberPickerCounter(int counter){
        numberPickerViewHolder.setCounter(counter);
    }

    public void setNumberPickerListener(NumberPickerViewHolder.OnNumberPickerActions listener){
        numberPickerViewHolder.setActionsListener(listener);
    }
}

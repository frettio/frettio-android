package com.frettio.frettio.presentation.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.frettio.frettio.R;
import com.frettio.frettio.app.common.AppConstants;
import com.frettio.frettio.app.common.utils.ActivityHelper;
import com.frettio.frettio.presentation.mvp.fragment.BaseFragment;
import com.frettio.frettio.presentation.mvp.lists.Searchable;
import com.frettio.frettio.presentation.orders.ListOrdersFragment;
import com.frettio.frettio.presentation.orders.currentorder.MyOrderActivity;
import com.frettio.frettio.presentation.profile.ProfileActivity;
import com.frettio.frettio.presentation.restaurants.ListRestaurantsFragment;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

public class MainActivity extends BaseMainActivity {

    public static final String MENU_OPTION = "MENU_OPTION";

    private MaterialSearchView searchView;
    private BaseFragment mainContentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupSearchView();
        Bundle extras = getIntent().getExtras();
        if(extras != null && extras.containsKey(MENU_OPTION)){
            MainMenuOption menuOption = (MainMenuOption) extras.getSerializable(MENU_OPTION);
            Log.d(AppConstants.TAG, "Setting menu option " + menuOption);
            onMenuOptionSelected(menuOption);
        } else {
            Log.d(AppConstants.TAG, "Setting menu option is NULL");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_searchable, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }

    @Override
    protected void customizeToolbar() {
        //Do nothing.
    }

    @Override
    protected void setupMainContent() {
        mainContentFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.container);

        if (mainContentFragment == null) {
            mainContentFragment = ListRestaurantsFragment.newInstance();

            ActivityHelper.addFragmentToActivity(
                    getSupportFragmentManager(), mainContentFragment, R.id.container
            );
        }
        getSupportActionBar().setTitle(getString(R.string.menu_option_restaurants));
    }

    @Override
    protected int getMainLayoutResourceID() {
        return R.layout.activity_main;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.application_name;
    }

    @Override
    protected void onMenuOptionSelected(MainMenuOption menuOption) {
        Fragment fragment = null;
        String title = getString(R.string.application_name);
        switch (menuOption){
            case RESTAURANTS:
                title = getString(R.string.menu_option_restaurants);
                fragment = new ListRestaurantsFragment();
                break;
            case MY_ORDER:
                ActivityHelper.getInstance().begin(this, MyOrderActivity.class);
                return;
            case ORDERS:
                title = getString(R.string.menu_option_orders);
                fragment = new ListOrdersFragment();
                break;
            case ACCOUNT:
                ActivityHelper.getInstance().begin(this, ProfileActivity.class);
                return;
            case LOGOUT:
                requestLogoutConfirmation();
                return;
        }
        getSupportActionBar().setTitle(title);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }

    private void setupSearchView() {
        searchView = (MaterialSearchView) findViewById(R.id.searchView);
        searchView.setVisibility(View.VISIBLE);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                ((Searchable) mainContentFragment).search(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ((Searchable) mainContentFragment).search(newText);
                return false;
            }
        });
    }

}

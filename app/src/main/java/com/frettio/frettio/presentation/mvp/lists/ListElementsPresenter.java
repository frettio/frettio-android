package com.frettio.frettio.presentation.mvp.lists;

import android.util.Log;

import com.frettio.frettio.app.common.AppConstants;
import com.frettio.frettio.domain.interactor.Interactor;
import com.frettio.frettio.presentation.mvp.AbstractPresenter;
import com.frettio.frettio.presentation.mvp.Repository;

import java.util.List;

/**
 * Created by luisburgos on 2/17/17.
 */

public class ListElementsPresenter<Model>
        extends AbstractPresenter<ListElementsContract.View<Model>, Repository<Model>>
        implements ListElementsContract.UserActionsListener<Model> {

    public ListElementsPresenter(
            ListElementsContract.View<Model> mView,
            Repository<Model> mInteractor
    ) {
        super(mView, mInteractor);
    }

    @Override
    public void loadElements(boolean forceUpdate) {
        if(forceUpdate){
            mInteractor.refresh();
        }

        mInteractor.getAll(new Repository.ListAllCallback<Model>() {
            @Override
            public void onItemsLoaded(List<Model> models) {
                Log.d(AppConstants.TAG, "Retrieved total of " + models.size());
                mView.setProgressIndicator(false);
                if(models.isEmpty()){
                    Log.d(AppConstants.TAG, "Setting empty state");
                    mView.setEmptyListState(true);
                } else {
                    Log.d(AppConstants.TAG, "Displaying data");
                    mView.setEmptyListState(false);
                    mView.showElements(models);
                }
            }

            @Override
            public void onNetworkError(String networkErrorMessage) {
                ListElementsPresenter.this.onNetworkError(networkErrorMessage);
            }

            @Override
            public void onServerError(String serverErrorMessage) {
                ListElementsPresenter.this.onServerError(serverErrorMessage);
            }
        });
    }

    @Override
    public void openElement(Model element) {
        mView.showElementDetail(element);
    }
}

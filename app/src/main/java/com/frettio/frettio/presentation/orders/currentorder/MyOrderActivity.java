package com.frettio.frettio.presentation.orders.currentorder;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.frettio.frettio.R;
import com.frettio.frettio.app.Injection;
import com.frettio.frettio.app.common.utils.ActivityHelper;
import com.frettio.frettio.data.local.managers.CurrentOrderManager;
import com.frettio.frettio.presentation.mvp.activity.BaseActivityView;

import org.fabiomsr.moneytextview.MoneyTextView;

public class MyOrderActivity
        extends BaseActivityView
        implements MyOrderContract.View, ListCurrentOrderDishesFragment.OnUpdatesListener {

    private ListCurrentOrderDishesFragment listCurrentOrderFragment;
    private Button requestOrderButton;
    private MyOrderContract.UserActionsListener mActionListener;
    private MoneyTextView totalOrderCostTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView(R.layout.activity_my_order);
        setupPresenter();

        listCurrentOrderFragment = (ListCurrentOrderDishesFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        if (listCurrentOrderFragment == null) {
            listCurrentOrderFragment = ListCurrentOrderDishesFragment.newInstance();
            listCurrentOrderFragment.setOnUpdatesListener(this);
            ActivityHelper.addFragmentToActivity(
                    getSupportFragmentManager(), listCurrentOrderFragment, R.id.container
            );
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_deletable, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                if(!listCurrentOrderFragment.isEmpty()){
                    showDialogBeforeDeleteAll();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected ViewGroup getMainLayout() {
        return (CoordinatorLayout) findViewById(R.id.myOrderLayoutContainer);
    }

    @Override
    protected void initViews() {
        requestOrderButton = (Button) findViewById(R.id.requestOrderButton);
        requestOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!listCurrentOrderFragment.isEmpty()){
                    showDialogBeforeRequestOrder();
                }
            }
        });

        totalOrderCostTextView = (MoneyTextView) findViewById(R.id.totalOrderCostTextView);
        setupTotalOrderCost();
    }

    @Override
    protected void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public void setProgressIndicator(boolean active) {
        if(active){
            mProgressDialog.setMessage(getString(R.string.requesting_order));
            mProgressDialog.show();
        } else {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void displayOrderRequestedMessage() {
        displayLargeFailedMessage(getString(R.string.requested_order_message));
        listCurrentOrderFragment.deleteAllDishesFromOrder();
    }

    private void setupPresenter() {
        mActionListener = new MyOrderPresenter(
                this,
                Injection.provideOrderInteractor(Injection.provideTokenManager(this).getSession()),
                Injection.provideUserSessionManager(this)
        );
    }


    private void showDialogBeforeDeleteAll() {
        new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle(getString(R.string.delete_all_from_order_title))
                .setMessage(getString(R.string.delete_all_from_order_message))
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listCurrentOrderFragment.deleteAllDishesFromOrder();
                    }
                }).create().show();
    }

    private void showDialogBeforeRequestOrder() {
        new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle(getString(R.string.request_order_title))
                .setMessage(getString(R.string.request_order_message))
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mActionListener.requestOrder(
                                Injection.provideCurrentOrderInteractor(getApplicationContext()).getSession()
                        );
                    }
                }).create().show();
    }

    @Override
    public void onUpdatedListElements() {
        setupTotalOrderCost();
    }

    private void setupTotalOrderCost() {
        CurrentOrderManager orderManager = Injection.provideCurrentOrderInteractor(this);
        totalOrderCostTextView.setAmount(orderManager.getTotal());
    }

}

package com.frettio.frettio.presentation.profile;

import com.frettio.frettio.presentation.mvp.BaseView;

public interface ProfileContract {

    interface View extends BaseView {

        void displayProfileInformation(
                String firstName,
                String lastName,
                String email,
                String phone,
                String streetNumber,
                String crossStreets,
                String houseBlock,
                String street,
                String city,
                String imageProfileURL
        );

        void displayProfileUpdatedMessage();

        void setEditionMode(boolean editable);

        void setFirstNameErrorMessage();

        void setLastNamesErrorMessage();

        void setEmailErrorMessage();

        void setPhoneErrorMessage();

        void setStreetNumberErrorMessage();

        void setCityErrorMessage();

        void setStreetNameErrorMessage();

        void setCrossStreetsErrorMessage();

        void setHouseBlockErrorMessage();
    }

    interface UserActionsListener {

        void loadProfile(boolean forceUpdate);

        void editProfileInformation(
                String firstName,
                String lastName,
                String email,
                String phone,
                String streetNumber,
                String crossStreets,
                String houseBlock,
                String street,
                String city
        );
    }
}


package com.frettio.frettio.presentation.mvp.lists;

import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.presentation.mvp.BaseView;

import java.util.List;

/**
 * Created by luisburgos on 2/17/17.
 */

public interface ListElementsContract<Model> {

    interface View<Model> extends BaseView {

        void setEmptyListState(boolean isEmpty);

        void showElements(List<Model> elements);

        void showElementDetail(Model elementSnap);

    }

    interface UserActionsListener<Model> {

        void loadElements(boolean forceUpdate);

        void openElement(Model element);

    }
}

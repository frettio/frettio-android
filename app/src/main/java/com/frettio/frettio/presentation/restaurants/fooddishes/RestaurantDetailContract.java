package com.frettio.frettio.presentation.restaurants.fooddishes;

import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.domain.model.NewOrder;
import com.frettio.frettio.domain.model.Order;
import com.frettio.frettio.presentation.mvp.BasePresenter;
import com.frettio.frettio.presentation.mvp.BaseView;

import java.util.List;

/**
 * Created by luisburgos on 3/4/17.
 */

public interface RestaurantDetailContract {

    interface View extends BaseView {

        void displayAddedToOrderMessage(String dishName);

        void displayRemovedFromOrderMessage(String dishName);

        void sendToOrderDishesContent();

        void setCurrentOrder(NewOrder currentOrder);
    }

    interface UserActionsListener extends BasePresenter {

        boolean removeFromOrder(FoodDish dish);

        boolean addToOrder(FoodDish dish);

        void clearOrder();

        void openOrder();
    }

}

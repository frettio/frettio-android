package com.frettio.frettio.presentation.mvp;

import com.frettio.frettio.domain.interactor.Interactor;

import java.util.List;

/**
 * Main entry point for accessing domain data across the application.
 * This is the main interface for accessing the data layer.
 * @param <T>
 */
public interface Repository<T> extends Interactor<T> {

    void add(T item);

    void add(Iterable<T> items);

    void update(T item);

    void remove(T item);

    void getAll(ListAllCallback<T> callback);

    void get(int id, SingleItemInformationCallback<T> callback);

    void deleteAll();

    void refresh();

    public interface FetchCallback {

        void onNetworkError(String networkErrorMessage);

        void onServerError(String serverErrorMessage);

    }

    public interface ListAllCallback<Item> extends FetchCallback {

        void onItemsLoaded(List<Item> items);

    }

    public interface SingleItemInformationCallback<Item> extends FetchCallback {

        void onItemInformationLoaded(Item item);

    }

    public interface CompositeItemInformationCallback<Item, Item2> extends FetchCallback {

        void onItemInformationLoaded(Item item, Item2 item2);

    }

}

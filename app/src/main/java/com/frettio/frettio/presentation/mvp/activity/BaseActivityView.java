package com.frettio.frettio.presentation.mvp.activity;

import android.app.ProgressDialog;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.frettio.frettio.R;
import com.frettio.frettio.presentation.mvp.BaseView;
import com.frettio.frettio.app.common.utils.ActivityHelper;

/**
 *
 * Created by luisburgos on 10/5/16.
 */

public abstract class BaseActivityView extends AppCompatActivity implements BaseView {

    protected ViewGroup mainLayout;
    protected ProgressDialog mProgressDialog;

    @Override
    public void displayFailedMessage(String message) {
        Snackbar.make(mainLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void displayLargeFailedMessage(String largeMessage) {
        Snackbar snackbar = Snackbar.make(
                getMainLayout(), largeMessage, Snackbar.LENGTH_LONG
        );
        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(5);
        snackbar.show();
    }

    public void displayInfoMessage(String message){
        Snackbar.make(mainLayout, message, Snackbar.LENGTH_LONG).show();
    }

    public void showEmptyDataMessage() {
        Snackbar.make(mainLayout, getResources().getString(R.string.error_empty_fields), Snackbar.LENGTH_LONG).show();
    }

    public void showUserNonExistingMessage() {
        Snackbar.make(mainLayout, getResources().getString(R.string.error_non_existing_user), Snackbar.LENGTH_LONG).show();
    }

    protected abstract ViewGroup getMainLayout();

    protected abstract void initViews();

    protected abstract void initToolbar();

    protected void setView(int contentViewID){
        setContentView(contentViewID);
        setupMainLayout();
        initViews();
        initToolbar();
        mProgressDialog = ActivityHelper.createModalProgressDialog(this);
    }

    private void setupMainLayout(){
        mainLayout = this.getMainLayout();
        if (mainLayout == null){
            mainLayout = (ViewGroup) findViewById(android.R.id.content);
        }
    }
}

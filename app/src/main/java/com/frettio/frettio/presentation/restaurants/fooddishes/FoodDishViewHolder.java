package com.frettio.frettio.presentation.restaurants.fooddishes;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.frettio.frettio.R;
import com.frettio.frettio.presentation.orders.currentorder.NumberPickerViewHolder;
import com.frettio.frettio.presentation.restaurants.DescriptiveViewHolder;

/**
 * Created by luisburgos on 3/2/17.
 */

public class FoodDishViewHolder extends DescriptiveViewHolder {

    public TextView costTextView;
    public Button addToOrderButton;
    public Button removeFromOrderButton;

    public FoodDishViewHolder(View itemView) {
        super(itemView);
        costTextView = (TextView) itemView.findViewById(R.id.costTextView);
        addToOrderButton = (Button) itemView.findViewById(R.id.addToOrderButton);
        removeFromOrderButton = (Button) itemView.findViewById(R.id.removeFromOrderButton);
    }

}


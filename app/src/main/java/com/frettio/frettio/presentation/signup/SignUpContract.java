package com.frettio.frettio.presentation.signup;

import com.frettio.frettio.presentation.mvp.BasePresenter;
import com.frettio.frettio.presentation.mvp.BaseView;

/**
 * Created by luisburgos on 2/13/17.
 */

public interface SignUpContract {

    interface View extends BaseView {

        void setEmailErrorMessage();

        void setPasswordErrorMessage();

        void setFirstNameErrorMessage();

        void setLastNamesErrorMessage();

        void setPhoneErrorMessage();

        void setStreetNumberErrorMessage();

        void setCityErrorMessage();

        void setStreetNameErrorMessage();

        void setCrossStreetsErrorMessage();

        void setHouseBlockErrorMessage();

        void sendToLogin();

        void setPasswordConfirmationErrorMessage();

        void clearErrors();
    }

    interface UserActionsListener extends BasePresenter {

        void doSignUp(
                String email,
                String password,
                String passwordConfirmation,
                String firstName,
                String lastNames,
                String phone,
                String streetNumber,
                String city,
                String streetName,
                String crossStreets,
                String houseBlock
        );

        void alreadyHaveAnAccount();
    }
}

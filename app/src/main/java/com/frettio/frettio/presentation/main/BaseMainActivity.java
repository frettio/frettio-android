package com.frettio.frettio.presentation.main;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.frettio.frettio.R;
import com.frettio.frettio.app.Injection;
import com.frettio.frettio.app.common.utils.LogoutUtil;
import com.frettio.frettio.data.local.managers.UserSessionManager;
import com.frettio.frettio.domain.model.User;

/**
 * Created by Roberto on 19/11/16.
 */

public abstract class BaseMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    protected DrawerLayout mDrawer;
    protected NavigationView mNavigationView;
    protected Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getMainLayoutResourceID());
        setupToolbar(getToolbarTitle());
        setupDrawerLayout();
        setupHeaderContent();
        setupMainContent();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            onMenuOptionSelected(MainMenuOption.LOGOUT);
        } else if (id == R.id.nav_restaurants) {
            onMenuOptionSelected(MainMenuOption.RESTAURANTS);
        } else if (id == R.id.nav_my_order) {
            onMenuOptionSelected(MainMenuOption.MY_ORDER);
        } else if (id == R.id.nav_orders) {
            onMenuOptionSelected(MainMenuOption.ORDERS);
        } else if (id == R.id.nav_profile) {
            onMenuOptionSelected(MainMenuOption.ACCOUNT);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setupToolbar(int titleID) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.customizeToolbar();
        setSupportActionBar(toolbar);
        setTitle(getString(titleID));
    }

    protected abstract void customizeToolbar();

    private void setupDrawerLayout() {
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                mDrawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        ){
            @Override
            public void onDrawerStateChanged(int newState) {
                if (newState == DrawerLayout.STATE_SETTLING) {
                    if (!mDrawer.isDrawerOpen(GravityCompat.START)) {
                        setupHeaderContent();
                    }
                }
            }
        };
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    /**
     * This method depends on {@link BaseMainActivity#setupNavigationView()}
     */
    private void setupHeaderContent() {

        this.setupNavigationView();
        View header = mNavigationView.getHeaderView(0);
        UserInformationViewHolder holder = new UserInformationViewHolder(header, this);

        UserSessionManager sessionManager = Injection.provideUserSessionManager(this);
        User user = sessionManager.getSession();

        holder.nameTextView.setText(user.getEmail());
        //holder.loadImageProfile(sessionManager.getImageProfileURL());
    }

    private void setupNavigationView() {
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    protected abstract void setupMainContent();

    protected abstract int getMainLayoutResourceID();

    protected abstract int getToolbarTitle();

    protected abstract void onMenuOptionSelected(MainMenuOption menuOption);

    protected void requestLogoutConfirmation() {
        LogoutUtil.requestLogoutConfirmation(this);
    }

}

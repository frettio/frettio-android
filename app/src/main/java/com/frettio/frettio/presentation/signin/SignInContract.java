package com.frettio.frettio.presentation.signin;

import com.frettio.frettio.presentation.mvp.BasePresenter;
import com.frettio.frettio.presentation.mvp.BaseView;


/**
 * Created by luisburgos on 8/9/16.
 */
public interface SignInContract {

    interface View extends BaseView {

        void setEmailErrorMessage();

        void setPasswordErrorMessage();

        void sendToMain();

        void sendToPasswordRecovery();

        void displaySignInLimitNumberOfTryoutsMessage(int currentIntent);

        void displayMaxNumberOfIntentsReachedMessage();
    }

    interface UserActionsListener extends BasePresenter {

        void doSignIn(String email, String password);

        void recoverPassword();

    }

}
package com.frettio.frettio.presentation.orders.currentorder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.frettio.frettio.R;

import static com.frettio.frettio.app.common.utils.ActivityHelper.checkNotNull;

/**
 * Created by luisburgos on 3/6/17.
 */

public class NumberPickerViewHolder {

    public ImageButton numberPickerDownButton;
    public ImageButton numberPickerUpButton;
    public TextView numberPickerTextView;

    private OnNumberPickerActions mActionsListener;
    private int counter = 0;
    private int minimumLimit = 0;

    public NumberPickerViewHolder(View root) {
        checkNotNull(root);
        numberPickerTextView = (TextView) root.findViewById(R.id.numberPickerTextView);
        numberPickerDownButton = (ImageButton) root.findViewById(R.id.numberPickerDownButton);
        numberPickerUpButton = (ImageButton) root.findViewById(R.id.numberPickerUpButton);

        numberPickerUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                up();
            }
        });

        numberPickerDownButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                down();
            }
        });
        setCounter(counter);
    }

    private void up(){
        setCounter(++counter);
        if(mActionsListener != null){
            mActionsListener.onUp(counter);
        }
    }

    private void down(){
        if(counter > minimumLimit){
            setCounter(--counter);
            if(mActionsListener != null){
                mActionsListener.onDown(counter);
            }
        }
    }

    public void setCounter(int newCounter){
        counter = newCounter;
        numberPickerTextView.setText(String.valueOf(newCounter));
    }

    public void setActionsListener(@NonNull OnNumberPickerActions actionsListener){
        mActionsListener = checkNotNull(actionsListener);
    }

    public interface OnNumberPickerActions {
        void onUp(int newCounter);
        void onDown(int newCounter);
    }
}

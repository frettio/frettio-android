package com.frettio.frettio.presentation.restaurants;

import android.view.View;
import android.widget.TextView;

import com.frettio.frettio.R;

/**
 * Created by luisburgos on 2/23/17.
 */

public class RestaurantViewHolder extends DescriptiveViewHolder {

    TextView branchOfficeTextView;

    public RestaurantViewHolder(View itemView) {
        super(itemView);
        branchOfficeTextView = (TextView) itemView.findViewById(R.id.branchOfficeTextView);
    }

}

package com.frettio.frettio.presentation.signup;

import android.text.TextUtils;

import com.frettio.frettio.domain.interactor.AbstractSignUpInteractor;
import com.frettio.frettio.domain.model.Consumer;
import com.frettio.frettio.domain.model.HomeAddress;
import com.frettio.frettio.domain.model.User;
import com.frettio.frettio.presentation.mvp.AbstractPresenter;
import com.frettio.frettio.presentation.mvp.Repository;

/**
 * Created by luisburgos on 2/13/17.
 */

public class SignUpPresenter extends AbstractPresenter<SignUpContract.View, AbstractSignUpInteractor> implements SignUpContract.UserActionsListener {

    public SignUpPresenter(
            SignUpContract.View mView,
            AbstractSignUpInteractor mInteractor
    ) {
        super(mView, mInteractor);
    }

    @Override
    public void doSignUp(
            String email,
            String password,
            String passwordConfirmation,
            String firstName,
            String lastNames,
            String phone,
            String streetNumber,
            String city,
            String streetName,
            String crossStreets,
            String houseBlock
    ) {
        mView.clearErrors();
        if(isValidInformation(email, password, passwordConfirmation, firstName, lastNames, phone, streetNumber, city, streetName, crossStreets, houseBlock)){
            mView.setProgressIndicator(true);
            final Consumer consumer = new Consumer(
                    email, password, firstName, lastNames, phone,
                    streetNumber, city, streetName, crossStreets, houseBlock
            );

            mInteractor.signUp(consumer, new Repository.SingleItemInformationCallback<User>() {
                @Override
                public void onItemInformationLoaded(User user) {
                    System.out.println(user.getEmail());
                    mView.setProgressIndicator(false);
                    mView.sendToLogin();
                }

                @Override
                public void onNetworkError(String networkErrorMessage) {
                    mView.setProgressIndicator(false);
                    mView.displayFailedMessage(networkErrorMessage);
                }

                @Override
                public void onServerError(String serverErrorMessage) {
                    mView.setProgressIndicator(false);
                    mView.displayFailedMessage(serverErrorMessage);
                }
            });
        }
    }

    @Override
    public void alreadyHaveAnAccount() {
        mView.sendToLogin();
    }

    /**
     * Validates user signIn information.
     * @return boolean value whether provided information is valid/invalid.
     */
    private boolean isValidInformation(
            String email,
            String password,
            String passwordConfirmation,
            String firstName,
            String lastNames,
            String phone,
            String streetNumber,
            String city,
            String streetName,
            String crossStreets,
            String houseBlock
    ) {
        boolean isValid = true;

        if(TextUtils.isEmpty(email)){
            mView.setEmailErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(password)){
            mView.setPasswordErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(passwordConfirmation) || !passwordConfirmation.equals(password)){
            mView.setPasswordConfirmationErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(firstName)){
            mView.setFirstNameErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(lastNames)){
            mView.setLastNamesErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(phone) || !TextUtils.isDigitsOnly(phone)){
            mView.setPhoneErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(streetName)){
            mView.setStreetNameErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(streetNumber)){
            mView.setStreetNumberErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(city)){
            mView.setCityErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(crossStreets)){
            mView.setCrossStreetsErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(houseBlock)){
            mView.setHouseBlockErrorMessage();
            isValid = false;
        }

        return isValid;
    }
}

package com.frettio.frettio.presentation.restaurants.fooddishes;

import com.frettio.frettio.app.common.AppConstants;
import com.frettio.frettio.data.local.managers.CurrentOrderManager;
import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.domain.model.Order;
import com.frettio.frettio.domain.model.Restaurant;
import com.frettio.frettio.presentation.mvp.AbstractPresenter;

import java.util.List;

/**
 * Created by luisburgos on 3/4/17.
 */

public class RestaurantDetailPresenter
        extends AbstractPresenter<RestaurantDetailContract.View, CurrentOrderManager>
        implements RestaurantDetailContract.UserActionsListener {

    public RestaurantDetailPresenter(
            RestaurantDetailContract.View mView,
            CurrentOrderManager orderManager,
            Restaurant restaurant
    ) {
        super(mView, orderManager);
        mInteractor.setRestaurantName(restaurant.getName());
        mInteractor.setRestaurantEmail(restaurant.getEmail());
        mView.setCurrentOrder(mInteractor.getSession());
    }

    @Override
    public boolean removeFromOrder(FoodDish dish) {
        mInteractor.removeFromOrder(dish);
        mView.displayRemovedFromOrderMessage(dish.getName());
        return true;
    }

    @Override
    public boolean addToOrder(FoodDish dish) {
        boolean operationPossible = mInteractor.isFromSameCurrentRestaurant(dish);
        if(operationPossible){
            mInteractor.addToOrder(dish);
            mView.displayAddedToOrderMessage(dish.getName());
        } else {
            mView.displayLargeFailedMessage(AppConstants.Messages.NOT_SAME_RESTAURANT);
        }
        return operationPossible;
    }

    @Override
    public void clearOrder() {
        mInteractor.clearSession();
    }

    @Override
    public void openOrder() {
        mView.sendToOrderDishesContent();
    }
}

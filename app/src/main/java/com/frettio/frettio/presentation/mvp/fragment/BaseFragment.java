package com.frettio.frettio.presentation.mvp.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.frettio.frettio.R;
import com.frettio.frettio.app.common.utils.ActivityHelper;
import com.frettio.frettio.presentation.restaurants.fooddishes.FilterableDishesAdapter;
import com.frettio.frettio.presentation.restaurants.fooddishes.RestaurantDetailActivity;

import static com.frettio.frettio.app.common.utils.ActivityHelper.checkNotNull;


/**
 * Created by luisburgos on 10/27/16.
 */
public abstract class BaseFragment extends Fragment {

    protected ProgressDialog mProgressDialog;
    protected Snackbar mSnackbar;
    protected SnackbarAction mSnackbarAction;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setupPresenter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(getMainLayoutResource(), container, false);
        this.bindViews(root);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        mProgressDialog = ActivityHelper.createModalProgressDialog(getActivity());
        this.setupDataOnLinkedViews();
    }

    protected abstract void setupPresenter();

    protected abstract void setupDataOnLinkedViews();

    protected abstract void bindViews(View root);

    protected abstract int getMainLayoutResource();

    protected void setSnackbarMessage(String message){
        if(mSnackbar == null){
            setupSnackbar(message, getString(R.string.understood));
        }
        mSnackbar.setText(message);
    }

    protected void setSnackbarAction(@NonNull SnackbarAction action){
        mSnackbarAction = checkNotNull(action);
    }

    private void setupSnackbar(String actionTitle, String message){
        if(getView() == null){
            return;
        }

        mSnackbar = Snackbar.make(getView(), message, Snackbar.LENGTH_INDEFINITE);
        TextView textView = (TextView) mSnackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(5);

        mSnackbar.setAction(actionTitle, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mSnackbarAction != null){
                    mSnackbarAction.execute();
                } else {
                    mSnackbar.dismiss();
                }
            }
        });
    }

    public interface SnackbarAction {
        void execute();
    }
}

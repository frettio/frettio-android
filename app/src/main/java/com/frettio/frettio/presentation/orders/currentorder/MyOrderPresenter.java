package com.frettio.frettio.presentation.orders.currentorder;

import com.frettio.frettio.data.local.managers.UserSessionManager;
import com.frettio.frettio.domain.interactor.AbstractOrderInteractor;
import com.frettio.frettio.domain.model.NewOrder;
import com.frettio.frettio.domain.model.Order;
import com.frettio.frettio.presentation.mvp.AbstractPresenter;
import com.frettio.frettio.presentation.mvp.Repository;

import static com.frettio.frettio.app.common.utils.ActivityHelper.checkNotNull;

/**
 * Created by luisburgos on 3/6/17.
 */

public class MyOrderPresenter
        extends AbstractPresenter<MyOrderContract.View, AbstractOrderInteractor>
        implements MyOrderContract.UserActionsListener {

    private UserSessionManager mSessionManager;

    public MyOrderPresenter(
            MyOrderContract.View mView,
            AbstractOrderInteractor mInteractor,
            UserSessionManager sessionManager
    ) {
        super(mView, mInteractor);
        mSessionManager = checkNotNull(sessionManager);
    }

    @Override
    public void requestOrder(NewOrder order) {
        mView.setProgressIndicator(true);
        order.setConsumerEmail(mSessionManager.getSession().getEmail());
        mInteractor.createOrder(order, new Repository.SingleItemInformationCallback<NewOrder>() {
            @Override
            public void onItemInformationLoaded(NewOrder order) {
                mView.setProgressIndicator(false);
                mView.displayOrderRequestedMessage();
            }

            @Override
            public void onNetworkError(String networkErrorMessage) {
                notifyFailure(networkErrorMessage);
            }

            @Override
            public void onServerError(String serverErrorMessage) {
                notifyFailure(serverErrorMessage);
            }
        });
    }
}

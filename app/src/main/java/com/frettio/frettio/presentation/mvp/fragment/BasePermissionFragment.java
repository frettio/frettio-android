package com.frettio.frettio.presentation.mvp.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.frettio.frettio.R;
import com.frettio.frettio.app.common.utils.PermissionHelper;
import com.frettio.frettio.presentation.mvp.activity.BasePermissionActivityView;

import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;


/**
 * Created by luisburgos on 11/26/16.
 */

public abstract class BasePermissionFragment extends BaseFragment
        implements PermissionHelper.ApplicationPermissionsCallbacks {

    protected PermissionHelper mPermissionHelper;
    private BasePermissionActivityView.AfterAskedPermissionsAction mAction;

    protected final int SETTINGS_SCREEN = 777;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPermissionHelper = new PermissionHelper(getActivity(), this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionHelper.isRequest=false;
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, getActivity());

    }

    @Override
    public void onNoNeedToAskForAlreadyGrantedPermissions(){

        if(mAction != null){
            mAction.execute(true);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> permissions) {

        if(mAction != null){
            mAction.execute(true);
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> permissions) {

        if(getOnNeedToCheckForPermanentlyDeniedPermissions()){

            if (EasyPermissions.somePermissionPermanentlyDenied(this, permissions)) {
                new AppSettingsDialog.Builder(this, getOnAskToOpenSettingsMessage())
                        .setTitle(getString(R.string.title_settings_dialog))
                        .setPositiveButton(getString(R.string.settings))
                        .setNegativeButton(getString(R.string.cancel), null)
                        .setRequestCode(SETTINGS_SCREEN)
                        .build()
                        .show();
            }

        } else {
            if(mAction != null){
                mAction.execute(false);
            }
        }
    }

    protected abstract boolean getOnNeedToCheckForPermanentlyDeniedPermissions();

    protected abstract String getOnAskToOpenSettingsMessage();

    protected void requestAccessToFineLocation(BasePermissionActivityView.AfterAskedPermissionsAction action) {
        mAction = action;
        mPermissionHelper.checkAccessFineLocationPermission();
    }

}

package com.frettio.frettio.presentation.orders.currentorder;

import com.frettio.frettio.domain.model.NewOrder;
import com.frettio.frettio.presentation.mvp.BasePresenter;
import com.frettio.frettio.presentation.mvp.BaseView;

/**
 * Created by luisburgos on 3/6/17.
 */

public interface MyOrderContract {

    interface View extends BaseView {

        void displayOrderRequestedMessage();
    }

    interface UserActionsListener extends BasePresenter {

        void requestOrder(NewOrder order);

    }

}

package com.frettio.frettio.presentation.mvp.activity;

import android.support.annotation.NonNull;

import com.frettio.frettio.R;
import com.frettio.frettio.app.common.utils.PermissionHelper;

import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;


public abstract class BasePermissionActivityView extends BaseActivityView
        implements PermissionHelper.ApplicationPermissionsCallbacks {

    protected PermissionHelper mPermissionHelper = new PermissionHelper(this);
    private AfterAskedPermissionsAction mAction;

    protected final int SETTINGS_SCREEN = 777;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onNoNeedToAskForAlreadyGrantedPermissions(){
        if(mAction != null){
            mAction.execute(true);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> permissions) {
        if(mAction != null){
            mAction.execute(true);
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> permissions) {
        if(getOnNeedToCheckForPermanentlyDeniedPermissions()){
            if (EasyPermissions.somePermissionPermanentlyDenied(this, permissions)) {
                new AppSettingsDialog.Builder(this, getOnAskToOpenSettingsMessage())
                        .setTitle(getString(R.string.title_settings_dialog))
                        .setPositiveButton(getString(R.string.settings))
                        .setNegativeButton(getString(R.string.cancel), null)
                        .setRequestCode(SETTINGS_SCREEN)
                        //.setTheme(R.style.CustomAppCompatAlertDialogStyle)
                        .build()
                        .show();
            }

        } else {
            if(mAction != null){
                mAction.execute(false);
            }
        }
    }

    protected abstract boolean getOnNeedToCheckForPermanentlyDeniedPermissions();

    protected abstract String getOnAskToOpenSettingsMessage();

    protected void requestAccessToGalleryPermissions(AfterAskedPermissionsAction action) {
        mAction = action;
        mPermissionHelper.checkReadExternalStorage();
    }

    protected void requestAllPermissions(AfterAskedPermissionsAction action) {
        mAction = action;
        mPermissionHelper.checkAllPermissions();
    }

    public interface AfterAskedPermissionsAction {
        void execute(boolean result);
    }
}

package com.frettio.frettio.presentation.restaurants.fooddishes;

import android.os.Bundle;
import android.util.Log;

import com.frettio.frettio.app.Injection;
import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.presentation.adapter.FilterableListAdapter;
import com.frettio.frettio.presentation.mvp.lists.ListElementsFragment;
import com.frettio.frettio.presentation.mvp.lists.ListElementsPresenter;
import com.frettio.frettio.presentation.mvp.lists.Searchable;
import com.frettio.frettio.presentation.restaurants.FilterableRestaurantsAdapter;

import java.util.ArrayList;
import java.util.List;

import static com.frettio.frettio.app.common.AppConstants.TAG;

/**
 * Created by luisburgos on 2/25/17.
 */

public class ListRestaurantFoodDishesFragment
        extends ListElementsFragment<FoodDish, FoodDishViewHolder>
        implements Searchable {

    public static final String EXTRA_RESTAURANT_ID = "EXTRA_RESTAURANT_ID";
    private String mCurrentRestaurantIdentifier;

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        mCurrentRestaurantIdentifier = args.getString(EXTRA_RESTAURANT_ID);
        Log.d(TAG, "Setting argument " + mCurrentRestaurantIdentifier);
    }

    @Override
    protected void setupAdapter() {
        mAdapter = new FilterableDishesAdapter(
                new ArrayList<FoodDish>(0),
                mFilterMethod,
                mItemListener,
                (FilterableDishesAdapter.DishActionsListener) getActivity()
        );
    }

    @Override
    protected void setupPresenter() {
        mActionsListener = new ListElementsPresenter<>(
                this,
                Injection.provideFoodDishRepository(getContext(), mCurrentRestaurantIdentifier)
        );
    }

    @Override
    public void showElements(List<FoodDish> elements) {
        List<FoodDish> currentDishes = Injection.provideCurrentOrderInteractor(getContext()).getListOfDishes();
        for(FoodDish dish : elements){
            if(isAddedToCurrentOrder(currentDishes, dish.getId())){
                dish.markAsAddedToOrder(true);
            }
        }

        super.showElements(elements);
    }

    @Override
    protected String getEmptyListMessage() {
        return null;
    }

    @Override
    protected void setupDataOnLinkedViews() {
        //IMPORTANT: Not necessary.
    }

    @Override
    public void showElementDetail(FoodDish elementSnap) {
        //IMPORTANT: Not necessary.
    }

    @Override
    public void displayLargeFailedMessage(String largeMessage) {
        setSnackbarMessage(largeMessage);
        mSnackbar.show();
    }

    @Override
    public void search(final String criteria) {
        ((FilterableListAdapter )mAdapter).getFilter().filter(criteria);
    }

    /**
     * IMPORTANT: This method defines the filter criteria used by
     * the filterable adapter {@link FilterableRestaurantsAdapter}
     */
    protected final FilterableListAdapter.FilterMethod<FoodDish> mFilterMethod = new FilterableRestaurantsAdapter.FilterMethod<FoodDish>() {
        @Override
        public boolean evaluate(FoodDish filterableItem, String criteria) {
            return filterableItem.getName().toLowerCase().contains(criteria.toLowerCase())
                    || filterableItem.getDescription().toLowerCase().contains(criteria.toLowerCase());
        }
    };

    public static ListRestaurantFoodDishesFragment newInstance(String restaurantId) {
        Bundle arguments = new Bundle();
        arguments.putString(EXTRA_RESTAURANT_ID, restaurantId);
        ListRestaurantFoodDishesFragment fragment = new ListRestaurantFoodDishesFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    private boolean isAddedToCurrentOrder(List<FoodDish> foodDishes, long dishId) {
        boolean isAdded = false;
        if(foodDishes != null){
            for(FoodDish dish : foodDishes){
                if(dish.getId() == dishId){
                    isAdded = true;
                    break;
                }
            }
        }
        return isAdded;
    }

}

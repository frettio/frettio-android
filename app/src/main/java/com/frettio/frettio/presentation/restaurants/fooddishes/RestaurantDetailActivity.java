package com.frettio.frettio.presentation.restaurants.fooddishes;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.andremion.counterfab.CounterFab;
import com.frettio.frettio.R;
import com.frettio.frettio.app.Injection;
import com.frettio.frettio.app.common.AppConstants;
import com.frettio.frettio.app.common.utils.ActivityHelper;
import com.frettio.frettio.app.common.utils.PicassoCustomLoader;
import com.frettio.frettio.data.repositories.FoodDishesRepository;
import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.domain.model.NewOrder;
import com.frettio.frettio.domain.model.Order;
import com.frettio.frettio.domain.model.Restaurant;
import com.frettio.frettio.presentation.main.MainActivity;
import com.frettio.frettio.presentation.main.MainMenuOption;
import com.frettio.frettio.presentation.mvp.activity.BaseActivityView;
import com.google.gson.Gson;

import java.util.List;

public class RestaurantDetailActivity
        extends BaseActivityView
        implements FilterableDishesAdapter.DishActionsListener, RestaurantDetailContract.View {

    public static final String EXTRA_RESTAURANT = "EXTRA_RESTAURANT";

    private ListRestaurantFoodDishesFragment mainContentFragment;
    private ProgressBar progress;
    private ImageView profileImageView;
    private TextView restaurantNameTextView;
    private CounterFab orderCounterFab;

    private Restaurant mRestaurant;
    private RestaurantDetailContract.UserActionsListener mActionsListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView(R.layout.activity_restaurant_detail);
        String restaurantJSON = getIntent().getExtras().getString(EXTRA_RESTAURANT);
        mRestaurant = new Gson().fromJson(restaurantJSON, Restaurant.class);

        mainContentFragment = (ListRestaurantFoodDishesFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        if (mainContentFragment == null) {
            mainContentFragment = ListRestaurantFoodDishesFragment.newInstance(mRestaurant.getEmail());

            ActivityHelper.addFragmentToActivity(
                    getSupportFragmentManager(), mainContentFragment, R.id.container
            );
        }

        mActionsListener = new RestaurantDetailPresenter(
                this, Injection.provideCurrentOrderInteractor(getApplicationContext()), mRestaurant
        );

        setupImage();
    }

    @Override
    protected void onDestroy() {
        FoodDishesRepository.destroyInstance();
        super.onDestroy();
    }

    private void setupImage() {
        restaurantNameTextView.setText(mRestaurant.getDescription());
        getSupportActionBar().setTitle(mRestaurant.getName());
        if(mRestaurant.getLogo() != null){
            Log.d(AppConstants.TAG, mRestaurant.getLogo());
            new PicassoCustomLoader().load(this, profileImageView, mRestaurant.getLogo());
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                showDialogBeforeSave();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void showDialogBeforeSave() {
        new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle(getString(R.string.restaurant_detail_back_title))
                .setMessage(getString(R.string.restaurant_detail_back_message))
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        setResult(Activity.RESULT_CANCELED);
                        RestaurantDetailActivity.super.onBackPressed();
                        mActionsListener.clearOrder();
                    }
                }).create().show();
    }

    @Override
    protected ViewGroup getMainLayout() {
        return (CoordinatorLayout) findViewById(R.id.information_coordinator_layout);
    }

    @Override
    protected void initViews() {
        profileImageView = (ImageView) findViewById(R.id.profileImageView);
        restaurantNameTextView = (TextView) findViewById(R.id.restaurantNameTextView);
        progress = (ProgressBar) findViewById(R.id.progress);
        orderCounterFab = (CounterFab) findViewById(R.id.orderCounterFab);

        orderCounterFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActionsListener.openOrder();
            }
        });
    }

    @Override
    protected void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public void setProgressIndicator(boolean active) {
        if(active) {
            progress.setVisibility(View.VISIBLE);
        } else {
            progress.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void addToOrder(Button addButton, Button removeButton, int position, FoodDish dish) {
        if(mActionsListener.addToOrder(dish)){
            addButton.setVisibility(View.GONE);
            removeButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void removeFromOrder(Button addButton, Button removeButton, int position, FoodDish dish) {
        if(mActionsListener.removeFromOrder(dish)){
            removeButton.setVisibility(View.GONE);
            addButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void displayAddedToOrderMessage(String dishName) {
        if(orderCounterFab.getCount() == 0){
            orderCounterFab.setVisibility(View.VISIBLE);
        }
        orderCounterFab.increase();

        displayInfoMessage(getString(R.string.text_message_added_to_order, dishName));
    }

    @Override
    public void displayRemovedFromOrderMessage(String dishName) {
        orderCounterFab.decrease();
        if(orderCounterFab.getCount() == 0){
            orderCounterFab.setVisibility(View.GONE);
        }

        displayInfoMessage(getString(R.string.text_message_removed_to_order, dishName));
    }

    @Override
    public void sendToOrderDishesContent() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(MainActivity.MENU_OPTION, MainMenuOption.MY_ORDER);
        ActivityHelper.getInstance().sendAndFinishWithExtras(this, MainActivity.class, bundle);
    }

    @Override
    public void setCurrentOrder(NewOrder currentOrder) {
        orderCounterFab.setCount(currentOrder.getDishes().size());

        if(!currentOrder.getDishes().isEmpty()){
            orderCounterFab.setVisibility(View.VISIBLE);
        }
    }

}

package com.frettio.frettio.presentation.orders.currentorder;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.frettio.frettio.R;
import com.frettio.frettio.app.Injection;
import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.domain.model.Order;
import com.frettio.frettio.presentation.adapter.FilterableListAdapter;
import com.frettio.frettio.presentation.mvp.lists.Deletable;
import com.frettio.frettio.presentation.mvp.lists.ListElementsFragment;
import com.frettio.frettio.presentation.mvp.lists.Searchable;
import com.frettio.frettio.presentation.restaurants.FilterableRestaurantsAdapter;
import com.frettio.frettio.presentation.restaurants.fooddishes.FilterableDishesAdapter;
import com.frettio.frettio.presentation.restaurants.fooddishes.FoodDishViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luisburgos on 3/5/17.
 */

public class ListCurrentOrderDishesFragment
        extends ListElementsFragment<FoodDish, FoodDishViewHolder>
        implements Searchable, Deletable, CurrentOrderDishAdapter.OrderDishActionsListener,
        ListCurrentOrderContract.View {

    private OnUpdatesListener onUpdatesListener;

    @Override
    protected void setupAdapter() {
        mAdapter = new CurrentOrderDishAdapter(
                new ArrayList<FoodDish>(0), mFilterMethod, mItemListener, this
        );
    }

    @Override
    protected void setupPresenter() {
        mActionsListener = new ListCurrentOrderDishesPresenter(
                this,
                Injection.provideCurrentOrderInteractor(getActivity().getApplicationContext())
        );
    }

    @Override
    public void showElements(List<FoodDish> elements) {
        super.showElements(elements);
        notifyUpdatesListener();
    }

    @Override
    public void increaseDishQuantity(int newCounter, int position, FoodDish dish) {
        ((ListCurrentOrderDishesPresenter)mActionsListener).increaseNumberOfSameDishes(newCounter, position, dish);
    }

    @Override
    public void decreaseDishQuantity(int newCounter, int position, FoodDish dish) {
        ((ListCurrentOrderDishesPresenter)mActionsListener).increaseNumberOfSameDishes(newCounter, position, dish);
    }

    @Override
    protected String getEmptyListMessage() {
        return null;
    }

    @Override
    protected void setupDataOnLinkedViews() {
        //IMPORTANT: Not necessary.
    }

    @Override
    public void showElementDetail(FoodDish elementSnap) {
        //IMPORTANT: Not necessary.
    }

    @Override
    public void displayLargeFailedMessage(String largeMessage) {
        setSnackbarMessage(largeMessage);
        mSnackbar.show();
    }

    @Override
    public void search(final String criteria) {
        ((FilterableListAdapter) mAdapter).getFilter().filter(criteria);
    }

    /**
     * IMPORTANT: This method defines the filter criteria used by
     * the filterable adapter {@link FilterableRestaurantsAdapter}
     */
    protected final FilterableListAdapter.FilterMethod<FoodDish> mFilterMethod = new FilterableRestaurantsAdapter.FilterMethod<FoodDish>() {
        @Override
        public boolean evaluate(FoodDish filterableItem, String criteria) {
            return filterableItem.getName().toLowerCase().contains(criteria.toLowerCase())
                    || filterableItem.getDescription().toLowerCase().contains(criteria.toLowerCase());
        }
    };

    public static ListCurrentOrderDishesFragment newInstance() {
        Bundle arguments = new Bundle();
        ListCurrentOrderDishesFragment fragment = new ListCurrentOrderDishesFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void addToOrder(Button addButton, Button removeButton, int position, FoodDish dish) {
        //IMPORTANT: DO NOTHING!!
    }

    @Override
    public void removeFromOrder(Button addButton, Button removeButton, int position, FoodDish dish) {
        ((ListCurrentOrderDishesPresenter) mActionsListener).removeFromOrder(position, dish);
        notifyUpdatesListener();
    }

    @Override
    public void displayRemovedFromOrderMessage(int dishPosition) {
        ((CurrentOrderDishAdapter) mAdapter).removeItem(dishPosition);
        notifyUpdatesListener();
    }

    @Override
    public void displayNewNumberOfDish(int newNumber, int fromPosition, FoodDish dish) {
        dish.setQuantity(newNumber);
        mAdapter.notifyItemChanged(fromPosition, dish);
        notifyUpdatesListener();
    }

    public boolean isEmpty(){
        return mAdapter.getItemCount() == 0;
    }

    public void deleteAllDishesFromOrder() {
        ((ListCurrentOrderDishesPresenter) mActionsListener).removeAll();
        mAdapter.replaceData(new ArrayList<FoodDish>(0));
        notifyUpdatesListener();
    }

    public void setOnUpdatesListener(OnUpdatesListener onUpdatesListener) {
        this.onUpdatesListener = onUpdatesListener;
    }

    private void notifyUpdatesListener(){
        if(onUpdatesListener != null){
            onUpdatesListener.onUpdatedListElements();
        }
    }

    public interface OnUpdatesListener {
        void onUpdatedListElements();
    }
}


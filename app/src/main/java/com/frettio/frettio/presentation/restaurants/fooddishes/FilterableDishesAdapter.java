package com.frettio.frettio.presentation.restaurants.fooddishes;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.frettio.frettio.R;
import com.frettio.frettio.app.common.utils.PicassoCustomLoader;
import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.presentation.adapter.FilterableListAdapter;
import com.frettio.frettio.presentation.adapter.ItemListener;

import java.util.List;

import static com.frettio.frettio.app.common.utils.ActivityHelper.checkNotNull;

/**
 * Created by luisburgos on 3/2/17.
 */

public class FilterableDishesAdapter extends FilterableListAdapter<FoodDish, FoodDishViewHolder> {

    protected DishActionsListener mDishActionsListener;

    /**
     * Class constructor.
     *
     * @param dishes  base list of items.
     * @param filterMethod filter criteria method.
     * @param itemListener listener that responds on item interaction.
     */
    public FilterableDishesAdapter(
            @NonNull List<FoodDish> dishes,
            @NonNull FilterMethod<FoodDish> filterMethod,
            @NonNull ItemListener<FoodDish> itemListener,
            @NonNull DishActionsListener dishActionsListener
    ) {
        super(dishes, filterMethod, itemListener);
        setDishActionsListener(dishActionsListener);
    }

    @Override
    public FoodDishViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.component_list_item_food_dish, parent, false);
        return new FoodDishViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FoodDishViewHolder viewHolder, final int position) {
        final FoodDish dish = mItemList.get(position);

        viewHolder.nameTextView.setText(dish.getName());
        viewHolder.costTextView.setText(
                mContext.getString(R.string.text_holder_price, dish.getCost())
        );
        viewHolder.descriptionTextView.setText(dish.getDescription());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemListener.onItemClick(dish);
            }
        });

        viewHolder.addToOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDishActionsListener.addToOrder(
                        viewHolder.addToOrderButton, viewHolder.removeFromOrderButton, position, dish)
                ;
            }
        });

        viewHolder.removeFromOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDishActionsListener.removeFromOrder(
                        viewHolder.addToOrderButton, viewHolder.removeFromOrderButton, position, dish)
                ;
            }
        });

        if(dish.isAlreadyInCurrentOrder()){
            viewHolder.addToOrderButton.setVisibility(View.GONE);
            viewHolder.removeFromOrderButton.setVisibility(View.VISIBLE);
        } else {
            viewHolder.removeFromOrderButton.setVisibility(View.GONE);
            viewHolder.addToOrderButton.setVisibility(View.VISIBLE);
        }

        new PicassoCustomLoader().load(mContext.getApplicationContext(), viewHolder.logoImageView, dish.getImageURL());
    }

    public void setDishActionsListener(DishActionsListener dishActionsListener) {
        mDishActionsListener = checkNotNull(dishActionsListener);
    }

    public interface DishActionsListener {

        void addToOrder(Button addButton, Button removeButton, int position, FoodDish dish);

        void removeFromOrder(Button addButton, Button removeButton, int position, FoodDish dish);

    }
}


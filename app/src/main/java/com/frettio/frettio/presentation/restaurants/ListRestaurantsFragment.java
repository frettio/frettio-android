package com.frettio.frettio.presentation.restaurants;

import android.os.Bundle;

import com.frettio.frettio.app.Injection;
import com.frettio.frettio.app.common.utils.ActivityHelper;
import com.frettio.frettio.domain.model.Restaurant;
import com.frettio.frettio.presentation.mvp.lists.ListElementsFragment;
import com.frettio.frettio.presentation.mvp.lists.ListElementsPresenter;
import com.frettio.frettio.presentation.mvp.lists.Searchable;
import com.frettio.frettio.presentation.restaurants.fooddishes.RestaurantDetailActivity;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by luisburgos on 2/23/17.
 */

public class ListRestaurantsFragment extends ListElementsFragment<Restaurant, RestaurantViewHolder> implements Searchable {

    @Override
    protected void setupAdapter() {
        mAdapter = new FilterableRestaurantsAdapter(
                new ArrayList<Restaurant>(0),
                mFilterMethod,
                mItemListener
        );
    }

    @Override
    protected void setupPresenter() {
        mActionsListener = new ListElementsPresenter<>(
                this,
                Injection.provideRestaurantsRepository(getContext())
        );
    }

    @Override
    protected String getEmptyListMessage() {
        return null;
    }

    @Override
    protected void setupDataOnLinkedViews() {
        //Not necessary.
    }

    @Override
    public void showElementDetail(Restaurant elementSnap) {
        Bundle bundle = new Bundle();
        bundle.putString(RestaurantDetailActivity.EXTRA_RESTAURANT, new Gson().toJson(elementSnap));
        ActivityHelper.getInstance().begin(
                getActivity(), RestaurantDetailActivity.class, bundle
        );
    }

    @Override
    public void displayLargeFailedMessage(String largeMessage) {
        //TODO: Implement SnackBar long
    }

    @Override
    public void search(final String criteria) {
        ((FilterableRestaurantsAdapter )mAdapter).getFilter().filter(criteria);
    }

    /**
     * IMPORTANT: This method defines the filter criteria used by
     * the filterable adapter {@link FilterableRestaurantsAdapter}
     */
    public final FilterableRestaurantsAdapter.FilterMethod<Restaurant> mFilterMethod = new FilterableRestaurantsAdapter.FilterMethod<Restaurant>() {
        @Override
        public boolean evaluate(Restaurant filterableItem, String criteria) {
            return filterableItem.getName().toLowerCase().contains(criteria.toLowerCase())
                    || filterableItem.getDescription().toLowerCase().contains(criteria.toLowerCase())
                    || filterableItem.getBranchOffice().toLowerCase().contains(criteria.toLowerCase());
        }
    };

    public static ListRestaurantsFragment newInstance() {
        Bundle arguments = new Bundle();
        ListRestaurantsFragment fragment = new ListRestaurantsFragment();
        fragment.setArguments(arguments);
        return fragment;
    }
}

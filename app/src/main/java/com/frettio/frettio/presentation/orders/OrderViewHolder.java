package com.frettio.frettio.presentation.orders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.frettio.frettio.R;

/**
 * Created by luisburgos on 3/5/17.
 */

public class OrderViewHolder extends RecyclerView.ViewHolder {

    public TextView counterView;
    public TextView restaurantNameTextView;
    public TextView restaurantEmailTextView;
    public TextView costTextView;

    public OrderViewHolder(View itemView) {
        super(itemView);
        counterView = (TextView) itemView.findViewById(R.id.counterView);
        restaurantNameTextView = (TextView) itemView.findViewById(R.id.restaurantNameTextView);
        restaurantEmailTextView = (TextView) itemView.findViewById(R.id.restaurantEmailTextView);
        costTextView = (TextView) itemView.findViewById(R.id.costTextView);
    }

}

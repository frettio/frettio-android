package com.frettio.frettio.presentation.signin;

import android.text.TextUtils;
import android.util.Log;

import com.frettio.frettio.app.common.utils.Utils;
import com.frettio.frettio.data.local.managers.TokenManager;
import com.frettio.frettio.data.local.managers.UserSessionManager;
import com.frettio.frettio.domain.interactor.AbstractSignInInteractor;
import com.frettio.frettio.domain.model.User;
import com.frettio.frettio.presentation.mvp.AbstractPresenter;
import com.frettio.frettio.presentation.mvp.Repository;

import static com.frettio.frettio.app.common.AppConstants.TAG;
import static com.frettio.frettio.app.common.utils.ActivityHelper.checkNotNull;


/**
 * Actions listener implementation to all actions on signIn view {@link SignInContract.View}
 */
public class SignInPresenter extends AbstractPresenter<SignInContract.View, AbstractSignInInteractor> implements SignInContract.UserActionsListener {

    private UserSessionManager mSessionManager;
    private TokenManager mTokenManager;

    private final int MAX_SIGN_IN_INTENTS = 5;
    private int signInIntents;

    /**
     * Class constructor
     * @param view signIn view reference.
     */
    public SignInPresenter(
            SignInContract.View view,
            AbstractSignInInteractor interactor,
            UserSessionManager sessionManager,
            TokenManager tokenManager
    ) {
        super(view, interactor);
        mInteractor = checkNotNull(interactor);
        mSessionManager = checkNotNull(sessionManager);
        mTokenManager = checkNotNull(tokenManager);
        signInIntents = 0;
    }

    /**
     * This method depends on {@link SignInPresenter#isValidInformation(String, String)} to
     * validate user information when tries to signIn. If information is valid tells the
     * interactor to make a server request.
     */
    @Override
    public void doSignIn(String email, String password){
        if(canSignIn() && isValidInformation(email, password)){
            mView.setProgressIndicator(true);
            mInteractor.signIn(email, password, new Repository.CompositeItemInformationCallback<User, String>() {
                @Override
                public void onItemInformationLoaded(User user, String token) {
                    mView.setProgressIndicator(false);
                    mSessionManager.saveSession(user);
                    mTokenManager.saveSession(token);
                    mView.sendToMain();
                }

                @Override
                public void onNetworkError(String networkErrorMessage) {
                    mView.setProgressIndicator(false);
                    mView.displayFailedMessage(networkErrorMessage);
                }

                @Override
                public void onServerError(String serverErrorMessage) {
                    mView.setProgressIndicator(false);
                    Log.d(TAG, "onServerError: " + serverErrorMessage);
                    if(canSignIn()){
                        mView.displaySignInLimitNumberOfTryoutsMessage(MAX_SIGN_IN_INTENTS - (++signInIntents));
                    } else {
                        mView.displayMaxNumberOfIntentsReachedMessage();
                    }
                }
            });
        } else {
            mView.displayMaxNumberOfIntentsReachedMessage();
        }
    }

    @Override
    public void recoverPassword() {
        mView.sendToPasswordRecovery();
    }

    private boolean canSignIn() {
        return signInIntents < MAX_SIGN_IN_INTENTS;
    }

    /**
     * Validates user signIn information.
     * @param email text from UI field
     * @param password text from UI field
     * @return boolean value whether provided information is valid/invalid.
     */
    private boolean isValidInformation(String email, String password) {
        boolean isValid = true;

        if(TextUtils.isEmpty(email) || !Utils.isAnEmail(email)){
            mView.setEmailErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(password)){
            mView.setPasswordErrorMessage();
            isValid = false;
        }

        if(!isValid){
            mView.displayFailedMessage("Por favor, llena todos los campos");
        }

        return isValid;
    }


}

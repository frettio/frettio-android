package com.frettio.frettio.presentation.signin;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.frettio.frettio.R;
import com.frettio.frettio.app.Injection;
import com.frettio.frettio.app.common.utils.ActivityHelper;
import com.frettio.frettio.app.common.utils.Utils;
import com.frettio.frettio.data.APIConstants;
import com.frettio.frettio.presentation.main.MainActivity;
import com.frettio.frettio.presentation.mvp.activity.BasePermissionActivityView;

public class SignInActivity extends BasePermissionActivityView implements SignInContract.View {

    private EditText emailEditText;
    private EditText passwordEditText;
    private RelativeLayout signInContainer;

    private ProgressBar progress;

    private SignInContract.UserActionsListener mActionsListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView(R.layout.activity_sign_in);
        setupPresenter();
    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return super.onSupportNavigateUp();
    }

    /**
     * Binds all UI elements to class variables in order to manipulate them later.
     */
    @Override
    protected void initViews() {
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        progress = (ProgressBar) findViewById(R.id.progress);
        signInContainer = (RelativeLayout) findViewById(R.id.signInContainer);

        final Button loginButton = (Button) findViewById(R.id.signInButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyboard(SignInActivity.this);
                mActionsListener.doSignIn(
                        emailEditText.getText().toString().trim(),
                        passwordEditText.getText().toString().trim()
                );
            }
        });

        final TextView appLogoTitle = (TextView) findViewById(R.id.appLogoTitle);
        appLogoTitle.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryText));

        final TextView forgetPasswordAction = (TextView) findViewById(R.id.forgetPasswordAction);
        Utils.makeTextViewClickable(forgetPasswordAction, new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                mActionsListener.recoverPassword();
            }
        });
    }

    /**
     * Setups the main signIn layout.
     * @return a RelativeLayout that represents the main layout of the signIn view.
     */
    @Override
    protected ViewGroup getMainLayout() {
        return signInContainer;
    }

    /**
     * Shows error message on email field {@link SignInActivity#emailEditText} when fails validation.
     */
    @Override
    public void setEmailErrorMessage() {
        emailEditText.setError(getResources().getString(R.string.error_email_field));
    }

    /**
     * Shows error message on password field {@link SignInActivity#passwordEditText} when fails validation.
     */
    @Override
    public void setPasswordErrorMessage() {
        passwordEditText.setError(getResources().getString(R.string.error_password_field));
    }

    /**
     * When signIn on server succeeds, this method redirects the user to the main view of the
     * app.
     *
     * This method also ask for runtime permissions if the current device version needs it.
     */
    @Override
    public void sendToMain() {
        requestAllPermissions(new AfterAskedPermissionsAction() {
            @Override
            public void execute(boolean result) {
                ActivityHelper.getInstance().sendAndFinishWithFlags(
                        SignInActivity.this, MainActivity.class, Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                );
            }
        });
    }

    /**
     *
     */
    @Override
    public void sendToPasswordRecovery() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(APIConstants.FORGOT_PASSWORD_ENDPOINT));
        startActivity(browserIntent);
    }

    @Override
    public void displaySignInLimitNumberOfTryoutsMessage(int currentIntent) {
        displayLargeFailedMessage(getString(R.string.error_limit_number_sign_in_intents, String.valueOf(currentIntent)));
    }

    @Override
    public void displayMaxNumberOfIntentsReachedMessage() {
        displayLargeFailedMessage(getString(R.string.error_max_number_intents_reached));
    }

    /**
     * Stops progress view if needed.
     * @param active determines if UI is loading.
     */
    @Override
    public void setProgressIndicator(boolean active) {
        if(active) {
            signInContainer.setVisibility(View.INVISIBLE);
            progress.setVisibility(View.VISIBLE);
        } else {
            progress.setVisibility(View.INVISIBLE);
            signInContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    protected boolean getOnNeedToCheckForPermanentlyDeniedPermissions() {
        return false;
    }

    @Override
    protected String getOnAskToOpenSettingsMessage() {
        return getString(R.string.rationale_ask_again);
    }

    /**
     * Setups {@link SignInPresenter} actions listener.
     */
    private void setupPresenter() {
        mActionsListener = new SignInPresenter(
                this,
                Injection.provideSignInInteractor(),
                Injection.provideUserSessionManager(this),
                Injection.provideTokenManager(this)
        );
    }
}


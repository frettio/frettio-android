package com.frettio.frettio.presentation.orders.currentorder;

import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.presentation.mvp.lists.ListElementsContract;

/**
 * Created by luisburgos on 3/5/17.
 */

public interface ListCurrentOrderContract {

    interface View extends ListElementsContract.View<FoodDish> {

        void displayRemovedFromOrderMessage(int dishPosition);

        void displayNewNumberOfDish(int newNumber, int fromPosition, FoodDish dish);
    }

    interface UserActionsListener extends ListElementsContract.UserActionsListener<FoodDish> {

        boolean removeFromOrder(int position, FoodDish dish);

        void increaseNumberOfSameDishes(int newNumber, int fromPosition, FoodDish ofDish);

        void decreaseNumberOfSameDishes(int newNumber, int fromPosition, FoodDish ofDish);

    }

}

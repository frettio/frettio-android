package com.frettio.frettio.presentation.adapter;

public interface ItemListener<Item> {
    void onItemClick(Item clickedUser);
}
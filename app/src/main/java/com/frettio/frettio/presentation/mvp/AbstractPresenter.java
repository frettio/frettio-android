package com.frettio.frettio.presentation.mvp;

import com.frettio.frettio.domain.interactor.Interactor;

/**
 * Base class for common presenters that frequently uses {@link BaseView} methods when
 * receives response from a server request {@link com.frettio.frettio.presentation.mvp.Repository.FetchCallback}.
 * This class depends on a concrete view that inherits from {@link BaseView}
 * Created by luisburgos on 8/31/16.
 */
public abstract class AbstractPresenter<ConcreteView extends BaseView, ConcreteInteractor extends Interactor>
        implements Repository.FetchCallback {

    /**
     * Concrete implementation of BaseView.
     */
    protected ConcreteView mView;

    /**
     * Concrete implementation of AbstractInteractor.
     */
    protected ConcreteInteractor mInteractor;



    public AbstractPresenter(ConcreteView mView, ConcreteInteractor mInteractor) {
        this.mView = mView;
        this.mInteractor = mInteractor;
    }

    /**
     * This method is called when network is unavailable or unreachable.
     * @param networkErrorMessage this message depends on the state of network.
     */
    @Override
    public void onNetworkError(String networkErrorMessage) {
        notifyFailure(networkErrorMessage);
    }

    /**
     * This method is called when a server request fails.
     * @param serverErrorMessage message return by the server.
     */
    @Override
    public void onServerError(String serverErrorMessage) {
        notifyFailure(serverErrorMessage);
    }

    /**
     * Helper method to tell the UI stop the loading state and shows a message to the user.
     * @param message to show.
     */
    protected void notifyFailure(String message) {
        mView.setProgressIndicator(false);
        mView.displayFailedMessage(message);
    }



}

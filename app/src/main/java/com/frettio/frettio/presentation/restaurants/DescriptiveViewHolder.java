package com.frettio.frettio.presentation.restaurants;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.frettio.frettio.R;

/**
 * Created by luisburgos on 3/2/17.
 */

public abstract class DescriptiveViewHolder extends RecyclerView.ViewHolder {

    public TextView nameTextView;
    public TextView descriptionTextView;
    public ImageView logoImageView;

    public DescriptiveViewHolder(View itemView) {
        super(itemView);

        nameTextView = (TextView) itemView.findViewById(R.id.nameTextView);
        descriptionTextView = (TextView) itemView.findViewById(R.id.descriptionTextView);
        logoImageView = (ImageView) itemView.findViewById(R.id.logoImageView);
    }

}

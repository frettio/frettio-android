package com.frettio.frettio.presentation.main;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.frettio.frettio.R;
import com.frettio.frettio.app.common.utils.CircleTransformation;
import com.squareup.picasso.Picasso;

import static com.frettio.frettio.app.common.utils.ActivityHelper.checkNotNull;

public class UserInformationViewHolder {

    private Context mContext;

    public ImageView circleViewContainer;
    public TextView nameTextView;

    public UserInformationViewHolder(View root, Context context) {
        checkNotNull(root);
        mContext = checkNotNull(context);
        circleViewContainer = (ImageView) root.findViewById(R.id.circleViewContainer);
        nameTextView = (TextView) root.findViewById(R.id.nameTextView);
    }

    public void loadImageProfile(String imageProfileURL) {
        Picasso.with(mContext)
                .load(imageProfileURL)
                .transform(new CircleTransformation())
                //.placeholder(R.mipmap.ic_user_photo_holder)
                .into(circleViewContainer);
    }
}

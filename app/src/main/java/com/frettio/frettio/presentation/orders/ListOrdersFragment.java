package com.frettio.frettio.presentation.orders;

import com.frettio.frettio.R;
import com.frettio.frettio.app.Injection;
import com.frettio.frettio.domain.model.Order;
import com.frettio.frettio.presentation.mvp.lists.ListElementsFragment;
import com.frettio.frettio.presentation.mvp.lists.ListElementsPresenter;

import java.util.ArrayList;

/**
 * Created by luisburgos on 2/25/17.
 */

public class ListOrdersFragment extends ListElementsFragment<Order, OrderViewHolder> {

    @Override
    protected void setupAdapter() {
        mAdapter = new ListOrdersAdapter(
                new ArrayList<Order>(0),
                mItemListener
        );
    }

    @Override
    protected void setupPresenter() {
        mActionsListener = new ListElementsPresenter<Order> (
                this,
                Injection.provideOrdersInteractor(getContext())
        );
    }

    @Override
    protected void setupDataOnLinkedViews() {
        //NOT NECESSARY
    }

    @Override
    protected String getEmptyListMessage() {
        return getString(R.string.empty_orders);
    }

    @Override
    public void displayLargeFailedMessage(String largeMessage) {
        setSnackbarMessage(largeMessage);
        mSnackbar.show();
    }

    @Override
    public void showElementDetail(Order elementSnap) {
        //NOT NECESSARY
    }
}

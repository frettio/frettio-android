package com.frettio.frettio.presentation.mvp.lists;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.frettio.frettio.R;
import com.frettio.frettio.app.common.AppConstants;
import com.frettio.frettio.presentation.adapter.EndlessRecyclerOnScrollListener;
import com.frettio.frettio.presentation.adapter.ItemListener;
import com.frettio.frettio.presentation.adapter.ListAdapter;
import com.frettio.frettio.presentation.mvp.fragment.BaseFragment;

import java.util.List;

import static com.frettio.frettio.app.common.AppConstants.TAG;

/**
 * Created by luisburgos on 2/17/17.
 */

public abstract class ListElementsFragment<Model, ItemViewHolder extends RecyclerView.ViewHolder>
        extends BaseFragment implements ListElementsContract.View<Model> {

    protected ListElementsContract.UserActionsListener<Model> mActionsListener;
    protected ListAdapter<Model, ItemViewHolder> mAdapter;
    protected LinearLayoutManager mLayoutManager;

    private RecyclerView mRecyclerView;
    private RelativeLayout mEmptyState;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupAdapter();
        setupPresenter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mActionsListener.loadElements(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int getMainLayoutResource() {
        return R.layout.component_fragment_list;
    }

    @Override
    public void setEmptyListState(boolean isEmpty) {
        Log.d(TAG, "Change view elements visible");
        if(isEmpty){
            mRecyclerView.setVisibility(View.GONE);
            mEmptyState.setVisibility(View.VISIBLE);
        } else {
            mEmptyState.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void bindViews(View root) {
        setupRecyclerView(root);
        setupSwipeRefreshLayout(root);
        setupEmptyStateContainer(root);
    }

    @Override
    public void setProgressIndicator(final boolean active) {
        if (getView() == null) {
            return;
        }
        final SwipeRefreshLayout srl =
                (SwipeRefreshLayout) getView().findViewById(R.id.refresh_layout);

        // Make sure setRefreshing() is called after the layout is done with everything else.
        srl.post(new Runnable() {
            @Override
            public void run() {
                srl.setRefreshing(active);
            }
        });
    }

    @Override
    public void displayFailedMessage(String message) {
        if(getView() == null){
            return;
        }
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showElements(List<Model> elements) {
        Log.d(TAG, "Showing total of " + elements.size());
        mAdapter.replaceData(elements);
    }

    protected abstract void setupAdapter();

    protected abstract void setupPresenter();

    protected abstract String getEmptyListMessage();

    protected void setupRecyclerView(View root) {
        mRecyclerView = (RecyclerView) root.findViewById(R.id.recycler_view);

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                mActionsListener.loadElements(true);
            }
        });
    }

    protected void setupSwipeRefreshLayout(View root) {
        SwipeRefreshLayout swipeRefreshLayout =
                (SwipeRefreshLayout) root.findViewById(R.id.refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mActionsListener.loadElements(true);
            }
        });
    }

    /**
     * Listener for clicks on notes in the RecyclerView.
     */
    protected ItemListener<Model> mItemListener = new ItemListener<Model>() {
        @Override
        public void onItemClick(Model clickedElement) {
            mActionsListener.openElement(clickedElement);
        }
    };

    public void setupEmptyStateContainer(View root) {
        mEmptyState = (RelativeLayout) root.findViewById(R.id.emptyStateContainer);
        if(getEmptyListMessage() != null){
            ((TextView) mEmptyState.findViewById(R.id.emptyStateMessage)).setText(
                    getEmptyListMessage()
            );
        }
    }
}

package com.frettio.frettio.presentation.main;

public enum MainMenuOption {
        RESTAURANTS,
        ORDERS,
        MY_ORDER,
        ACCOUNT,
        LOGOUT
}
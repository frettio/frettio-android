package com.frettio.frettio.presentation.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

/**
 * Wrapper to create a reusable Dialog to allow the user to log out from the application.
 * This class handles the cancel action and provides an interface to interact with the
 * confirmation action.
 */
public class LogoutDialog extends Dialog implements View.OnClickListener {

    private Context mContext;
    private OnConfirmationLogout mListener;
    private Dialog mDialog;
    private Button btnConfirmation;
    private Button btnCancel;

    private int mDialogLayoutID;
    private int mConfirmationButtonID;
    private int mCancelButtonID;

    public LogoutDialog(
            Context context,
            int dialogLayoutID,
            int confirmationButtonID,
            int cancelButtonID,
            OnConfirmationLogout listener
    ) {
        super(context);
        this.mContext = context;
        this.mDialogLayoutID = dialogLayoutID;
        this.mConfirmationButtonID = confirmationButtonID;
        this.mCancelButtonID = cancelButtonID;
        this.mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(mDialogLayoutID);
        btnConfirmation = (Button) findViewById(mConfirmationButtonID);
        btnCancel = (Button) findViewById(mCancelButtonID);
        btnCancel.setOnClickListener(this);
        btnConfirmation.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (this.isShowing()) {
            this.dismiss();
        }

        if(v.getId() == mConfirmationButtonID){
            mListener.onConfirmation();
        }else if (v.getId() == mCancelButtonID){
            mListener.onCancel();
        }
    }

    public interface OnConfirmationLogout {
        void onConfirmation();
        void onCancel();
    }

}
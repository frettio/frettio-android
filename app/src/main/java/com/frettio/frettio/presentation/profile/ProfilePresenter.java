package com.frettio.frettio.presentation.profile;

import android.text.TextUtils;

import com.frettio.frettio.data.local.managers.UserSessionManager;
import com.frettio.frettio.domain.interactor.AbstractProfileInteractor;
import com.frettio.frettio.domain.model.Consumer;
import com.frettio.frettio.domain.model.User;
import com.frettio.frettio.presentation.mvp.AbstractPresenter;
import com.frettio.frettio.presentation.mvp.Repository;

/**
 * Created by Melissa on 25/02/2017.
 */

public class ProfilePresenter
        extends AbstractPresenter<ProfileContract.View, AbstractProfileInteractor>
        implements ProfileContract.UserActionsListener {

    private UserSessionManager mSessionManager;

    public ProfilePresenter(
            ProfileContract.View mView,
            AbstractProfileInteractor mInteractor,
            UserSessionManager sessionManager) {
        super(mView, mInteractor);

        mSessionManager = sessionManager;
    }

    @Override
    public void loadProfile(boolean forceUpdate) {
        mView.setEditionMode(false);
        if(forceUpdate) {
            mView.setProgressIndicator(true);
            mInteractor.getConsumer(mSessionManager.getSession().getEmail(), new Repository.SingleItemInformationCallback<User>() {
                @Override
                public void onItemInformationLoaded(User user) {
                    mView.setProgressIndicator(false);
                    mSessionManager.saveSession(user);
                    updateViewInformation();
                }

                @Override
                public void onNetworkError(String networkErrorMessage) {
                    notifyFailure(networkErrorMessage);
                }

                @Override
                public void onServerError(String serverErrorMessage) {
                    notifyFailure(serverErrorMessage);
                }
            });
        } else {
            updateViewInformation();
        }

    }

    private void updateViewInformation() {
        User user = mSessionManager.getSession();
        mView.displayProfileInformation(
                user.getFirstName(),
                user.getLastNames(),
                user.getEmail(),
                user.getPhone(),
                user.getHomeAddress().getNumber(),
                user.getHomeAddress().getCrossStreets(),
                user.getHomeAddress().getHouseBlock(),
                user.getHomeAddress().getStreet(),
                user.getHomeAddress().getCity(),
                user.getImageURL()
        );
    }

    @Override
    public void editProfileInformation(
            String firstName,
            String lastNames,
            String email,
            String phone,
            String streetNumber,
            String city,
            String streetName,
            String crossStreets,
            String houseBlock
    ) {
        if(isValidInformation(email, firstName, lastNames, phone, streetNumber, city, streetName, crossStreets, houseBlock)){
            mView.setProgressIndicator(true);

            Consumer updatedConsumer = new Consumer();
            updatedConsumer.setEmail(email);
            updatedConsumer.setFirstName(firstName);
            updatedConsumer.setLastNames(lastNames);

            updatedConsumer.setNumber(streetNumber);
            updatedConsumer.setCity(city);
            updatedConsumer.setStreet(streetName);
            updatedConsumer.setCrossStreets(crossStreets);
            updatedConsumer.setHouseBlock(houseBlock);

            mInteractor.updateConsumer(updatedConsumer, new Repository.SingleItemInformationCallback<User>() {
                @Override
                public void onItemInformationLoaded(User user) {
                    mView.setProgressIndicator(false);
                    mSessionManager.saveSession(user);
                    mView.setEditionMode(false);
                    mView.displayProfileUpdatedMessage();
                }

                @Override
                public void onNetworkError(String networkErrorMessage) {
                    notifyFailure(networkErrorMessage);
                }

                @Override
                public void onServerError(String serverErrorMessage) {
                    notifyFailure(serverErrorMessage);
                }
            });
        }else{
            mView.setProgressIndicator(false);
        }
    }

    /**
     * Validates user signIn information.
     * @return boolean value whether provided information is valid/invalid.
     */
    private boolean isValidInformation(
            String email,
            String firstName,
            String lastNames,
            String phone,
            String streetNumber,
            String city,
            String streetName,
            String crossStreets,
            String houseBlock
    ) {
        boolean isValid = true;

        if(TextUtils.isEmpty(email)){
            mView.setEmailErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(firstName)){
            mView.setFirstNameErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(lastNames)){
            mView.setLastNamesErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(phone) || !TextUtils.isDigitsOnly(phone)){
            mView.setPhoneErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(streetName)){
            mView.setStreetNameErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(streetNumber)){
            mView.setStreetNumberErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(city)){
            mView.setCityErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(crossStreets)){
            mView.setCrossStreetsErrorMessage();
            isValid = false;
        }

        if(TextUtils.isEmpty(houseBlock)){
            mView.setHouseBlockErrorMessage();
            isValid = false;
        }

        return isValid;
    }
}

package com.frettio.frettio.presentation.mvp.lists;

/**
 * Created by luisburgos on 3/2/17.
 */

public interface Searchable {
    void search(final String criteria);
}

package com.frettio.frettio.presentation.orders;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.frettio.frettio.R;
import com.frettio.frettio.domain.model.Order;
import com.frettio.frettio.presentation.adapter.ItemListener;
import com.frettio.frettio.presentation.adapter.ListAdapter;

import java.util.List;

/**
 * Created by luisburgos on 3/5/17.
 */

public class ListOrdersAdapter extends ListAdapter<Order, OrderViewHolder> {

    protected ListOrdersAdapter(
            @NonNull List<Order> orders,
            @NonNull ItemListener<Order> itemListener
    ) {
        super(orders, itemListener);
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View noteView = inflater.inflate(R.layout.component_list_item_order, parent, false);
        return new OrderViewHolder(noteView);
    }

    @Override
    public void onBindViewHolder(OrderViewHolder holder, int position) {
        final Order order = mItemList.get(position);
        holder.counterView.setText(
                mContext.getString(R.string.text_holder_total_dishes, String.valueOf(order.getDishes().size())
        ));
        holder.restaurantNameTextView.setText(order.getRestaurant().getName());
        holder.restaurantEmailTextView.setText(order.getRestaurant().getEmail());

        holder.costTextView.setText(
                mContext.getString(R.string.text_holder_total_cost, String.valueOf(order.getTotal()))
        );
    }
}

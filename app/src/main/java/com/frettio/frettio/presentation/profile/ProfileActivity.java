package com.frettio.frettio.presentation.profile;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.frettio.frettio.R;
import com.frettio.frettio.app.Injection;
import com.frettio.frettio.app.common.utils.PicassoCustomLoader;
import com.frettio.frettio.app.common.utils.Utils;
import com.frettio.frettio.presentation.mvp.activity.BaseActivityView;

public class ProfileActivity extends BaseActivityView implements ProfileContract.View {

    private EditText inputName;
    private EditText inputLastName;
    private EditText inputEmail;
    private EditText inputPhoneNumber;
    private TextInputLayout inputPasswordContainer;
    private TextInputLayout inputPasswordConfirmContainer;;
    private EditText inputNumber;
    private EditText inputCrossStreets;
    private EditText inputHouseBlock;
    private EditText inputStreet;
    private EditText inputCity;
    private ImageView userProfileImageView;

    private ProgressBar progress;

    private Button signUpButton;
    private TextView alreadyHaveAnAccountAction;

    private RelativeLayout profileLayoutContainer;
    private ProfileContract.UserActionsListener mActionListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView(R.layout.activity_profile);
        setupPresenter();
    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mActionListener.loadProfile(true);
    }

    @Override
    protected ViewGroup getMainLayout() {
        return profileLayoutContainer;
    }

    @Override
    protected void initViews() {
        inputName = (EditText) findViewById(R.id.inputName);
        inputLastName = (EditText) findViewById(R.id.inputLastName);
        inputEmail = (EditText) findViewById(R.id.inputEmail);
        inputPhoneNumber = (EditText) findViewById(R.id.inputPhoneNumber);
        inputNumber = (EditText) findViewById(R.id.inputNumber);
        inputCrossStreets = (EditText) findViewById(R.id.inputCrossStreets);
        inputHouseBlock = (EditText) findViewById(R.id.inputHouseBlock);
        inputStreet = (EditText) findViewById(R.id.inputStreet);
        inputCity = (EditText) findViewById(R.id.inputCity);
        progress = (ProgressBar) findViewById(R.id.progress);
        profileLayoutContainer = (RelativeLayout) findViewById(R.id.profileLayoutContainer);
        progress = (ProgressBar) findViewById(R.id.progress);
        signUpButton = (Button) findViewById(R.id.signUpButton);
        alreadyHaveAnAccountAction = (TextView) findViewById(R.id.alreadyHaveAnAccountAction);
        userProfileImageView = (ImageView) findViewById(R.id.userProfileImageView);

        inputName.clearFocus();
        inputPasswordContainer = (TextInputLayout) findViewById(R.id.passwordInputLayout);
        inputPasswordConfirmContainer = (TextInputLayout) findViewById(R.id.passwordConfirmInputLayout);

        inputPasswordContainer.setVisibility(View.GONE);
        inputPasswordConfirmContainer.setVisibility(View.GONE);
        signUpButton.setVisibility(View.GONE);

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.hideKeyboard(ProfileActivity.this);
                setProgressIndicator(true);
                mActionListener.editProfileInformation(
                        inputName.getText().toString(),
                        inputLastName.getText().toString(),
                        inputEmail.getText().toString(),
                        inputPhoneNumber.getText().toString(),
                        inputNumber.getText().toString(),
                        inputCity.getText().toString(),
                        inputStreet.getText().toString(),
                        inputCrossStreets.getText().toString(),
                        inputHouseBlock.getText().toString()
                );
            }
        });

        final TextInputLayout inputEmailContainer = (TextInputLayout) findViewById(R.id.inputEmailContainer);
        inputEmailContainer.setVisibility(View.GONE);
        inputEmail.setEnabled(false);
        inputEmail.setVisibility(View.GONE);
        alreadyHaveAnAccountAction.setVisibility(View.GONE);
    }

    @Override
    protected void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_edit) {
            signUpButton.setText(getString(R.string.profile_update));
            setEditionMode(true);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void displayProfileInformation(
            String firstName,
            String lastName,
            String email,
            String phone,
            String number,
            String crossStreets,
            String houseBlock,
            String street,
            String city,
            String imageProfileURL
    ) {
        inputName.setText(firstName);
        inputLastName.setText(lastName);
        inputEmail.setText(email);
        inputPhoneNumber.setText(phone);
        inputNumber.setText(number);
        inputCrossStreets.setText(crossStreets);
        inputHouseBlock.setText(houseBlock);
        inputStreet.setText(street);
        inputCity.setText(city);

        //TODO: Load imageURL.
        new PicassoCustomLoader().load(getApplicationContext(), userProfileImageView, imageProfileURL);
    }

    @Override
    public void displayProfileUpdatedMessage() {
        Snackbar.make(profileLayoutContainer, getString(R.string.profile_updated), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void setEditionMode(boolean editable) {
        inputName.setEnabled(editable);
        inputLastName.setEnabled(editable);
        inputPhoneNumber.setEnabled(editable);
        inputNumber.setEnabled(editable);
        inputCrossStreets.setEnabled(editable);
        inputHouseBlock.setEnabled(editable);
        inputStreet.setEnabled(editable);
        inputCity.setEnabled(editable);

        if(editable){
            signUpButton.setVisibility(View.VISIBLE) ;
        }else{
            signUpButton.setVisibility(View.GONE) ;
        }
    }

    @Override
    public void setFirstNameErrorMessage() {
        inputName.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setLastNamesErrorMessage() {
        inputLastName.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setEmailErrorMessage() {
        inputEmail.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setPhoneErrorMessage() {
        inputPhoneNumber.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setStreetNumberErrorMessage() {
        inputNumber.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setCityErrorMessage() {
        inputCity.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setStreetNameErrorMessage() {
        inputStreet.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setCrossStreetsErrorMessage() {
        inputCrossStreets.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setHouseBlockErrorMessage() {
        inputHouseBlock.setError(getString(R.string.error_empty_fields));
    }

    @Override
    public void setProgressIndicator(boolean active) {
        if(active) {
            profileLayoutContainer.setVisibility(View.INVISIBLE);
            progress.setVisibility(View.VISIBLE);
        } else {
            progress.setVisibility(View.INVISIBLE);
            profileLayoutContainer.setVisibility(View.VISIBLE);
        }
    }

    private void setupPresenter() {
        mActionListener = new ProfilePresenter(
                this,
                Injection.provideProfileInteractor(),
                Injection.provideUserSessionManager(this)
        );
    }
}

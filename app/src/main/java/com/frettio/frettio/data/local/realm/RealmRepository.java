package com.frettio.frettio.data.local.realm;

import com.frettio.frettio.data.local.realm.mappers.Mapper;
import com.frettio.frettio.presentation.mvp.Repository;

import io.realm.RealmConfiguration;
import io.realm.RealmModel;

/**
 * Realm based local persistence main entry point. Only local persistence which is
 * Realm based specific should inherit this class.
 *
 * Created by luisburgos on 9/13/16.
 */
public abstract class RealmRepository<RealmEntity extends RealmModel, DomainEntity>
        implements Repository<DomainEntity> {

    protected final RealmConfiguration realmConfiguration;
    protected final Mapper<RealmEntity, DomainEntity> toDomainEntityMapper;

    /**
     * Class constructor.
     * @param realmConfiguration setup for specific Realm instance.
     * @param mapper bridge between realm objects and domain model objects.
     */
    public RealmRepository(final RealmConfiguration realmConfiguration, Mapper<RealmEntity, DomainEntity> mapper) {
        this.realmConfiguration = realmConfiguration;
        this.toDomainEntityMapper = mapper;
    }

}


package com.frettio.frettio.data.local.managers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.frettio.frettio.app.SessionExpirationDateTransformer;
import com.frettio.frettio.domain.model.HomeAddress;
import com.frettio.frettio.domain.model.User;
import com.google.gson.Gson;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static com.frettio.frettio.app.common.AppConstants.TAG;

public class UserSessionManager extends SessionManager<User> {

    private static final String USER_PREFERENCES = "USER_PREFERENCES";
    private static final String IS_USER_LOGIN = "IS_USER_LOGIN";

    public static final String KEY_EMAIL = "KEY_EMAIL";
    public static final String KEY_NAME = "KEY_NAME";
    public static final String KEY_LAST_NAME = "KEY_LAST_NAME";
    public static final String KEY_PHONE = "KEY_PHONE";
    public static final String KEY_IMAGE_URL = "KEY_IMAGE_URL";
    public static final String KEY_CITY = "KEY_CITY";
    public static final String KEY_ADDRESS_NUMBER = "KEY_ADDRESS_NUMBER";
    public static final String KEY_ADDRESS_CROSS_STREETS = "KEY_ADDRESS_CROSS_STREETS";
    public static final String KEY_ADDRESS_HOUSE_BLOCK = "KEY_ADDRESS_HOUSE_BLOCK";
    public static final String KEY_ADDRESS_STREET = "KEY_ADDRESS_STREET";

    public static final String KEY_TOKEN = "TOKEN";
    public static final String KEY_SIGN_IN_DATE = "KEY_SIGN_IN_DATE";
    public static final String KEY_SIGN_IN_EXPIRATION_DATE = "KEY_SIGN_IN_EXPIRATION_DATE";

    private SessionExpirationDateTransformer mDateTransformer;

    public UserSessionManager(Context context, SessionExpirationDateTransformer dateTransformer) {
        super(context);
        mDateTransformer = dateTransformer;
    }

    @Override
    public void saveSession(User user) {
        Log.d(
                TAG,
                "Registering data: " + user.getEmail() + ", "
                        + user.getFirstName() + ", " + user.getLastNames() + ", " + user.getPhone()
        );

        mEditor.putBoolean(IS_USER_LOGIN, true);
        mEditor.putString(KEY_EMAIL, user.getEmail());
        mEditor.putString(KEY_NAME, user.getFirstName());
        mEditor.putString(KEY_LAST_NAME, user.getLastNames());
        mEditor.putString(KEY_PHONE, user.getPhone());
        mEditor.putString(KEY_IMAGE_URL, user.getImageURL());

        if(user.getHomeAddress() != null){
            mEditor.putString(KEY_CITY, user.getHomeAddress().getCity());
            mEditor.putString(KEY_ADDRESS_NUMBER, user.getHomeAddress().getNumber());
            mEditor.putString(KEY_ADDRESS_CROSS_STREETS, user.getHomeAddress().getCrossStreets());
            mEditor.putString(KEY_ADDRESS_HOUSE_BLOCK, user.getHomeAddress().getHouseBlock());
            mEditor.putString(KEY_ADDRESS_STREET, user.getHomeAddress().getStreet());
        }

        mEditor.putString(KEY_SIGN_IN_DATE, mDateTransformer.getSignInDate().toString());
        mEditor.putString(KEY_SIGN_IN_EXPIRATION_DATE, mDateTransformer.getExpirationDate().toString());
        mEditor.commit();
    }

    @Override
    public void clearSession() {
        mEditor.remove(IS_USER_LOGIN);
        mEditor.remove(KEY_EMAIL);
        mEditor.remove(KEY_NAME);
        mEditor.remove(KEY_LAST_NAME);
        mEditor.remove(KEY_PHONE);
        mEditor.remove(KEY_IMAGE_URL);
        mEditor.remove(KEY_CITY);
        mEditor.remove(KEY_ADDRESS_NUMBER);
        mEditor.remove(KEY_ADDRESS_CROSS_STREETS);
        mEditor.remove(KEY_ADDRESS_HOUSE_BLOCK);
        mEditor.remove(KEY_ADDRESS_STREET);
        mEditor.remove(KEY_SIGN_IN_DATE);
        mEditor.remove(KEY_SIGN_IN_EXPIRATION_DATE);
        mEditor.commit();
    }

    @Override
    public User getSession() {
        User user = new User();
        user.setEmail(mPreferences.getString(KEY_EMAIL, null));
        user.setFirstName(mPreferences.getString(KEY_NAME, null));
        user.setLastNames(mPreferences.getString(KEY_LAST_NAME, null));
        user.setPhone(mPreferences.getString(KEY_PHONE, null));
        user.setImageURL(mPreferences.getString(KEY_IMAGE_URL, null));

        HomeAddress homeAddress = new HomeAddress();
        homeAddress.setCity(mPreferences.getString(KEY_CITY, null));
        homeAddress.setNumber(mPreferences.getString(KEY_ADDRESS_NUMBER, null));
        homeAddress.setCrossStreets(mPreferences.getString(KEY_ADDRESS_CROSS_STREETS, null));
        homeAddress.setHouseBlock(mPreferences.getString(KEY_ADDRESS_HOUSE_BLOCK, null));
        homeAddress.setStreet(mPreferences.getString(KEY_ADDRESS_STREET, null));

        user.setHomeAddress(homeAddress);
        Log.d(TAG, "Retrieve user " + new Gson().toJson(user));

        return user;
    }

    @NonNull
    @Override
    public boolean isSessionSaved() {
        return mPreferences.getBoolean(IS_USER_LOGIN, false);
    }

    @Override
    public String getSessionStorageName() {
        return USER_PREFERENCES;
    }

    public boolean isValidSession() {
        Calendar calendar = new GregorianCalendar();
        Date currentDate = calendar.getTime();
        Date signInDate = new Date(mPreferences.getString(KEY_SIGN_IN_DATE, null));
        Date expirationDate = new Date(mPreferences.getString(KEY_SIGN_IN_EXPIRATION_DATE, null));

        Log.d(TAG, "currentDate " + currentDate.toString());
        Log.d(TAG, "signInDate " + signInDate.toString());
        Log.d(TAG, "expirationDate " + expirationDate.toString());

        return !currentDate.after(expirationDate);
    }
}
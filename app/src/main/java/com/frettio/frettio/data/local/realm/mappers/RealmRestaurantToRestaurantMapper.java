package com.frettio.frettio.data.local.realm.mappers;

import com.frettio.frettio.data.local.realm.models.RealmRestaurant;
import com.frettio.frettio.domain.model.Restaurant;

/**
 * Created by luisburgos on 1/27/17.
 */

public class RealmRestaurantToRestaurantMapper implements Mapper<RealmRestaurant, Restaurant>{

    @Override
    public Restaurant map(RealmRestaurant realmItem) {
        Restaurant restaurant = new Restaurant();

        restaurant.setName(realmItem.getName());
        restaurant.setDescription(realmItem.getDescription());

        return restaurant;
    }

}

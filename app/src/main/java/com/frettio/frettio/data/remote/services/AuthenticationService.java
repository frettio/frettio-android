package com.frettio.frettio.data.remote.services;

import com.frettio.frettio.data.APIConstants;
import com.frettio.frettio.data.remote.responses.SignInResponse;
import com.frettio.frettio.domain.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Endpoint to provided user authentication.
 * This interface is based on the Authentication reference on the project API definition.
 *
 * IMPORTANT: Current app version uses Apiary as the tool for API reference.
 * @see <a href="">API Reference</a>
 *
 */
public interface AuthenticationService {

    @POST(APIConstants.LOGIN_ENDPOINT)
    Call<SignInResponse> signIn(@Body User user);

}

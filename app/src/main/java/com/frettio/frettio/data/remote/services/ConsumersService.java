package com.frettio.frettio.data.remote.services;

import com.frettio.frettio.data.APIConstants;
import com.frettio.frettio.data.remote.responses.ConsumerResponse;
import com.frettio.frettio.domain.model.Consumer;
import com.frettio.frettio.domain.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Melissa on 25/02/2017.
 */

public interface ConsumersService {

    @GET(APIConstants.GET_CONSUMER_ENDPOINT)
    Call<User> getConsumer(
            @Path(APIConstants.Keys.EMAIL) String email
    );

    @PUT(APIConstants.UPDATE_CONSUMER_ENDPOINT)
    Call<User> updateConsumer(
            @Path(APIConstants.Keys.EMAIL) String email,
            @Body Consumer user
    );
}

package com.frettio.frettio.data;

/**All server constants defined for API communication.
 * IMPORTANT: Current app version uses Apiary as the tool for API reference.
 * @see <a href="http://docs.frettio.apiary.io/#reference">Frettio API</a>
 *
 * Created by Roberto on 11/17/16.
 */

public class APIConstants {

    //public static final String BASE_URL = "http://frettio.io/";
    public static final String BASE_URL = "http://192.168.1.77:8081/";
    //public static final String BASE_URL = "http://10.0.2.2:8081/";
    private static final String API_PATH = "api"; //NOTE: Not for this project

    private static final String API_ENDPOINT = BASE_URL + API_PATH;

    public static class Keys {
        public static final String DESCRIPTION = "description";
        public static final String USERNAME = "username";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String EMAIL = "email";
        public static final String PHONE = "phone";
        public static final String PASSWORD = "password";
        public static final String ID = "id";
        public static final String IS_CREATED = "is_created";
        public static final String IS_CHANGE = "is_change";
        public static final String IS_EDITED = "is_edited";
    }

    public static class Queries {
        public static final String STATE = "state";
    }

    public static final String UNAUTHORIZED = "UNAUTHORIZED";

    ///LOGIN.
    /**
     * LOGIN.
     * Supports: POST.
     */
    private static final String LOGIN_PATH = "/login";
    public static final String LOGIN_ENDPOINT = API_ENDPOINT + LOGIN_PATH;

    //SIGN UP.

    public static final String BASIC_SIGN_UP_PATH = "/consumers";
    public static final String BASIC_SIGN_UP_ENDPOINT = API_ENDPOINT + BASIC_SIGN_UP_PATH;
    public static final String SIGN_UP_ENDPOINT = BASIC_SIGN_UP_ENDPOINT +"/";

    //RESTAURANTS
    private static final String RESTAURANTS_PATH = "/restaurants";
    private static final String RESTAURANTS_ENDPOINT = API_ENDPOINT + RESTAURANTS_PATH;

    public static final String LIST_ALL_RESTAURANTS_ENDPOINT = RESTAURANTS_ENDPOINT;
    public static final String GET_RESTAURANT_ENDPOINT = RESTAURANTS_ENDPOINT + "/{"+ Keys.EMAIL +"}";

    //CONSUMERS
    public static final String CONSUMERS_PATH = "/consumers";
    private static final String CONSUMERS_ENDPOINT = API_ENDPOINT + CONSUMERS_PATH;

    public static final String GET_CONSUMER_ENDPOINT = CONSUMERS_ENDPOINT + "/{"+ Keys.EMAIL +"}";
    public static final String UPDATE_CONSUMER_ENDPOINT = GET_CONSUMER_ENDPOINT;

    //DISHES
    public static final String DISHES_PATH = "/food";
    public static final String DISHES_ENDPOINT = RESTAURANTS_ENDPOINT + "/{"+ Keys.EMAIL +"}" + DISHES_PATH;

    public static final String LIST_ALL_DISHES_ENDPOINT = DISHES_ENDPOINT;

    //ORDERS
    public static final String ORDERS_PATH = "/orders";
    public static final String HISTORY_PATH = "/history";

    private static final String ORDERS_ENDPOINT = API_ENDPOINT + ORDERS_PATH ;
    private static final String ORDERS_HISTORY_ENDPOINT = API_ENDPOINT + ORDERS_PATH + HISTORY_PATH;

    public static final String CREATE_ORDER_ENDPOINT = ORDERS_ENDPOINT;
    public static final String GET_ORDERS_HISTORY_ENDPOINT = ORDERS_HISTORY_ENDPOINT + "/{"+ Keys.EMAIL +"}";

    //ACCOUNT CONFIG
    public static final String FORGOT_PASSWORD_ENDPOINT = BASE_URL + "#!/forgotPassword";
}

package com.frettio.frettio.data.local.managers;

import android.content.Context;
import android.util.Log;

import com.frettio.frettio.domain.interactor.Interactor;
import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.domain.model.NewOrder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.frettio.frettio.app.common.AppConstants.TAG;

/**
 * Created by luisburgos on 3/4/17.
 */

public class CurrentOrderManager extends SessionManager<NewOrder> implements Interactor<NewOrder> {

    private static final String CURRENT_ORDER_PREFERENCES = "CURRENT_ORDER_PREFERENCES";

    public static final String KEY_DISHES = "KEY_DISHES";

    public static final String KEY_RESTAURANT_NAME = "KEY_NAME";
    public static final String KEY_RESTAURANT_EMAIL = "KEY_RESTAURANT_EMAIL";
    public static final String KEY_CONSUMER_EMAIL = "KEY_CONSUMER_EMAIL";
    public static final String KEY_TOTAL = "KEY_TOTAL";

    public CurrentOrderManager(Context context) {
        super(context);
        String restaurantEmail = mPreferences.getString(KEY_RESTAURANT_EMAIL, null);
        Log.d(TAG, "Current restaurant is " + restaurantEmail);
    }

    public boolean isFromSameCurrentRestaurant(FoodDish dish){
        String restaurantEmail = mPreferences.getString(KEY_RESTAURANT_EMAIL, null);
        Log.d(TAG, "Current restaurant is " + restaurantEmail + " comparing to " + dish.getRestaurantEmail());
        return restaurantEmail != null && restaurantEmail.equals(dish.getRestaurantEmail());
    }

    public void addToOrder(FoodDish dish){
        HashMap<Long, FoodDish> dishes = getOrderDishes();
        if(dishes == null){
            dishes = new HashMap<>();
        }
        dishes.put(dish.getId(), dish);
        setOrderDishes(dishes);
        getOrderDishes();
    }

    public void removeFromOrder(FoodDish dish){
        HashMap<Long, FoodDish> dishes = getOrderDishes();
        dishes.remove(dish.getId());
        if(dishes.isEmpty()){
            mEditor.remove(KEY_DISHES);
            mEditor.commit();
        }

        setOrderDishes(dishes);
        getOrderDishes();
    }

    @Override
    public void saveSession(NewOrder toSave) {
        Log.d(
                TAG,
                "Registering order: " + toSave.getRestaurantEmail() + ", " + toSave.getRestaurantName()
                        + ", " + toSave.getConsumerEmail() + ", " + toSave.getTotal()
        );

        setOrderDishes(getDishesMap(toSave.getDishes()));
        mEditor.putString(KEY_RESTAURANT_NAME, toSave.getRestaurantName());
        mEditor.putString(KEY_RESTAURANT_EMAIL, toSave.getRestaurantEmail());
        mEditor.putString(KEY_CONSUMER_EMAIL, toSave.getConsumerEmail());
        mEditor.putFloat(KEY_TOTAL, Float.valueOf(String.valueOf(toSave.getTotal())));
        mEditor.commit();
        recalculateTotal();
    }

    private HashMap<Long, FoodDish> getDishesMap(ArrayList<FoodDish> dishes) {
        HashMap<Long, FoodDish> hashMap = new HashMap<>();
        for(FoodDish dish : dishes){
            hashMap.put(dish.getId(), dish);
        }
        return hashMap;
    }

    public ArrayList<FoodDish> getListOfDishes() {
        if(getOrderDishes() != null){
            return new ArrayList<>(getOrderDishes().values());
        } else {
            return new ArrayList<>(0);
        }
    }

    @Override
    public void clearSession() {
        Log.d(TAG, "Clearing ORDER");
        mEditor.remove(KEY_RESTAURANT_NAME);
        mEditor.remove(KEY_RESTAURANT_EMAIL);
        mEditor.remove(KEY_CONSUMER_EMAIL);
        mEditor.remove(KEY_TOTAL);
        mEditor.remove(KEY_DISHES);
        mEditor.commit();
    }

    @Override
    public NewOrder getSession() {
        NewOrder currentOrder = new NewOrder();
        currentOrder.setRestaurantEmail(mPreferences.getString(KEY_RESTAURANT_EMAIL, null));
        currentOrder.setRestaurantName(mPreferences.getString(KEY_RESTAURANT_NAME, null));
        currentOrder.setConsumerEmail(mPreferences.getString(KEY_CONSUMER_EMAIL, null));
        currentOrder.setTotal(mPreferences.getFloat(KEY_TOTAL, 0));
        currentOrder.setDishes(getListOfDishes());
        return currentOrder;
    }

    @Override
    public boolean isSessionSaved() {
        return !getOrderDishes().isEmpty();
    }

    @Override
    public String getSessionStorageName() {
        return CURRENT_ORDER_PREFERENCES;
    }

    private void setOrderDishes(HashMap<Long, FoodDish> dishes){
        String dishesJSON = new Gson().toJson(dishes);
        Log.d(TAG, "SETTING: " + dishesJSON);
        mEditor.putString(KEY_DISHES, dishesJSON);
        mEditor.commit();
        recalculateTotal();
    }

    private HashMap<Long, FoodDish> getOrderDishes() {
        String dishesJSON = mPreferences.getString(KEY_DISHES, null);
        Log.d(TAG, "RETRIEVE: " + dishesJSON);
        return new Gson().fromJson(dishesJSON, new TypeToken<HashMap<Long, FoodDish>>(){}.getType());
    }

    public void setRestaurantName(String restaurantName) {
        if(getListOfDishes().isEmpty()){
            mEditor.putString(KEY_RESTAURANT_NAME, restaurantName);
            mEditor.commit();
        }
    }

    public void setRestaurantEmail(String restaurantEmail) {
        if(getListOfDishes().isEmpty()){
            mEditor.putString(KEY_RESTAURANT_EMAIL, restaurantEmail);
            mEditor.commit();
        }
    }

    public void updateDish(FoodDish ofDish) {
        HashMap<Long, FoodDish> dishHashMap = getOrderDishes();
        for(Map.Entry<Long, FoodDish> entry : dishHashMap.entrySet()) {
            Long dishID = entry.getKey();
            if(dishID == ofDish.getId()){
                entry.setValue(ofDish);
            }
        }
        setOrderDishes(dishHashMap);
    }

    private void recalculateTotal(){
        Log.d(TAG, "COST Recalculate total cost");
        float total = 0;
        HashMap<Long, FoodDish> dishHashMap = getOrderDishes();
        for(Map.Entry<Long, FoodDish> entry : dishHashMap.entrySet()) {
            FoodDish dish = entry.getValue();
            total += (dish.getCost() * (dish.getQuantity() == 0 ? 1 : dish.getQuantity()));
        }

        Log.d(TAG, "COST Total is " + total + " for " + dishHashMap.values().size() + " dishes");
        mEditor.putFloat(KEY_TOTAL, total);
        mEditor.commit();
    }

    public Float getTotal() {
        return mPreferences.getFloat(KEY_TOTAL, 0);
    }
}

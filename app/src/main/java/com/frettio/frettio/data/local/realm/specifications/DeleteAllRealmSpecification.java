package com.frettio.frettio.data.local.realm.specifications;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by Roberto on 11/17/16.
 */

public class DeleteAllRealmSpecification<QueryObject extends RealmObject> extends RealmSpecification<QueryObject> {

    public DeleteAllRealmSpecification(Class<QueryObject> classToQuery) {
        super(classToQuery);
    }

    /**
     * Query to delete all entries from database table.
     * @param realm instance of Realm database
     * @return null because there is no need to used a list of {@link RealmResults}
     */
    @Override
    public RealmResults<QueryObject> toRealmResults(Realm realm) {
        realm.delete(getClassToQuery());
        return null;
    }
}

package com.frettio.frettio.data.local.realm.models;

import com.frettio.frettio.domain.model.FoodDish;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by luisburgos on 3/5/17.
 */

public class RealmOrder extends RealmObject {

    @PrimaryKey
    private long id;
    private String restaurantEmail;
    private String restaurantName;
    private String consumerEmail;
    private double total;
    private RealmList<RealmFoodDish> dishes;

    public RealmOrder() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRestaurantEmail() {
        return restaurantEmail;
    }

    public void setRestaurantEmail(String restaurantEmail) {
        this.restaurantEmail = restaurantEmail;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getConsumerEmail() {
        return consumerEmail;
    }

    public void setConsumerEmail(String consumerEmail) {
        this.consumerEmail = consumerEmail;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public RealmList<RealmFoodDish> getDishes() {
        return dishes;
    }

    public void setDishes(RealmList<RealmFoodDish> dishes) {
        this.dishes = dishes;
    }
}

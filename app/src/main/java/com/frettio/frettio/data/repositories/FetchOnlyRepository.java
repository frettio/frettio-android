package com.frettio.frettio.data.repositories;

import com.frettio.frettio.presentation.mvp.Repository;

/**
 * Simple implementation of {@link Repository} that only supports fetching data from data sources.
 * WARNING: Use this class if only {@link Repository#get(int, SingleItemInformationCallback)} and
 * {@link Repository#getAll(ListAllCallback)} needs to be used.
 * Created by luisburgos on 9/29/16.
 */

public abstract class FetchOnlyRepository<Item> implements Repository<Item> {

    @Override
    public void add(Item item) {
        //Not supported by remote client.
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(Iterable<Item> items) {
        //Not supported by remote client.
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Item item) {
        //Not supported by remote client.
        throw new UnsupportedOperationException();
    }

    @Override
    public void remove(Item item) {
        //Not supported by remote client.
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteAll() {
        // Not supported
        throw new UnsupportedOperationException();
    }

}

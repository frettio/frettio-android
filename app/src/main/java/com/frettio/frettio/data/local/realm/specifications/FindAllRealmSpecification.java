package com.frettio.frettio.data.local.realm.specifications;

import com.frettio.frettio.data.local.realm.RealmRepository;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;


/**
 * public class AllRealmSpecificationImplementation of real specification to query all from local database.
 */

public class FindAllRealmSpecification<QueryObject extends RealmObject> extends RealmSpecification<QueryObject>{

    public FindAllRealmSpecification(Class<QueryObject> classToQuery) {
        super(classToQuery);
    }

    /**
     * Query all from local database table.
     * @param realm current instance
     * @return {@link RealmResults} list with all clients table results.
     */
    @Override
    public RealmResults<QueryObject> toRealmResults(Realm realm) {
        return realm.where(getClassToQuery()).findAll();
    }

}

package com.frettio.frettio.data.local;

import com.frettio.frettio.data.local.realm.RealmRepository;
import com.frettio.frettio.data.local.realm.mappers.Mapper;
import com.frettio.frettio.data.local.realm.models.RealmRestaurant;
import com.frettio.frettio.data.local.realm.specifications.DeleteAllRealmSpecification;
import com.frettio.frettio.data.local.realm.specifications.FindAllRealmSpecification;
import com.frettio.frettio.data.local.realm.specifications.FindByIdRealmSpecification;
import com.frettio.frettio.data.local.realm.specifications.RealmSpecification;
import com.frettio.frettio.domain.model.Restaurant;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by luisburgos on 1/27/17.
 */

public class RestaurantsLocalRepository extends RealmRepository<RealmRestaurant, Restaurant> {

    private static RestaurantsLocalRepository INSTANCE;

    private RestaurantsLocalRepository(RealmConfiguration realmConfiguration, Mapper mapper) {
        super(realmConfiguration, mapper);
    }

    public static RestaurantsLocalRepository getInstance(RealmConfiguration realmConfiguration, Mapper mapper) {
        if (INSTANCE == null) {
            INSTANCE = new RestaurantsLocalRepository(realmConfiguration, mapper);
        }
        return INSTANCE;
    }

    /**
     * Save a object into local database. Executes individual transaction on Realm database.
     * @param restaurant object to persist locally.
     */
    @Override
    public void add(final Restaurant restaurant) {
        final Realm realm = Realm.getInstance(realmConfiguration);

        //TODO: Add interface callbacks to match realm callback
        realm.executeTransactionAsync(
                new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        final RealmRestaurant realmItem = realm.createObject(RealmRestaurant.class);
                        //SETTERS
                    }
                },
                new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        // Transaction was a success.
                    }
                },
                new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        // Transaction failed and was automatically canceled.
                    }
                }
        );

        realm.close();
    }

    /**
     * Saves a list of objects into local database. This method iterates over the list and
     * call {@link RestaurantsLocalRepository#add(Restaurant)} to execute individual transactions.
     * @param restaurants list of objects to save.
     */
    @Override
    public void add(Iterable<Restaurant> restaurants) {
        final Realm realm = Realm.getInstance(realmConfiguration);

        List<RealmRestaurant> realmItems = new LinkedList<>();
        for(Restaurant item : restaurants){
            final RealmRestaurant realmItem = new RealmRestaurant();
            //SETTERS
            realmItems.add(realmItem);
        }

        realm.beginTransaction();
        realm.copyToRealmOrUpdate(realmItems);
        realm.commitTransaction();
    }

    /**
     * Retrieves all objects from local database.
     * @param callback responder of transaction.
     */
    @Override
    public void getAll(final ListAllCallback<Restaurant> callback) {
        final RealmSpecification<RealmRestaurant> realmSpecification = new FindAllRealmSpecification<>(RealmRestaurant.class);

        final Realm realm = Realm.getInstance(realmConfiguration);
        final RealmResults<RealmRestaurant> realmResults = realmSpecification.toRealmResults(realm);

        if(realmResults.isEmpty()){
            callback.onServerError("");
        } else {
            final List<Restaurant> items = new ArrayList<>();

            for (RealmRestaurant realmPet : realmResults) {
                items.add(toDomainEntityMapper.map(realmPet));
            }

            realm.close();
            callback.onItemsLoaded(items);
        }
    }

    /**
     * Retrieve a single object matching id provided. If there is no object matching the method
     * @param callback responder of transaction.
     * @param id matching primary key id
     */
    @Override
    public void get(int id, SingleItemInformationCallback<Restaurant> callback) {
        final RealmSpecification realmSpecification = new FindByIdRealmSpecification<RealmRestaurant>(
                RealmRestaurant.class, "email", id
        );

        final Realm realm = Realm.getInstance(realmConfiguration);
        final RealmResults<RealmRestaurant> realmResult = realmSpecification.toRealmResults(realm);

        Restaurant restaurant = toDomainEntityMapper.map(realmResult.first());
        if (restaurant != null) {
            callback.onItemInformationLoaded(restaurant);
        } else {
            callback.onServerError("");
        }
    }

    @Override
    public void deleteAll() {
        final RealmSpecification realmSpecification = new DeleteAllRealmSpecification(RealmRestaurant.class);
        final Realm realm = Realm.getInstance(realmConfiguration);

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realmSpecification.toRealmResults(realm);
            }
        });
    }

    @Override
    public void update(Restaurant item) {
        //Not supported.
    }

    @Override
    public void remove(Restaurant item) {
        //Not supported.
    }

    @Override
    public void refresh() {
        // Not required because the {@link } handles the logic of refreshing the
        // elders from all the available data sources.
    }
}


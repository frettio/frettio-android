package com.frettio.frettio.data.remote.services;

import com.frettio.frettio.data.APIConstants;
import com.frettio.frettio.domain.model.Consumer;
import com.frettio.frettio.domain.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by luisburgos on 2/13/17.
 */

public interface SignUpService {

    @POST(APIConstants.SIGN_UP_ENDPOINT)
    Call<User> signUp(@Body Consumer user);
}

package com.frettio.frettio.data.local.managers;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import static com.frettio.frettio.app.common.utils.PermissionHelper.TAG;

/**
 * Created by luisburgos on 4/15/17.
 */

public class TokenManager extends SessionManager<String> {

    private static final String TOKEN_PREFERENCES = "TOKEN_PREFERENCES";;

    public static final String KEY_TOKEN = "TOKEN";

    public TokenManager(Context context) {
        super(context);
    }

    @Override
    public void saveSession(String token) {
        Log.d(TAG, "Registering token: " + token);
        mEditor.putString(KEY_TOKEN, token);
        mEditor.commit();
    }

    @Override
    public void clearSession() {
        mEditor.remove(KEY_TOKEN);
        mEditor.commit();
    }

    @Override
    public String getSession() {
        String token = mPreferences.getString(KEY_TOKEN, null);
        Log.d(TAG, "Retrieve token: " + token);

        return token;
    }

    @Override
    public boolean isSessionSaved() {
        return false;
    }

    @Override
    public String getSessionStorageName() {
        return TOKEN_PREFERENCES;
    }
}

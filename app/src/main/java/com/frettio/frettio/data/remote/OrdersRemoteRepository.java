package com.frettio.frettio.data.remote;

import com.frettio.frettio.data.repositories.FetchOnlyRepository;
import com.frettio.frettio.domain.interactor.AbstractOrderInteractor;
import com.frettio.frettio.domain.model.Order;
import com.frettio.frettio.presentation.mvp.Repository;

import org.jetbrains.annotations.NotNull;

import static android.os.Build.VERSION_CODES.M;
import static com.frettio.frettio.app.common.utils.ActivityHelper.checkNotNull;

/**
 * Created by luisburgos on 3/5/17.
 */

public class OrdersRemoteRepository extends FetchOnlyRepository<Order> {

    private AbstractOrderInteractor mInteractor;
    private String mUserEmail;

    public OrdersRemoteRepository(
            @NotNull AbstractOrderInteractor interactor,
            String userEmail
    ){
        mInteractor = checkNotNull(interactor);
        mUserEmail = checkNotNull(userEmail);
    }

    /**
     * Fetch all elders that belongs to the current application user.
     * @param callback observer of the server request.
     */
    @Override
    public void getAll(Repository.ListAllCallback<Order> callback) {
        mInteractor.getAllOrders(mUserEmail, callback);
    }

    /**
     * Fetch a single elder matching the id provided.
     * @param id specific primary key id of the desired elder.
     * @param callback observer of the server request.
     */
    @Override
    public void get(int id, SingleItemInformationCallback<Order> callback) {
        //DO NOTHING!!
    }

    @Override
    public void refresh() {
        // Not required because the {@link EldersRepository} handles the logic of refreshing the
        // elders from all the available data sources.
    }
}



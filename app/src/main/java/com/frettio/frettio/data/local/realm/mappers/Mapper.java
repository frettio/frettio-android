package com.frettio.frettio.data.local.realm.mappers;

/**
 * Created by Roberto on 11/17/16.
 */

public interface Mapper<From, To> {
    To map(From from);
}

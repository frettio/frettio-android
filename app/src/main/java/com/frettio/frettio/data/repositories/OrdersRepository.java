package com.frettio.frettio.data.repositories;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.frettio.frettio.domain.model.Order;
import com.frettio.frettio.presentation.mvp.Repository;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.frettio.frettio.app.common.AppConstants.TAG;
import static com.frettio.frettio.app.common.utils.ActivityHelper.checkNotNull;

/**
 * Created by luisburgos on 2/25/17.
 */

public class OrdersRepository implements Repository<Order> {

    private static OrdersRepository INSTANCE = null;

    private final Repository<Order> mRemoteDataSource;

    private final Repository<Order> mLocalDataSource;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    Map<Long, Order> mCache;

    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */
    boolean mCacheIsDirty = false;

    private OrdersRepository(
            @NonNull Repository<Order> remoteDataSource,
            @NonNull Repository<Order> localDataSource
    ) {
        this.mRemoteDataSource = checkNotNull(remoteDataSource);
        this.mLocalDataSource = checkNotNull(localDataSource);
    }

    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param remoteDataSource the backend data source
     * @param localDataSource  the device storage data source
     * @return the {@link Repository} instance
     */
    public static OrdersRepository getInstance(
            Repository<Order> remoteDataSource,
            Repository<Order> localDataSource
    ){
        if (INSTANCE == null) {
            INSTANCE = new OrdersRepository(remoteDataSource, localDataSource);
        }
        return INSTANCE;
    }

    /**
     * Used to force {@link #getInstance(Repository, Repository)} to create a new instance
     * next time it's called.
     */
    public static void destroyInstance() {
        INSTANCE = null;
    }

    /**
     * Gets elders from cache, local data source (Realm) or remote data source, whichever is
     * available first.
     * <p>
     *
     * @param callback observer of the server request that expects a list of results.
     */
    @Override
    public void getAll(final ListAllCallback<Order> callback) {
        checkNotNull(callback);

        // Respond immediately with cache if available and not dirty
        if (mCache != null && !mCacheIsDirty) {
            callback.onItemsLoaded(new ArrayList<>(mCache.values()));
            return;
        }

        if (mCacheIsDirty) {
            // If the cache is dirty we need to fetch new data from the network.
            getFromRemoteDataSource(callback);
        } else {
            // Query the local storage if available. If not, query the network.
            mLocalDataSource.getAll(new ListAllCallback<Order>() {
                @Override
                public void onItemsLoaded(List<Order> list) {
                    Log.d(TAG, "Adding to cache total if " + list.size());
                    refreshCache(list);
                    callback.onItemsLoaded(new ArrayList<>(mCache.values()));
                }

                @Override
                public void onNetworkError(String networkErrorMessage) {
                    //Do nothing
                }

                @Override
                public void onServerError(String serverErrorMessage) {
                    getFromRemoteDataSource(callback);
                }

            });
        }
    }

    /**
     * Gets elders from local data source (Realm) unless the table is new or empty. In that
     * case it uses the network data source.
     * <p>
     *
     * @param callback observer of the server request that expects a single item.
     * @param id specific primary key id of the desired elder.
     */
    @Override
    public void get(final int id, final SingleItemInformationCallback<Order> callback) {
        checkNotNull(id);
        checkNotNull(callback);

        Order cachedItem = getWithId(id);

        // Respond immediately with cache if available
        if (cachedItem != null) {
            callback.onItemInformationLoaded(cachedItem);
            return;
        }

        // Load from server/persisted if needed.

        // Is the task in the local data source? If not, query the network.
        mLocalDataSource.get(id, new SingleItemInformationCallback<Order>() {
            @Override
            public void onItemInformationLoaded(Order Order) {
                callback.onItemInformationLoaded(Order);
            }

            @Override
            public void onNetworkError(String networkErrorMessage) {
                //Do nothing
            }

            @Override
            public void onServerError(String serverErrorMessage) {
                mRemoteDataSource.get(id, new SingleItemInformationCallback<Order>() {
                    @Override
                    public void onItemInformationLoaded(Order Order) {
                        callback.onItemInformationLoaded(Order);
                    }

                    @Override
                    public void onNetworkError(String networkErrorMessage) {
                        callback.onServerError(networkErrorMessage);
                    }

                    @Override
                    public void onServerError(String serverErrorMessage) {
                        callback.onServerError(serverErrorMessage);
                    }
                });
            }

        });
    }

    /**
     * Deletes all entries on data sources, local data source (Realm), remote server and
     * cached elders.
     *
     */
    @Override
    public void deleteAll() {
        mLocalDataSource.deleteAll();

        if (mCache == null) {
            mCache = new LinkedHashMap<>();
        }
        mCache.clear();
    }

    /**
     * Sets the cached state to dirty in order to directly call server on any request.
     */
    @Override
    public void refresh() {
        mCacheIsDirty = true;
    }

    @Override
    public void add(Order item) {
        checkNotNull(item);
        mLocalDataSource.add(item);

        if (mCache == null){
            mCache = new LinkedHashMap<>();
        }
        mCache.put(item.getId(), item);
    }

    @Override
    public void add(Iterable<Order> items) {
        mLocalDataSource.add(items);
    }

    @Override
    public void update(Order item) {
        //Not supported
    }

    @Override
    public void remove(Order item) {
        //Not supported
    }

    /**
     * Makes remote call using remote data source implementation.
     *
     * @param callback delegate observer of request.
     */
    private void getFromRemoteDataSource(final ListAllCallback<Order> callback) {
        mRemoteDataSource.getAll(new ListAllCallback<Order>() {
            @Override
            public void onItemsLoaded(List<Order> list) {
                refreshCache(list);
                refreshLocalDataSource(list);
                callback.onItemsLoaded(new ArrayList<>(mCache.values()));
            }

            @Override
            public void onNetworkError(String networkErrorMessage) {
                callback.onNetworkError(networkErrorMessage);
            }

            @Override
            public void onServerError(String serverErrorMessage) {
                callback.onServerError(serverErrorMessage);
            }

        });
    }

    /**
     * Updates cache content.
     * @param orders updated list of objects.
     */
    private void refreshCache(List<Order> orders) {
        if (mCache == null) {
            mCache = new LinkedHashMap<>();
        }
        mCache.clear();
        for (Order order : orders) {
            mCache.put(order.getId(), order);
        }
        mCacheIsDirty = false;
    }

    /**
     * Updates local data source (Realm) content.
     * @param Orders updated list of objects.
     */
    private void refreshLocalDataSource(List<Order> Orders) {
        mLocalDataSource.deleteAll();
        mLocalDataSource.add(Orders);
    }

    /**
     * Adds cache content.
     * @param Orders list of new objects.
     */
    private void addToLocalDataSource(List<Order> Orders) {
        for (Order item : Orders) {
            mCache.put(item.getId(), item);
        }
    }

    /**
     * Adds local data source (Realm) content.
     * @param Orders list of new objects.
     */
    private void addToCache(List<Order> Orders) {
        mLocalDataSource.add(Orders);
    }

    /**
     * Helper method to retrieve object from cache.
     *
     * @param id specific primary key.
     * @return a {@link Order} object matching provided id.
     */
    @Nullable
    private Order getWithId(long id) {
        checkNotNull(id);
        if (mCache == null || mCache.isEmpty()) {
            return null;
        } else {
            return mCache.get(id);
        }
    }
}

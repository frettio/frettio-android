package com.frettio.frettio.data.local.realm.mappers;

import com.frettio.frettio.data.local.realm.models.RealmFoodDish;
import com.frettio.frettio.domain.model.FoodDish;

/**
 * Created by luisburgos on 1/27/17.
 */

public class RealmFoodDishToFoodDishMapper implements Mapper<RealmFoodDish, FoodDish>{

    @Override
    public FoodDish map(RealmFoodDish realmItem) {
        FoodDish foodDish = new FoodDish();

        foodDish.setId(realmItem.getId());
        foodDish.setName(realmItem.getName());
        foodDish.setDescription(realmItem.getDescription());
        foodDish.setCost(realmItem.getCost());
        foodDish.setRestaurantEmail(realmItem.getRestaurantEmail());
        foodDish.setImageURL(realmItem.getImageURL());

        return foodDish;
    }

}

package com.frettio.frettio.data.local;

import com.frettio.frettio.data.local.realm.RealmRepository;
import com.frettio.frettio.data.local.realm.mappers.Mapper;
import com.frettio.frettio.data.local.realm.models.RealmFoodDish;
import com.frettio.frettio.data.local.realm.specifications.DeleteAllRealmSpecification;
import com.frettio.frettio.data.local.realm.specifications.FindAllRealmSpecification;
import com.frettio.frettio.data.local.realm.specifications.FindByIdRealmSpecification;
import com.frettio.frettio.data.local.realm.specifications.RealmSpecification;
import com.frettio.frettio.domain.model.FoodDish;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by luisburgos on 3/3/17.
 */

public class FoodDishesLocalRepository extends RealmRepository<RealmFoodDish, FoodDish> {

    private static FoodDishesLocalRepository INSTANCE;

    private FoodDishesLocalRepository(RealmConfiguration realmConfiguration, Mapper mapper) {
        super(realmConfiguration, mapper);
    }

    public static FoodDishesLocalRepository getInstance(RealmConfiguration realmConfiguration, Mapper mapper) {
        if (INSTANCE == null) {
            INSTANCE = new FoodDishesLocalRepository(realmConfiguration, mapper);
        }
        return INSTANCE;
    }

    /**
     * Save a object into local database. Executes individual transaction on Realm database.
     * @param restaurant object to persist locally.
     */
    @Override
    public void add(final FoodDish restaurant) {
        final Realm realm = Realm.getInstance(realmConfiguration);

        //TODO: Add interface callbacks to match realm callback
        realm.executeTransactionAsync(
                new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        final RealmFoodDish realmItem = realm.createObject(RealmFoodDish.class);
                        //SETTERS
                    }
                },
                new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        // Transaction was a success.
                    }
                },
                new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        // Transaction failed and was automatically canceled.
                    }
                }
        );

        realm.close();
    }

    /**
     * Saves a list of objects into local database. This method iterates over the list and
     * call {@link FoodDishesLocalRepository#add(FoodDish)} to execute individual transactions.
     * @param restaurants list of objects to save.
     */
    @Override
    public void add(Iterable<FoodDish> restaurants) {
        final Realm realm = Realm.getInstance(realmConfiguration);

        List<RealmFoodDish> realmItems = new LinkedList<>();
        for(FoodDish item : restaurants){
            final RealmFoodDish realmItem = new RealmFoodDish();
            //SETTERS
            realmItems.add(realmItem);
        }

        realm.beginTransaction();
        realm.copyToRealmOrUpdate(realmItems);
        realm.commitTransaction();
    }

    /**
     * Retrieves all objects from local database.
     * @param callback responder of transaction.
     */
    @Override
    public void getAll(final ListAllCallback<FoodDish> callback) {
        final RealmSpecification<RealmFoodDish> realmSpecification = new FindAllRealmSpecification<>(RealmFoodDish.class);

        final Realm realm = Realm.getInstance(realmConfiguration);
        final RealmResults<RealmFoodDish> realmResults = realmSpecification.toRealmResults(realm);

        if(realmResults.isEmpty()){
            callback.onServerError("");
        } else {
            final List<FoodDish> items = new ArrayList<>();

            for (RealmFoodDish realmFoodDish : realmResults) {
                items.add(toDomainEntityMapper.map(realmFoodDish));
            }

            realm.close();
            callback.onItemsLoaded(items);
        }
    }

    /**
     * Retrieve a single object matching id provided. If there is no object matching the method
     * @param callback responder of transaction.
     * @param id matching primary key id
     */
    @Override
    public void get(int id, SingleItemInformationCallback<FoodDish> callback) {
        final RealmSpecification realmSpecification = new FindByIdRealmSpecification<RealmFoodDish>(
                RealmFoodDish.class, "email", id
        );

        final Realm realm = Realm.getInstance(realmConfiguration);
        final RealmResults<RealmFoodDish> realmResult = realmSpecification.toRealmResults(realm);

        FoodDish foodDish = toDomainEntityMapper.map(realmResult.first());
        if (foodDish != null) {
            callback.onItemInformationLoaded(foodDish);
        } else {
            callback.onServerError("");
        }
    }

    @Override
    public void deleteAll() {
        final RealmSpecification realmSpecification = new DeleteAllRealmSpecification(RealmFoodDish.class);
        final Realm realm = Realm.getInstance(realmConfiguration);

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realmSpecification.toRealmResults(realm);
            }
        });
    }

    @Override
    public void update(FoodDish item) {
        //Not supported.
    }

    @Override
    public void remove(FoodDish item) {
        //Not supported.
    }

    @Override
    public void refresh() {
        // Not required because the {@link } handles the logic of refreshing the
        // elders from all the available data sources.
    }
}


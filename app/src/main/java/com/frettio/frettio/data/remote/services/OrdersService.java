package com.frettio.frettio.data.remote.services;

import com.frettio.frettio.data.APIConstants;
import com.frettio.frettio.domain.model.NewOrder;
import com.frettio.frettio.domain.model.Order;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by luisburgos on 2/25/17.
 */

public interface OrdersService {

    @POST(APIConstants.CREATE_ORDER_ENDPOINT)
    Call<Void> createOrder(@Body NewOrder order);

    @GET(APIConstants.GET_ORDERS_HISTORY_ENDPOINT)
    Call<List<Order>> getOrdersHistory(
            @Path(APIConstants.Keys.EMAIL) String email
    );
}

package com.frettio.frettio.data.remote;

import com.frettio.frettio.data.repositories.FetchOnlyRepository;
import com.frettio.frettio.domain.interactor.AbstractFoodDishInteractor;
import com.frettio.frettio.domain.model.FoodDish;

import org.jetbrains.annotations.NotNull;

import static com.frettio.frettio.app.common.utils.ActivityHelper.checkNotNull;

/**
 * Created by luisburgos on 3/3/17.
 */

public class FoodDishesRemoteRepository extends FetchOnlyRepository<FoodDish> {

    private AbstractFoodDishInteractor mInteractor;

    public FoodDishesRemoteRepository(@NotNull AbstractFoodDishInteractor interactor){
        mInteractor = checkNotNull(interactor);
    }

    /**
     * Fetch all elders that belongs to the current application user.
     * @param callback observer of the server request.
     */
    @Override
    public void getAll(ListAllCallback<FoodDish> callback) {
        mInteractor.getAllFoodDishes(callback);
    }

    /**
     * Fetch a single elder matching the id provided.
     * @param id specific primary key id of the desired elder.
     * @param callback observer of the server request.
     */
    @Override
    public void get(int id, SingleItemInformationCallback<FoodDish> callback) {
        //Do nothing!
    }

    @Override
    public void refresh() {
        // Not required because the {@link EldersRepository} handles the logic of refreshing the
        // elders from all the available data sources.
    }
}


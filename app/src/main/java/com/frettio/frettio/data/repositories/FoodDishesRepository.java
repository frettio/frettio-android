package com.frettio.frettio.data.repositories;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.presentation.mvp.Repository;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.frettio.frettio.app.common.AppConstants.TAG;
import static com.frettio.frettio.app.common.utils.ActivityHelper.checkNotNull;

/**
 * Created by luisburgos on 3/3/17.
 */

public class FoodDishesRepository implements Repository<FoodDish> {

    private static FoodDishesRepository INSTANCE = null;

    private final Repository<FoodDish> mRemoteDataSource;

    private final Repository<FoodDish> mLocalDataSource;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    Map<Long, FoodDish> mCache;

    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */
    boolean mCacheIsDirty = false;

    private FoodDishesRepository(
            @NonNull Repository<FoodDish> remoteDataSource,
            @NonNull Repository<FoodDish> localDataSource
    ) {
        this.mRemoteDataSource = checkNotNull(remoteDataSource);
        this.mLocalDataSource = checkNotNull(localDataSource);
    }

    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param remoteDataSource the backend data source
     * @param localDataSource  the device storage data source
     * @return the {@link Repository} instance
     */
    public static FoodDishesRepository getInstance(
            Repository<FoodDish> remoteDataSource,
            Repository<FoodDish> localDataSource
    ){
        if (INSTANCE == null) {
            INSTANCE = new FoodDishesRepository(remoteDataSource, localDataSource);
        }
        return INSTANCE;
    }

    /**
     * Used to force {@link #getInstance(Repository, Repository)} to create a new instance
     * next time it's called.
     */
    public static void destroyInstance() {
        INSTANCE = null;
    }

    /**
     * Gets elders from cache, local data source (Realm) or remote data source, whichever is
     * available first.
     * <p>
     *
     * @param callback observer of the server request that expects a list of results.
     */
    @Override
    public void getAll(final ListAllCallback<FoodDish> callback) {
        checkNotNull(callback);

        // Respond immediately with cache if available and not dirty
        if (mCache != null && !mCacheIsDirty) {
            callback.onItemsLoaded(new ArrayList<>(mCache.values()));
            return;
        }

        if (mCacheIsDirty) {
            // If the cache is dirty we need to fetch new data from the network.
            getFromRemoteDataSource(callback);
        } else {
            // Query the local storage if available. If not, query the network.
            mLocalDataSource.getAll(new ListAllCallback<FoodDish>() {
                @Override
                public void onItemsLoaded(List<FoodDish> list) {
                    Log.d(TAG, "Adding to cache total if " + list.size());
                    refreshCache(list);
                    callback.onItemsLoaded(new ArrayList<>(mCache.values()));
                }

                @Override
                public void onNetworkError(String networkErrorMessage) {
                    //Do nothing
                }

                @Override
                public void onServerError(String serverErrorMessage) {
                    getFromRemoteDataSource(callback);
                }

            });
        }
    }

    /**
     * Gets elders from local data source (Realm) unless the table is new or empty. In that
     * case it uses the network data source.
     * <p>
     *
     * @param callback observer of the server request that expects a single item.
     * @param id specific primary key id of the desired elder.
     */
    @Override
    public void get(final int id, final SingleItemInformationCallback<FoodDish> callback) {
        checkNotNull(id);
        checkNotNull(callback);

        FoodDish cachedItem = getWithId(id);

        // Respond immediately with cache if available
        if (cachedItem != null) {
            callback.onItemInformationLoaded(cachedItem);
            return;
        }

        // Load from server/persisted if needed.

        // Is the task in the local data source? If not, query the network.
        mLocalDataSource.get(id, new SingleItemInformationCallback<FoodDish>() {
            @Override
            public void onItemInformationLoaded(FoodDish restaurant) {
                callback.onItemInformationLoaded(restaurant);
            }

            @Override
            public void onNetworkError(String networkErrorMessage) {
                //Do nothing
            }

            @Override
            public void onServerError(String serverErrorMessage) {
                mRemoteDataSource.get(id, new SingleItemInformationCallback<FoodDish>() {
                    @Override
                    public void onItemInformationLoaded(FoodDish restaurant) {
                        callback.onItemInformationLoaded(restaurant);
                    }

                    @Override
                    public void onNetworkError(String networkErrorMessage) {
                        callback.onServerError(networkErrorMessage);
                    }

                    @Override
                    public void onServerError(String serverErrorMessage) {
                        callback.onServerError(serverErrorMessage);
                    }
                });
            }

        });
    }

    /**
     * Deletes all entries on data sources, local data source (Realm), remote server and
     * cached elders.
     *
     */
    @Override
    public void deleteAll() {
        mLocalDataSource.deleteAll();

        if (mCache == null) {
            mCache = new LinkedHashMap<>();
        }
        mCache.clear();
    }

    /**
     * Sets the cached state to dirty in order to directly call server on any request.
     */
    @Override
    public void refresh() {
        mCacheIsDirty = true;
    }

    @Override
    public void add(FoodDish itemToAdd) {
        checkNotNull(itemToAdd);
        mLocalDataSource.add(itemToAdd);

        if (mCache == null){
            mCache = new LinkedHashMap<>();
        }
        mCache.put(itemToAdd.getId(), itemToAdd);
    }

    @Override
    public void add(Iterable<FoodDish> items) {
        mLocalDataSource.add(items);
    }

    @Override
    public void update(FoodDish item) {
        //Not supported
    }

    @Override
    public void remove(FoodDish item) {
        //Not supported
    }

    /**
     * Makes remote call using remote data source implementation.
     *
     * @param callback delegate observer of request.
     */
    private void getFromRemoteDataSource(final ListAllCallback<FoodDish> callback) {
        mRemoteDataSource.getAll(new ListAllCallback<FoodDish>() {
            @Override
            public void onItemsLoaded(List<FoodDish> list) {
                refreshCache(list);
                refreshLocalDataSource(list);
                callback.onItemsLoaded(new ArrayList<>(mCache.values()));
            }

            @Override
            public void onNetworkError(String networkErrorMessage) {
                callback.onNetworkError(networkErrorMessage);
            }

            @Override
            public void onServerError(String serverErrorMessage) {
                callback.onServerError(serverErrorMessage);
            }

        });
    }

    /**
     * Updates cache content.
     * @param items updated list of objects.
     */
    private void refreshCache(List<FoodDish> items) {
        if (mCache == null) {
            mCache = new LinkedHashMap<>();
        }
        mCache.clear();
        for (FoodDish currentItem : items) {
            mCache.put(currentItem.getId(), currentItem);
        }
        mCacheIsDirty = false;
    }

    /**
     * Updates local data source (Realm) content.
     * @param restaurants updated list of objects.
     */
    private void refreshLocalDataSource(List<FoodDish> restaurants) {
        mLocalDataSource.deleteAll();
        mLocalDataSource.add(restaurants);
    }

    /**
     * Adds cache content.
     * @param items list of new objects.
     */
    private void addToLocalDataSource(List<FoodDish> items) {
        for (FoodDish item : items) {
            mCache.put(item.getId(), item);
        }
    }

    /**
     * Adds local data source (Realm) content.
     * @param restaurants list of new objects.
     */
    private void addToCache(List<FoodDish> restaurants) {
        mLocalDataSource.add(restaurants);
    }

    /**
     * Helper method to retrieve object from cache.
     *
     * @param id specific primary key.
     * @return a {@link FoodDish} object matching provided id.
     */
    @Nullable
    private FoodDish getWithId(long id) {
        checkNotNull(id);
        if (mCache == null || mCache.isEmpty()) {
            return null;
        } else {
            return mCache.get(id);
        }
    }
}


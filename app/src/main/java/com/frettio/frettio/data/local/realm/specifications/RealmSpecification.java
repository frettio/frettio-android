package com.frettio.frettio.data.local.realm.specifications;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmResults;

/**
 * Main interface to fetch results on a Realm transaction.
 * @param <RealmEntity> Realm objects class reference to be fetched
 */
public abstract class RealmSpecification<RealmEntity extends RealmModel> {

    private Class<RealmEntity> mClassToQuery;

    public RealmSpecification(Class<RealmEntity> classToQuery) {
        mClassToQuery = classToQuery;
    }

    public Class<RealmEntity> getClassToQuery() {
        return mClassToQuery;
    }

    public abstract RealmResults<RealmEntity> toRealmResults(Realm realm);
}
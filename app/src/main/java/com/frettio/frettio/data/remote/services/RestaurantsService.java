package com.frettio.frettio.data.remote.services;

import com.frettio.frettio.data.APIConstants;
import com.frettio.frettio.domain.model.Restaurant;
import com.frettio.frettio.presentation.mvp.Repository;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by luisburgos on 2/25/17.
 */

public interface RestaurantsService {

    @GET(APIConstants.LIST_ALL_RESTAURANTS_ENDPOINT)
    Call<List<Restaurant>> listAllRestaurants(
            @Query(APIConstants.Queries.STATE) String state
    );

    @GET(APIConstants.GET_RESTAURANT_ENDPOINT)
    Call<Restaurant> findRestaurant(
            @Path(APIConstants.Keys.EMAIL) String restaurantIdentifier
    );

}

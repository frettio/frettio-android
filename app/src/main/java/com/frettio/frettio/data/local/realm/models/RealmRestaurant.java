package com.frettio.frettio.data.local.realm.models;

import com.frettio.frettio.domain.model.RestaurantState;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by luisburgos on 1/27/17.
 */

public class RealmRestaurant extends RealmObject {

    @PrimaryKey
    private String email;
    private String name;
    private String branchOffice;
    private String description;
    private String owner;
    private String logo;
    private String state;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBranchOffice() {
        return branchOffice;
    }

    public void setBranchOffice(String branchOffice) {
        this.branchOffice = branchOffice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}

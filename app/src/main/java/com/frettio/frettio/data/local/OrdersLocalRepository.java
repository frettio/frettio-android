package com.frettio.frettio.data.local;

import com.frettio.frettio.data.local.realm.RealmRepository;
import com.frettio.frettio.data.local.realm.mappers.Mapper;
import com.frettio.frettio.data.local.realm.models.RealmOrder;
import com.frettio.frettio.data.local.realm.specifications.DeleteAllRealmSpecification;
import com.frettio.frettio.data.local.realm.specifications.FindAllRealmSpecification;
import com.frettio.frettio.data.local.realm.specifications.FindByIdRealmSpecification;
import com.frettio.frettio.data.local.realm.specifications.RealmSpecification;
import com.frettio.frettio.domain.model.Order;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by luisburgos on 2/25/17.
 */

public class OrdersLocalRepository extends RealmRepository<RealmOrder, Order> {

    private static OrdersLocalRepository INSTANCE;

    private OrdersLocalRepository(RealmConfiguration realmConfiguration, Mapper mapper) {
        super(realmConfiguration, mapper);
    }

    public static OrdersLocalRepository getInstance(RealmConfiguration realmConfiguration, Mapper mapper) {
        if (INSTANCE == null) {
            INSTANCE = new OrdersLocalRepository(realmConfiguration, mapper);
        }
        return INSTANCE;
    }

    /**
     * Save a object into local database. Executes individual transaction on Realm database.
     * @param Order object to persist locally.
     */
    @Override
    public void add(final Order Order) {
        final Realm realm = Realm.getInstance(realmConfiguration);

        //TODO: Add interface callbacks to match realm callback
        realm.executeTransactionAsync(
                new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        final RealmOrder realmItem = realm.createObject(RealmOrder.class);
                        //SETTERS
                    }
                },
                new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        // Transaction was a success.
                    }
                },
                new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        // Transaction failed and was automatically canceled.
                    }
                }
        );

        realm.close();
    }

    /**
     * Saves a list of objects into local database. This method iterates over the list and
     * call {@link OrdersLocalRepository#add(Order)} to execute individual transactions.
     * @param items list of objects to save.
     */
    @Override
    public void add(Iterable<Order> items) {
        final Realm realm = Realm.getInstance(realmConfiguration);

        List<RealmOrder> realmItems = new LinkedList<>();
        for(Order item : items){
            final RealmOrder realmItem = new RealmOrder();
            //SETTERS
            realmItems.add(realmItem);
        }

        realm.beginTransaction();
        realm.copyToRealmOrUpdate(realmItems);
        realm.commitTransaction();
    }

    /**
     * Retrieves all objects from local database.
     * @param callback responder of transaction.
     */
    @Override
    public void getAll(final ListAllCallback<Order> callback) {
        final RealmSpecification<RealmOrder> realmSpecification = new FindAllRealmSpecification<>(RealmOrder.class);

        final Realm realm = Realm.getInstance(realmConfiguration);
        final RealmResults<RealmOrder> realmResults = realmSpecification.toRealmResults(realm);

        if(realmResults.isEmpty()){
            callback.onServerError("");
        } else {
            final List<Order> items = new ArrayList<>();

            for (RealmOrder realmPet : realmResults) {
                items.add(toDomainEntityMapper.map(realmPet));
            }

            realm.close();
            callback.onItemsLoaded(items);
        }
    }

    /**
     * Retrieve a single object matching id provided. If there is no object matching the method
     * @param callback responder of transaction.
     * @param id matching primary key id
     */
    @Override
    public void get(int id, SingleItemInformationCallback<Order> callback) {
        final RealmSpecification realmSpecification = new FindByIdRealmSpecification<RealmOrder>(
                RealmOrder.class, "email", id
        );

        final Realm realm = Realm.getInstance(realmConfiguration);
        final RealmResults<RealmOrder> realmResult = realmSpecification.toRealmResults(realm);

        Order Order = toDomainEntityMapper.map(realmResult.first());
        if (Order != null) {
            callback.onItemInformationLoaded(Order);
        } else {
            callback.onServerError("");
        }
    }

    @Override
    public void deleteAll() {
        final RealmSpecification realmSpecification = new DeleteAllRealmSpecification(RealmOrder.class);
        final Realm realm = Realm.getInstance(realmConfiguration);

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realmSpecification.toRealmResults(realm);
            }
        });
    }

    @Override
    public void update(Order item) {
        //Not supported.
    }

    @Override
    public void remove(Order item) {
        //Not supported.
    }

    @Override
    public void refresh() {
        // Not required because the {@link } handles the logic of refreshing the
        // elders from all the available data sources.
    }
}

package com.frettio.frettio.data.remote.services;

import com.frettio.frettio.data.APIConstants;
import com.frettio.frettio.domain.model.FoodDish;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by luisburgos on 2/25/17.
 */

public interface FoodDishesService {

    @GET(APIConstants.LIST_ALL_DISHES_ENDPOINT)
    Call<List<FoodDish>> listAllDishes(
            @Path(APIConstants.Keys.EMAIL) String restaurantIdentifier
    );

}

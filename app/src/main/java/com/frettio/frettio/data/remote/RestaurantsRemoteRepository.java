package com.frettio.frettio.data.remote;

import com.frettio.frettio.data.repositories.FetchOnlyRepository;
import com.frettio.frettio.domain.interactor.AbstractRestaurantInteractor;
import com.frettio.frettio.domain.model.Restaurant;

import org.jetbrains.annotations.NotNull;

import static com.frettio.frettio.app.common.utils.ActivityHelper.checkNotNull;

/**
 * Created by luisburgos on 1/27/17.
 */

public class RestaurantsRemoteRepository extends FetchOnlyRepository<Restaurant> {

    private AbstractRestaurantInteractor mInteractor;

    public RestaurantsRemoteRepository(@NotNull AbstractRestaurantInteractor interactor){
        mInteractor = checkNotNull(interactor);
    }

    /**
     * Fetch all elders that belongs to the current application user.
     * @param callback observer of the server request.
     */
    @Override
    public void getAll(ListAllCallback<Restaurant> callback) {
        mInteractor.getAllRestaurants(callback);
    }

    /**
     * Fetch a single elder matching the id provided.
     * @param id specific primary key id of the desired elder.
     * @param callback observer of the server request.
     */
    @Override
    public void get(int id, SingleItemInformationCallback<Restaurant> callback) {
        mInteractor.getRestaurant(id, callback);
    }

    @Override
    public void refresh() {
        // Not required because the {@link RestaurantsRepository} handles the logic of refreshing the
        // elders from all the available data sources.
    }
}


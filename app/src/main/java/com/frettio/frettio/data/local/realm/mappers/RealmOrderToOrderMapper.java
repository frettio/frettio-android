package com.frettio.frettio.data.local.realm.mappers;

import com.frettio.frettio.data.local.realm.models.RealmFoodDish;
import com.frettio.frettio.data.local.realm.models.RealmOrder;
import com.frettio.frettio.domain.model.FoodDish;
import com.frettio.frettio.domain.model.NewOrder;
import com.frettio.frettio.domain.model.Order;

import java.util.ArrayList;

/**
 * Created by luisburgos on 3/5/17.
 */

public class RealmOrderToOrderMapper implements Mapper<RealmOrder, NewOrder>{

    @Override
    public NewOrder map(RealmOrder realmItem) {
        NewOrder order = new NewOrder();

        order.setRestaurantName(realmItem.getRestaurantName());
        order.setConsumerEmail(realmItem.getConsumerEmail());
        order.setRestaurantEmail(realmItem.getRestaurantEmail());
        order.setTotal(realmItem.getTotal());

        RealmFoodDishToFoodDishMapper foodDishMapper = new RealmFoodDishToFoodDishMapper();
        ArrayList<FoodDish> foodDishes = new ArrayList<>();
        for(RealmFoodDish realmFoodDish : realmItem.getDishes()){
            foodDishes.add(foodDishMapper.map(realmFoodDish));
        }

        order.setDishes(foodDishes);

        return order;
    }

}


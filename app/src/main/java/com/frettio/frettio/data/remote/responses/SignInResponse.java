package com.frettio.frettio.data.remote.responses;

import com.frettio.frettio.domain.model.User;
import com.google.gson.annotations.SerializedName;

/**
 * Created by luisburgos on 2/12/17.
 */

public class SignInResponse {

    @SerializedName("token")
    private String token;

    @SerializedName("user")
    private User user;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

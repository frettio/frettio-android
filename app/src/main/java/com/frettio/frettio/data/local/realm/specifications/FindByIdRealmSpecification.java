package com.frettio.frettio.data.local.realm.specifications;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Roberto on 11/17/16.
 */

public class FindByIdRealmSpecification<QueryObject extends RealmObject> extends RealmSpecification<QueryObject> {

    /**
     * Specific primary key.
     */
    private long mObjectID;
    private String mFieldName;

    public FindByIdRealmSpecification(Class<QueryObject> classToQuery, String fieldName, long objectID) {
        super(classToQuery);
        mFieldName = fieldName;
        mObjectID = objectID;
    }


    /**
     *
     * @param realm instance of realm database.
     * @return all matching clients to the id provided. It should be just one object.
     * This method uses {@link RealmQuery#findAll()} method because provides a {@link RealmResults}
     * list as return type.
     */
    @Override
    public RealmResults<QueryObject> toRealmResults(Realm realm) {
        return realm.where(getClassToQuery()).equalTo(mFieldName, mObjectID).findAll();
    }
}
